<?php
class M_karyawan extends CI_Model{
    
    
        function generateKode()
           {
                // FORMAT SMA/TAHUN SEKARANG/0001
                // EX : SMA/2020/0001
        
                $this->db->select('RIGHT(karyawan_nik,4) as kode', false);
                $this->db->order_by("karyawan_nik", "DESC");
                $this->db->limit(1);
                $query = $this->db->get('tb_karyawan');
        
                // SQL QUERY
                // SELECT RIGHT(kode, 4) AS kode FROM tb_siswa
                // ORDER BY kode
                // LIMIT 1
        
                // CEK JIKA DATA ADA
                if($query->num_rows() <> 0)
                {
                    $data       = $query->row(); // ambil satu baris data
                    $kode  = intval($data->karyawan_nik) + 1; // tambah 1
                }else{
                    $kode = 1; // isi dengan 1
                }
        
                $lastKode = str_pad($kodea, 4, "0", STR_PAD_LEFT);
                //$tahun    = date("Y");
                $TKA      = "TKA";
        
                $newKode  = $TKA."-".$lastKode;
        
                return $newKode;  // return kode baru
        
           }

    function CreateCode(){
    $this->db->select('RIGHT(tbl_karyawan.karyawan_nik,5) as karyawan_nik', FALSE);
    $this->db->order_by('karyawan_nik','DESC');    
    $this->db->limit(1);    
    $query = $this->db->get('tbl_karyawan');
        if($query->num_rows() <> 0){      
             $data = $query->row();
             $kode = intval($data->karyawan_nik) + 1; 
        }
        else{      
             $kode = 1;  
        }
    $batas = str_pad($kode, 5, "0", STR_PAD_LEFT);    
    $kodetampil = "TKA-".$batas;
    return $kodetampil;  
    }
    
  
	function get_all_karyawan(){
		$hsl=$this->db->query("select * from tbl_karyawan  ORDER BY `karyawan_nik` ASC");
		return $hsl;
	}
	function simpan($karyawan_id,$karyawan_nik,$karyawan_nama,$jabatan_nama){
        $hasil=$this->db->query("INSERT INTO tbl_karyawan (karyawan_id,karyawan_nik,karyawan_nama,jabatan_nama) VALUES ('$karyawan_id','$karyawan_nik','$karyawan_nama','$jabatan_nama')");
        return $hasil;
    }
	function update($karyawan_id,$karyawan_nik,$karyawan_nama,$jabatan_nama){
        $hasil=$this->db->query("UPDATE tbl_karyawan SET karyawan_nik='$karyawan_nik',karyawan_nama='$karyawan_nama',jabatan_nama='$jabatan_nama' WHERE karyawan_id='$karyawan_id'");
        return $hsl;
    }
	
	function hapus($karyawan_id){
		$hsl=$this->db->query("delete from tbl_karyawan where karyawan_id='$karyawan_id'");
		return $hsl;
	}
	
	function get_karyawan_byid($karyawan_id){
		$hsl=$this->db->query("select * from tbl_karyawan where karyawan_id='$karyawan_id'");
		return $hsl;
	}
	public function total_karyawan()
	{   
		$query = $this->db->get('tbl_karyawan');
		if($query->num_rows()>0)
		{
		  return $query->num_rows();
		}
		else
		{
		  return 0;
		}
	}
	
	function Ceknik($karyawan_nik) {
    $this->db->where('karyawan_nik',$karyawan_nik);
    $query = $this->db->get('tbl_karyawan');
    return $query->num_rows();
    }

}