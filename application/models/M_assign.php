<?php
class M_assign extends CI_Model{

	function get_all_assign(){
		$hsl=$this->db->query("select * from tbl_assign2  ORDER BY `assign_id` DESC");
		return $hsl;
	}
	function get_all_data(){
		$hsl=$this->db->query("select * from tbl_assign2  ORDER BY `assign_id` DESC");
		return $hsl;
	}
	function cariAlat()
	{
		$cari = $this->input->GET('cari', TRUE);
		$data = $this->db->query("SELECT * from tbl_assign2 where karyawan_nama like '%$cari%' ");
		return $data->result();
	}
		function cariSn()
	{
		$cari = $this->input->GET('cari', TRUE);
		$data = $this->db->query("SELECT * from tbl_assign2 where serial like '%$cari%' ");
		return $data->result();
	}
	
	function show_assign(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 ORDER BY `assign_id` DESC");
        return $hasil;
    }
	function alat_baru(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'BARU' ORDER BY `assign_id` DESC");
        return $hasil;
    }
	function alat_terpakai(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'TERPAKAI' ORDER BY `assign_id` DESC");
        return $hasil;
    }
    function alat_hilang(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'HILANG' ORDER BY `assign_id` DESC");
        return $hasil;
    }
    function alat_rusak(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'RUSAK' ORDER BY `assign_id` DESC");
        return $hasil;
    }
     function alat_perbaikan(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'PERBAIKAN' ORDER BY `assign_id` DESC");
        return $hasil;
    }
    function alat_dump(){
        $hasil=$this->db->query("SELECT * FROM tbl_assign2 WHERE status_nama = 'DUMP' ORDER BY `assign_id` DESC");
        return $hasil;
    }

	function simpan($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama){
        $hasil=$this->db->query("INSERT INTO tbl_assign (assign_id,alat_nama,kategori_nama,merek,model,serial,karyawan_nama,lokasi_nama,divisi_nama,deskripsi,jumlah,ukuran,status_nama) VALUES ('$assign_id','$alat_nama','$kategori_nama','$merek','$model','$serial','$karyawan_nama','$lokasi_nama','$divisi_nama','$deskripsi','$jumlah','$ukuran','$status_nama')");
        return $hasil;
    }
	
	function update($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama){
        $hasil=$this->db->query("UPDATE tbl_assign SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',status_nama='$status_nama' WHERE assign_id='$assign_id'");
        return $hsl;
    }
	
	function update_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$qr_code){
        $hasil=$this->db->query("UPDATE tbl_assign SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',status_nama='$status_nama',qr_code='$qr_code' WHERE assign_id='$assign_id'");
        return $hsl;
    }
	function simpan2($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$qr_code){
        $hasil=$this->db->query("INSERT INTO tbl_assign2 (assign_id,alat_nama,kategori_nama,merek,model,serial,karyawan_nama,lokasi_nama,divisi_nama,deskripsi,jumlah,ukuran,status_nama,qr_code) VALUES ('$assign_id','$alat_nama','$kategori_nama','$merek','$model','$serial','$karyawan_nama','$lokasi_nama','$divisi_nama','$deskripsi','$jumlah','$ukuran','$status_nama','$qr_code')");
	   return $hasil;
    }
	function pindah_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$qr_code){
        $hasil=$this->db->query("UPDATE tbl_assign2 SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',ukuran='$ukuran',status_nama='$status_nama',qr_code='$qr_code' WHERE assign_id='$assign_id'");
        return $hsl;
    }
    function dump_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$suplier_nama,$qr_code){
        $hasil=$this->db->query("UPDATE tbl_assign2 SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',ukuran='$ukuran',status_nama='$status_nama',suplier_nama='$suplier_nama',qr_code='$qr_code' WHERE assign_id='$assign_id'");
        return $hsl;
    }
    function perbaikan_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$suplier_nama){
        $hasil=$this->db->query("UPDATE tbl_assign2 SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',ukuran='$ukuran',status_nama='$status_nama',suplier_nama='$suplier_nama' WHERE assign_id='$assign_id'");
        return $hsl;
    }
    
	function assign_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$qr_code){
        $hasil=$this->db->query("UPDATE tbl_assign2 SET alat_nama='$alat_nama',kategori_nama='$kategori_nama',merek='$merek',model='$model',serial='$serial',karyawan_nama='$karyawan_nama',lokasi_nama='$lokasi_nama',divisi_nama='$divisi_nama',deskripsi='$deskripsi',jumlah='$jumlah',ukuran='$ukuran',status_nama='$status_nama',qr_code='$qr_code' WHERE assign_id='$assign_id'");
        return $hsl;
    }
	
	function hapus_assign($assign_id)
	{
		$hsl=$this->db->query("delete from tbl_assign2 where assign_id='$assign_id'");
		return $hsl;
	}
	
	function get_assign_byid($assign_id){
		$hsl=$this->db->query("select * from tbl_assign where assign_id='$assign_id'");
		return $hsl;
	}
	
	function total_assign()
	{   
		$query = $this->db->get('tbl_assign2');
		if($query->num_rows()>0)
		{
		  return $query->num_rows();
		}
		else
		{
		  return 0;
		}
	}
	function total_jumlah_alat()
	{
		$this->db->select_sum('jumlah');
		$query = $this->db->get('tbl_assign2');
		if($query->num_rows()>0)
			{
				return $query->row()->jumlah;
			}
			else
			{
				return 0;
			}
	}
	function get_detail($id)
    {
        $this->db->where('MD5(assign_id)', $id);
        $query = $this->db->get('tbl_assign2');
        return $query->row_array();
    }
    function alatlokasi(){
	 $this->db->select('assign_id, alat_nama, kategori_nama, merek, model, serial, karyawan_nama, lokasi_nama, divisi_nama, deskripsi, jumlah, ukuran, status_nama, COUNT(lokasi_nama) as total');
	 $this->db->group_by('lokasi_nama'); 
	 $this->db->order_by('total', 'desc'); 
	 $hasil = $this->db->get('tbl_assign2');
	return $hasil;
	}
	function alatstatus(){
	 $this->db->select('assign_id, alat_nama, kategori_nama, merek, model, serial, karyawan_nama, lokasi_nama, divisi_nama, deskripsi, jumlah, ukuran, status_nama, COUNT(status_nama) as total');
	 $this->db->group_by('status_nama'); 
	 $this->db->order_by('total', 'desc'); 
	 $hasil = $this->db->get('tbl_assign2');
	return $hasil;
	}
	function alatkategori(){
	 $this->db->select('assign_id, alat_nama, kategori_nama, merek, model, serial, karyawan_nama, lokasi_nama, divisi_nama, deskripsi, jumlah, ukuran, status_nama, COUNT(status_nama) as total');
	 $this->db->group_by('kategori_nama'); 
	 $this->db->order_by('total', 'desc'); 
	 $hasil = $this->db->get('tbl_assign2');
	return $hasil;
	}
	function alatmerek(){
	 $this->db->select('assign_id, alat_nama, kategori_nama, merek, model, serial, karyawan_nama, lokasi_nama, divisi_nama, deskripsi, jumlah, ukuran, status_nama, COUNT(merek) as total');
	 $this->db->group_by('lokasi_nama');
	 $this->db->group_by('status_nama');
	 $this->db->order_by('total', 'desc'); 
	 $hasil = $this->db->get('tbl_assign2');
	return $hasil;
	}
	
	function alatnamajenis(){
	 $this->db->select('assign_id, alat_nama, kategori_nama, merek, model, serial, karyawan_nama, lokasi_nama, divisi_nama, deskripsi, jumlah, ukuran, status_nama, COUNT(alat_nama) as total');
	 //$this->db->group_by('lokasi_nama');
	 $this->db->group_by('alat_nama');
	 $this->db->order_by('total', 'desc'); 
	 $hasil = $this->db->get('tbl_assign2');
	return $hasil;
	}
	
	
	
	
    function Cekserial($serial) {
    $this->db->where('serial',$serial);
    $query = $this->db->get('tbl_assign2');
    return $query->num_rows();
    }
    
    function Cekseri($serial) {
    $this->db->where('serial',$serial);
    $query = $this->db->get('tbl_assign2');
    return $query->num_rows();
    }
    
    function Cekseri1($serial) {
    $this->db->where('serial',$serial);
    $query = $this->db->get('tbl_assign2');
    return $query->num_rows();
    }
    
    function get_alat_lokasi()
    {
        $query = "SELECT  COUNT(*) AS total, lokasi_nama FROM tbl_assign2
                    GROUP BY lokasi_nama ORDER BY lokasi_nama ASC";
        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    function get_alat_nama()
    {
        $query = "SELECT  COUNT(*) AS total, alat_nama FROM tbl_assign2
                    GROUP BY alat_nama ORDER BY alat_nama ASC";
        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    function get_alat_merek()
    {
        $query = "SELECT  COUNT(*) AS total, merek FROM tbl_assign2
                    GROUP BY merek ORDER BY merek ASC";
        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    
    function get_alat_status()
    {
        $query = "SELECT COUNT(*) AS total, status_nama FROM tbl_assign2
                    GROUP BY status_nama ORDER BY status_nama DESC";

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    function get_alat_kategori()
    {
        $query = "SELECT COUNT(*) AS total, kategori_nama FROM tbl_assign2
                    GROUP BY kategori_nama ORDER BY kategori_nama DESC";

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    
    
}