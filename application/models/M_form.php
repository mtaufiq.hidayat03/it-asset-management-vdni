<?php
class M_form extends CI_Model{

	function get_all_form(){
		$hsl=$this->db->query("select * from tbl_form  ORDER BY `form_id` DESC");
		return $hsl;
	} 
	public function get_all()
    {
        return $this->db->get("tbl_form");
    }
	function tampil_form(){
        $hasil=$this->db->query("select * from tbl_form  ORDER BY `form_id` DESC");
        return $hasil;
    }
	function simpan_form($foto,$form_id,$nomor_form,$po_form,$divisi_nama,$suplier_nama,$tgl_masuk,$serial){
		$hsl=$this->db->query("insert into tbl_form(foto,form_id,nomor_form,po_form,divisi_nama,suplier_nama,tgl_masuk,serial) values ('$foto','$form_id','$nomor_form','$po_form','$divisi_nama','$suplier_nama','$tgl_masuk','$serial')");
		return $hsl;
	}
	function edit($id_dokumen,$doc_number,$issue_revision,$pr_number,$po_number,$staff_name,$staff_position,$departement_name,$approved_by,$quantity,$item_name,$spesification,$remarks){
        $hasil=$this->db->query("UPDATE tb_form SET doc_number='$doc_number',issue_revision='$issue_revision',pr_number='$pr_number,po_number='$po_number,staff_name='$staff_name,staff_position='$staff_position,departement_name='$departement_name,approved_by='$approved_by,quantity='$quantity,item_name='$item_name,spesification='$spesification,remarks='$remarks,' WHERE id_dokumen='$id_dokumen'");
        return $hsl;
    }
	function hapus_form($form_id){
		$hsl=$this->db->query("delete from tbl_form where form_id='$form_id'");
		return $hsl;
	}
	function get_detail($id)
    {
        $this->db->where('id_dokumen', $id);
        $query = $this->db->get('tb_form');
        return $query->row_array();
    }
    
     function get_form_suplier()
    {
        $query = "SELECT COUNT(*) AS total, suplier_nama FROM tbl_form
                    GROUP BY suplier_nama ORDER BY suplier_nama DESC";

        $result = $this->db->query($query)->result_array();
        return $result;
    }
	

}