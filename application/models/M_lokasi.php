<?php
class M_lokasi extends CI_Model{

	function show_lokasi(){
        $hasil=$this->db->query("SELECT * FROM tbl_lokasi  ORDER BY `lokasi_nama` DESC");
        return $hasil;
    }
	function get_all_lokasi(){
		$hsl=$this->db->query("select * from tbl_lokasi");
		return $hsl;
	}
	
	function simpan_lokasi($lokasi){
		$hsl=$this->db->query("insert into tbl_lokasi(lokasi_nama) values('$lokasi')");
		return $hsl;
	}
	function update_lokasi($id_lokasi,$lokasi){
		$hsl=$this->db->query("update tbl_lokasi set lokasi_nama='$lokasi' where lokasi_id='$id_lokasi'");
		return $hsl;
	}
	function hapus_lokasi($id_lokasi){
		$hsl=$this->db->query("delete from tbl_lokasi where lokasi_id='$id_lokasi'");
		return $hsl;
	}
	
	function get_lokasi_byid($lokasi_id){
		$hsl=$this->db->query("select * from tbl_lokasi where lokasi_id='$lokasi_id'");
		return $hsl;
	}
	public function total_lokasi()
	{   
		$query = $this->db->get('tbl_lokasi');
		if($query->num_rows()>0)
		{
		  return $query->num_rows();
		}
		else
		{
		  return 0;
		}
	}

}