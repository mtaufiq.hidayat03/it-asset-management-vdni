<?php
class M_file extends CI_Model{

	function getAllFiles()
	{
      $query = $this->db->get('tbl_upload1');
      return $query->result(); 
    }
 
   function insertfile($file)
    {
        return $this->db->insert('tbl_upload1', $file);
    }

    function download($id)
    {
          $query = $this->db->get_where('tbl_upload1',array('id'=>$id));
          return $query->row_array();
    }
    
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_upload1');
        return true;
    }
	
}