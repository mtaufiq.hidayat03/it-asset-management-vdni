<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('m_login');
		//$this->load->model('m_pengunjung');
		//$this->load->model('m_Properti');
		//$this->load->model('m_pengguna');
		//$this->load->model('m_kategori');
		//$this->load->model('m_kontak');
		//$this->load->model('m_komentar');
		//$this->load->model('m_galeri');
		//$this->load->model('m_album');
		//$this->load->model('m_agen');
        if($this->m_login->is_role() != "admin"){
            redirect("login/");
        }
    }

    function index(){
			//$x['visitor'] = $this->m_pengunjung->statistik_pengujung();
			//$x['jml_pengguna'] = $this->m_pengguna->jml_pengguna();
			//$x['total_pengunjung'] = $this->m_pengunjung->total_pengunjung();
			//$x['total_properti'] = $this->m_Properti->jml_properti();
			//$x['total_galeri'] = $this->m_galeri->jml_galeri();
			//$x['total_album'] = $this->m_album->jml_album();
			//$x['jml_agen']=$this->m_agen->total_agen();
			//$x['user']=$this->m_agen->show_agen();
			//$x['properti'] = $this->m_Properti->tampil_properti();
			//$x['data']=$this->m_kontak->get_all_inbox();
			//$x['terbaru']=$this->m_Properti->get_properti_terbaru();
			$this->load->view('admin/v_dashboard');
	
	}

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}