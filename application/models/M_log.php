<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_log extends CI_Model {
 
    public function save_log($param)
    {
        $sql        = $this->db->insert_string('tbl_log',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }
    
    
	function tampil_log(){
        $hasil=$this->db->query("SELECT * FROM tbl_log ORDER BY `log_time` DESC");
        return $hasil;
    }
	
	
	
}