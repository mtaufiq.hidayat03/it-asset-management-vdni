<!DOCTYPE html>
<html lang="en">

<head>
    <title>VDNI </title>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap\css\bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\themify-icons\themify-icons.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\icofont\css\icofont.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
</head>

<body class="fix-menu">
<form action="<?php echo base_url().'login/auth'?>" method="post">
    <section class="login-block">
        <div class="container">
            <div class="row">
                
                        <form class="md-float-material form-material">
                            <div class="auth-box card">
                                <div class="card-block">
                                    <div class="row m-b-20">
                                        <div class="col-md-12">
                                            <h3 class="text-center">IT Asset System</h3>
                                        </div>
                                    </div>
									
                                    <div class="form-control ">
                                        <input type="text" name="username" class="form-control" required="" placeholder="Username">
                                        <span class="form-bar"></span>
                                    </div>
                                    <div class="form-control ">
                                        <input type="password" name="password" class="form-control" required="" placeholder="Password">
                                        <span class="form-bar"></span>
                                    </div>
                                    
                                    <div class="row m-t-30">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info btn-block waves-effect waves-light text-center m-b-20">Login</button>
                                        </div>
                                        <hr>
                                        <div class="col-md-12">
										<center><img src="<?php echo base_url().'assets\files\assets\images\auth\vdni_ikon.png" alt="'?>"></center>
                                        </div>
                                    </div>
						        </form>
                                   
                                </div>
                            </div>
            </div>
        </div>
    </section>
</form>
   
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery\js\jquery.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-ui\js\jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\popper.js\js\popper.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'asset\files\bower_components\bootstrap\js\bootstrap.min.js'?>"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js'?>"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\modernizr.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\css-scrollbars.js'?>"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next\js\i18next.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-i18next\js\jquery-i18next.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\assets\js\common-pages.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>



<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Uername atau Password salah , silahkan Ulangi Lagi .",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Selamat datang Di IMS VDNI.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Data berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Data Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#DC143C'
                });
        </script>
    <?php else:?>

    <?php endif;?>









</body>

</html>
