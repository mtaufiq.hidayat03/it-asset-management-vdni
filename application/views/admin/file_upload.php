 <!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title>IMS VDNI</title>
 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">-->
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/font-awesome/4.5.0/css/font-awesome.min.css'?>" />
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap.min.css'?>" />
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
   <div class="navbar-header">
   </div>
   <center><h1>Form Upload Berkas</h1></center>
  </div>
 </nav>
 <div class="container">
  <div class="row">
   <div class="col-sm-4">
   <form method="POST" action="<?php echo base_url().'admin/file1/insert'?>" enctype="multipart/form-data">
     <div class="form-group">
      <label>Keterangan Berkas :</label>
      <input type="text" name="description" class="form-control" required>
     </div>
     <div class="form-group">
      <label>Format Berkas (pdf) :</label>
      <input type="file" name="upload" required>
     </div>
     <button type="submit" class="btn btn-primary">Unggah</button>
    </form>
    <?php
    if($this->session->flashdata('success')){
     ?>
     <div class="alert alert-success text-center" style="margin-top:20px;">
      <?php echo $this->session->flashdata('success'); ?>
     </div>
     <?php
    }

    if($this->session->flashdata('error')){
     ?>
     <div class="alert alert-danger text-center" style="margin-top:20px;">
      <?php echo $this->session->flashdata('error'); ?>
     </div>
     <?php
    }
    ?>
   </div>
   <br/>
   <div class="col-sm-8">
    <table id="example" class="display" style="width:100%">
     <thead>
      <tr>
       <th>File/Berkas</th>
       <th>Ket. Berkas</th>
       <th>Aksi</th>
      </tr>
     </thead>
     <tbody>
      <?php
      foreach($files as $file){
       ?>
       <tr>
        <td><?php echo $file->filename; ?></td>
        <td><?php echo $file->description; ?></td>
        <td><a href="<?php echo base_url().'admin/file1/download/'.$file->id; ?>" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt"></a>
            <a href="<?php echo base_url().'admin/file1/hapus/'.$file->id; ?>" class="btn btn-info btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
        </td>
        </tr>
        <?php
       }
       ?>
      </tbody>
     </table>
     <a href="<?php echo base_url('page');?>" class="btn btn-success btn-large"> Kembali <span class="glyphicon glyphicon-hand-left"></a>
    </div>
   </div>
  </div>

  <script type="text/javascript">
   $(document).ready(function() {
    $('table.display').DataTable();
   } );
  </script>
 </body>
 </html> 