<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>VDNI</title>
		    <link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
		    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
		    <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/font-awesome/4.5.0/css/font-awesome.min.css'?>" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap.min.css'?>" />
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
            <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
 
	<div class="container">
	<center><img src="<?php echo base_url().'assets\files\assets\images\auth\vdni_ikon.png" alt="'?>"><h3>Data Hasil Tracking </h3></center>
	<hr>
 
		<?php
 
		if(count($cari)>0)
		{
			foreach ($cari as $data) {
			echo $data->status_nama . ",  PENGGUNA : <b><i>" . $data->karyawan_nama ."</i></b> [" . $data->divisi_nama ."," . $data->lokasi_nama ."], ALAT : <b>" . $data->alat_nama ."</b> [" . $data->merek ."," . $data->serial ."]<br>";
			
			}
		}
 
		else
		{
			echo "Data tidak ditemukan";
		}
 
		?>
	</div><p>
	<center><a href="<?php echo base_url().'hrd/'; ?>" class="btn btn-success btn-sm"> Kembali <span class="glyphicon glyphicon-hand-left"></a></p>
	</body>
</html>