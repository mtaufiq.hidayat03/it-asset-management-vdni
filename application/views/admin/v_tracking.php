<!doctype html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>VDNI-TRACKING</title>
    <link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
	<script src="<?php echo base_url().'assets/js/html5-qrcode.min.js'?>"></script>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<style>
  .result{
    background-color: green;
    color:#fff;
    padding:20px;
  }
  .row{
    display:flex;
  }
</style>
</head>

<body>
<div class="row">
  <div class="col">
      <center><img src="<?php echo base_url().'assets\files\assets\images\auth\vdni_ikon.png" alt="'?>"><h3></h3></center>
    <div style="width:400px;" id="reader"></div>
     <center><h4>Hasil Scanning</h4></center>
     <br/>
                    <center>
                        <div id="result"></div>
                    </center>
  </div>
</div>	
	
</body>
<script type="text/javascript">
function onScanSuccess(qrCodeMessage) {
    document.getElementById('result').innerHTML = '<span class="result">'+qrCodeMessage+'</span>';
}
function onScanError(errorMessage) {
  //handle scan error
}
var html5QrcodeScanner = new Html5QrcodeScanner(
    "reader", { fps: 10, qrbox: 250 });
html5QrcodeScanner.render(onScanSuccess, onScanError);
</script>
</html>
