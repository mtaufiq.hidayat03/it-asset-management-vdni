 <!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title>VDNI</title>
 <link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
 <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">-->
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/font-awesome/4.5.0/css/font-awesome.min.css'?>" />
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap.min.css'?>" />
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
   <div class="navbar-header">
   </div>
   <center><h2>Info Purchasing</h2></center>
  </div>
 </nav>
 <div class="container">
  <div class="row">
      <center><img src="<?php echo base_url().'assets\files\assets\images\auth\vdni_ikon.png" alt="'?>"></center>
      <br/>
   <div class="col-xs-12">
       
    <table id="example" class="display" style="width:100%">
     <thead>
      <tr>
       <th>File/Berkas</th>
       <th>Keterangan Berkas</th>
       <th><center>Download</center></th>
      </tr>
     </thead>
     <tbody>
      <?php
      foreach($files as $file){
       ?>
       <tr>
        <td><?php echo $file->filename; ?></td>
        <td><?php echo $file->description; ?></td>
        <td><center><a href="<?php echo base_url().'admin/file1/download/'.$file->id; ?>" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt"></a></center>
        </td>
        </tr>
        <?php
       }
       ?>
      </tbody>
     </table>
    </div>
   </div>
  </div>

  <script type="text/javascript">
   $(document).ready(function() {
    $('table.display').DataTable();
   } );
  </script>
 </body>
 </html> 