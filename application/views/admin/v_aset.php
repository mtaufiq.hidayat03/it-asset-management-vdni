 <!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title>VDNI-IT ASET</title>
 <link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/font-awesome/4.5.0/css/font-awesome.min.css'?>" />
 <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap.min.css'?>" />
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
   <div class="navbar-header">
   </div>
   <center><h2>ASSET DATA ( IT 资产数据 )</h2></center>
  </div>
 </nav>
 <div class="container">
  <div class="row">
      <center><img src="<?php echo base_url().'assets\files\assets\images\auth\vdni_ikon.png" alt="'?>"></center>
      <br/>
   <div class="col-xs-12">
       
    <table id="example" class="display" style="width:100%">
     <thead>
      <tr>
       <th>No.数字</th>
       <th>Device 设备名称</th>
       <th>Trademark 商标</th>
       <th>Serial Number 序列号</th>
       <th>User 用户</th>
       <th>Department 分配</th>
       <th>Condition 状况</th>
       <th>Qr</th>
       
      </tr>
     </thead>
     <tbody>
      <?php
      	$no=0;
      foreach ($data1->result_array() as $i) :
													   $no++;
													   $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];               
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
													   $qr_code=$i['qr_code'];
													?>
       <tr>
        <td><?php echo $no ?></td>
                                                        <?php if($alat_nama=='Komputer AIO'):?>
														<td>Komputer AIO (计算机 AIO)</span></td>
														<?php elseif($alat_nama=='Komputer CPU'):?>
														<td>Komputer CPU ( 计算机 CPU)</span></td>
														<?php elseif($alat_nama=='Printer'):?>
														<td>Printer (打印机)</span></td>
														<?php elseif($alat_nama=='Notebook'):?>
														<td>Notebook (笔记本)</span></td>
														<?php elseif($alat_nama=='Monitor LCD / LED'):?>
														<td>Monitor LCD / LED (监控荧幕)</span></td>
														<?php elseif($alat_nama=='Proyektor'):?>
														<td>Projector (投影仪)</span></td>
														<?php elseif($alat_nama=='Adaptor'):?>
														<td>Adaptor (适配器)</span></td>
														<?php elseif($alat_nama=='Keyboard'):?>
														<td>Keyboard (键盘)</span></td>
														<?php elseif($alat_nama=='Mouse'):?>
														<td>Mouse (指针鼠标)</span></td>
														<?php elseif($alat_nama=='Speaker / Microfone'):?>
														<td>Speaker/Microfone (麦克风)</span></td>
														<?php elseif($alat_nama=='Switch'):?>
														<td>Switch (交换集线器)</span></td>
														<?php elseif($alat_nama=='Harddisk (HDD)'):?>
														<td>Harddisk (HDD) (硬盘)</span></td>
														<?php elseif($alat_nama=='Flashdisk'):?>
														<td>Flashdisk (闪存盘)</span></td>
														<?php elseif($alat_nama=='Repair Tool Kit'):?>
														<td>Repair Tool Kit (工具包)</span></td>
														<?php elseif($alat_nama=='VOIP Phone'):?>
														<td>VOIP Phone (电话)</span></td>
														<?php elseif($alat_nama=='Kamera Digital'):?>
														<td>Kamera Digital (数码相机)</span></td>
														<?php elseif($alat_nama=='Baterai'):?>
														<td>Baterai (电池)</span></td>
														<?php elseif($alat_nama=='Konverter'):?>
														<td>Konverter (变换设备)</span></td>
														<?php elseif($alat_nama=='System Operasi'):?>
														<td>Sistem Operasi (系统操作)</span></td>
														<?php elseif($alat_nama=='Software / Aplikasi'):?>
														<td>Software / Aplikasi (软件)</span></td>
														<?php elseif($alat_nama=='Mesin Absen'):?>
														<td>Mesin Absen (指纹机)</span></td>
														<?php elseif($alat_nama=='Cable Tester'):?>
														<td>Cable Tester</span></td>
														<?php elseif($alat_nama=='Compass'):?>
														<td>Compass</span></td>
														<?php elseif($alat_nama=='Digital Video Recorder'):?>
														<td>Digital Video Recorder</span></td>
														<?php elseif($alat_nama=='GPS'):?>
														<td>GPS</span></td>
														<?php elseif($alat_nama=='Modem GSM'):?>
														<td>Modem GSM</span></td>
														<?php elseif($alat_nama=='Network Scanner'):?>
														<td>Network Scanner</span></td>
														<?php elseif($alat_nama=='Polycom'):?>
														<td>Polycom</span></td>
														<?php elseif($alat_nama=='Office 365 Personal'):?>
														<td>Office 365 Personal</span></td>
														<?php elseif($alat_nama=='PoE Adapter'):?>
														<td>PoE Adapter</span></td>
														<?php elseif($alat_nama=='Sound Mixing Console'):?>
														<td>Sound Mixing Console</span></td>
														<?php elseif($alat_nama=='Rack Wall 8U'):?>
														<td>Rack Box</span></td>
														<?php elseif($alat_nama=='TV'):?>
														<td>TV (电视)</span></td>
														<?php elseif($alat_nama=='Router'):?>
														<td>Router (路由器)</span></td>
														<?php elseif($alat_nama=='Access Point'):?>
														<td>Acces Point(切入点)</span></td>
														<?php elseif($alat_nama=='Solid State Drive (SSD)'):?>
														<td>Solid State Drive (固态硬盘)</span></td>
														<?php elseif($alat_nama=='Detektor'):?>
														<td>Detektor (探测器)</span></td>
														<?php elseif($alat_nama=='LAN Cable'):?>
														<td>LAN Cable (网线)</span></td>
														<?php elseif($alat_nama=='Drone'):?>
														<td>Drone 无人机</span></td>
														<?php else:?>	
														<td><span class="label label-warning">Random Acces Memory (随机存取存储器)</span></td>
														<?php endif;?>
        <td><?php echo $merek; ?></td>
        <td><?php echo $serial; ?></td>
        <td><?php echo $karyawan_nama; ?></td>
        <td><?php echo $lokasi_nama; ?><b>(<?php echo $divisi_nama; ?>)</b></td>
                                                        <?php if($status_nama=='BARU'):?>
														<td><span class="label label-success">NEW 新的</span></td>
														<?php elseif($status_nama=='TERPAKAI'):?>
														<td><span class="label label-default">USED 用过的</span></td>
														<?php elseif($status_nama=='PERBAIKAN'):?>
														<td><span class="label label-warning">REPAIR 维修</span></td>
														<?php elseif($status_nama=='HILANG'):?>
														<td><span class="label label-danger">LOSE 丢失了</span></td>
														<?php elseif($status_nama=='RUSAK'):?>
														<td><span class="label label-danger">DAMAGED 损坏的</span></td>
														<?php else:?>	
														<td><span class="label label-info">DUMP 倾倒</span></td>
														<?php endif;?>
		<td>
														<center>
															    <a href="<?php echo base_url().'assets/images/'.$qr_code;?>" data-lightbox="2" data-title="" target="_blank">
																    <center><img width="25" src="<?php echo base_url().'assets/images/'.$qr_code;?>" alt="qr_code" class="img-fluid "></center>
															    </a>
														</center>
		</td>
        </tr>
       	<?php endforeach;?>
      </tbody>
     </table>
     <a href="<?php echo base_url('statistik');?>" class="btn btn-primary btn-lg"> Data Grafik ( 图表中的数据 )</a>
    </div>
   </div>
  </div>

  <script type="text/javascript">
   $(document).ready(function() {
    $('table.display').DataTable();
   } );
  </script>
 </body>
 </html> 