
			<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				

				<ul class="nav nav-list">
					<li class="hover">
						<a href="<?php echo base_url('user/dashboard');?>">
							<i class="menu-icon fa fa-th-large bigger-110 orange"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-desktop bigger-110 purple"></i>
							<span class="menu-text">
								Master Data
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('user/kategori3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kategori
								</a>
								<b class="arrow"></b>
							</li>
								<li class="hover">
								<a href="<?php echo base_url('user/merek3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Merek/Manufaktur
								</a>
								<b class="arrow"></b>
							</li>
							
							<li class="hover">
								<a href="<?php echo base_url('user/ukuran3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Jenis/Ukuran/Volume
								</a>
								<b class="arrow"></b>
							</li>
							
							<li class="hover">
								<a href="<?php echo base_url('user/divisi3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Divisi
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/suplier3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Suplier
								</a>
								<b class="arrow"></b>
							</li>
							
														
						</ul>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil-square-o bigger-110 green"></i>
							<span class="menu-text"> Form Alat</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('user/form');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Form Barang Masuk
								</a>

								<b class="arrow"></b>
							</li>
						
						
						
							<li class="hover">
								<a href="<?php echo base_url('user/assign3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Baru
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/assign11');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat User
								</a>

								<b class="arrow"></b>
							</li>

							<li class="hover">
								<a href="<?php echo base_url('user/assign4');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Assign Pengguna
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list bigger-110 red"></i>
							<span class="menu-text"> Transfer Alat </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('user/transfer1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Transfer Alat
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/transfer2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Ganti Status Alat
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="hover">
						<a href="#">
							<i class="menu-icon fa fa-database bigger-110 "></i>
							<span class="menu-text"> Data Alat</span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('user/assign6');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									List Alat (All)
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/assign7');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Baru 
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/assign5');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Terpakai( User)
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('admin/assign10');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Alat Dalam Perbaikan
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/assign8');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Rusak Total
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('user/assign9');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Alat Status DUMP
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					
						<li class="hover">
						<a href="#">
							<i class="menu-icon  fa fa-map-marker bigger-110 red"></i>
							<span class="menu-text"> Statistik Alat </span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="#">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Berdasarkan Lokasi
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="#">
									<i class="menu-icon fa fa-caret-right "></i>
									Alat Berdasarkan Divisi
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
					</li>
					
					<li class="hover">
						<a href="#">
							<i class="menu-icon fa fa-user-plus bigger-110 purple"></i>
							<span class="menu-text"> User Alat </span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('user/karyawan3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									List User Alat
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="<?php echo base_url('user/jabatan3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Level Jabatan
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
					</li>
				
					<li class="hover">
						<a href="<?php echo base_url().'user/dashboard/logout'?>">
							<i class="menu-icon fa fa-sign-out bigger-110 green"></i>
							<span class="menu-text"> Keluar </span>
						</a>
						<b class="arrow"></b>
						
					</li>

				
				</ul>
			</div>
