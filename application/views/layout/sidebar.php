
			<div id="sidebar" class="sidebar  h-sidebar navbar-collapse collaps ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				

				<ul class="nav nav-list">
					<li class="hover">
						<a href="<?php echo base_url('page');?>">
							<i class="menu-icon fa fa-th-large bigger-110 orange"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-desktop bigger-110 purple"></i>
							<span class="menu-text">
								Master Data
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
<?php if($this->session->userdata('akses')=='admin'):?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/alat3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Jenis Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/kategori1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kategori
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/status1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kondisi Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/merek1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Merek/Manufaktur
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/ukuran1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Satuan
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/lokasi1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Lokasi 
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/divisi1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Divisi/Bagian
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/suplier1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Suplier
								</a>
								<b class="arrow"></b>
							</li>
							</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/kategori2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kategori
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/merek2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Merek/Manufaktur
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/ukuran2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Satuan
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/divisi2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Divisi / Bagian
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/suplier2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Suplier
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					<?php else:?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kategori
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Status Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Merek/Manufaktur
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Satuan
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Lokasi
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Divisi
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Suplier
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php endif;?>	
					</li>
					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil-square-o bigger-110 green"></i>
							<span class="menu-text"> Form Alat</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
<?php if($this->session->userdata('akses')=='admin'):?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/form1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Tanda Terima Barang
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/baru1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Barang Baru 
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/lama1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Barang Lama 
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/assign1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Assign Alat Ke Karyawan
								</a>
								<b class="arrow"></b>
							</li>
							
							<li class="hover">
								<a href="<?php echo base_url('page/file1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Upload Berkas
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/form2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Tanda Terima Barang
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/baru2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Barang Baru
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/lama2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Barang Lama 
								</a>
								<b class="arrow"></b>
							</li>

							<li class="hover">
								<a href="<?php echo base_url('page/assign2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Assign Alat Ke Karyawan
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php endif;?>	
					</li>
					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list bigger-110 red"></i>
							<span class="menu-text"> Mutasi Alat </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
<?php if($this->session->userdata('akses')=='admin'):?>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/transfer1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Pindah Pengguna
								</a>
								<b class="arrow"></b>
							</li>
						    <li class="hover">
								<a href="<?php echo base_url('page/transfer3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Ganti Kondisi Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/perbaikan1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Perbaikan Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/dump1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Dump Alat
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>
					<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/transfer2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Pindah User
								</a>
								<b class="arrow"></b>
							</li>
						    <li class="hover">
								<a href="<?php echo base_url('page/transfer4');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Ganti Status Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/perbaikan5');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Perbaikan Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/dump5');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Dump Alat
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php endif;?>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-database bigger-110 "></i>
							<span class="menu-text"> Data Alat</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
<?php if($this->session->userdata('akses')=='admin'):?>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/alat1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Semua Alat
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatbaru1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Baru 
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatterpakai1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Terpakai
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatrusak1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Rusak
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatperbaikan1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								 Perbaikan
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatdump1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								DUMP
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alathilang1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Hilang
								</a>
								<b class="arrow"></b>
							</li>
							</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>
							<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/alat2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Semua Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatbaru2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Baru 
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatterpakai2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Terpakai( User)
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatrusak2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Rusak
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatperbaikan2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Perbaikan
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alatdump2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								DUMP
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/alathilang2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Hilang
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php endif;?>	
					</li>
				<!--	<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon  fa fa-pie-chart bigger-110 red"></i>
							<span class="menu-text"> Statistik Alat </span>
						</a>
<?php if($this->session->userdata('akses')=='admin'):?>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/statistik1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Data Statistik Alat
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>
                        <ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/statistik2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Data Statistik Alat
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
<?php endif;?>
					</li>-->
						<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-user-plus bigger-110 purple"></i>
							<span class="menu-text"> Pengguna </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
<?php if($this->session->userdata('akses')=='admin'):?>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/karyawan1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Daftar Pengguna
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="<?php echo base_url('page/jabatan1');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Level Jabatan
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
<?php elseif($this->session->userdata('akses')=='user'):?>						
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('page/karyawan2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Daftar Pengguna
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="<?php echo base_url('page/jabatan2');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Level Jabatan
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
<?php endif;?>						
					</li>
					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-wrench bigger-110 blue"></i>
							<span class="menu-text"> Pengaturan </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>
                        <?php if($this->session->userdata('akses')=='admin'):?>
						<ul class="submenu">
						    	<li class="hover">
								<a href="<?php echo base_url('page/profil');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Profil Sistem
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/user1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									User Sistem
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('page/log1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Log Sistem
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
						
							<?php else:?>
						<?php endif;?>
						
					</li>
					<li class="hover">
						<a href="<?php echo base_url().'login/logout'?>">
							<i class="menu-icon fa fa-sign-out bigger-110 green"></i>
							<span class="menu-text"> Keluar </span>
						</a>
						<b class="arrow"></b>
						
					</li>
					
				
				</ul>
				
			</div>
