
			<div id="sidebar" class="sidebar  h-sidebar navbar-collapse collaps ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				

				<ul class="nav nav-list">
					<li class="hover">
						<a href="<?php echo base_url('front/dashboard');?>">
							<i class="menu-icon fa fa-th-large bigger-110 orange"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-desktop bigger-110 purple"></i>
							<span class="menu-text">
								Master Data
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/kategori3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Kategori
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/status3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Status Alat
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/merek3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Merek/Manufaktur
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/ukuran3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Jenis/Ukuran/Volume
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/lokasi3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Lokasi Kerja
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/divisi3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Divisi
								</a>
								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/suplier3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Suplier
								</a>
								<b class="arrow"></b>
							</li>
							
														
						</ul>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil-square-o bigger-110 green"></i>
							<span class="menu-text"> Form Alat</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/form');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Form Barang Masuk
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Baru
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign11');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat User
								</a>

								<b class="arrow"></b>
							</li>

							<li class="hover">
								<a href="<?php echo base_url('front/assign4');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Assign Pengguna
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list bigger-110 red"></i>
							<span class="menu-text"> Transfer Alat </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/transfer1');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Transfer Alat
								</a>

								<b class="arrow"></b>
							</li>
						    <li class="hover">
								<a href="<?php echo base_url('front/transfer2');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Ganti Status Alat
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="hover">
						<a href="#">
							<i class="menu-icon fa fa-database bigger-110 "></i>
							<span class="menu-text"> Data Alat</span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/assign6');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									List Alat (All)
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign7');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Baru 
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign5');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Terpakai( User)
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign10');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Alat Dalam Perbaikan
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign8');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Rusak Total
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign9');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Alat Status DUMP
								</a>

								<b class="arrow"></b>
							</li>
							<li class="hover">
								<a href="<?php echo base_url('front/assign10');?>">
									<i class="menu-icon fa fa-caret-right"></i>
								Alat Perbaikan/Service
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="hover">
						<a href="#">
							<i class="menu-icon  fa fa-map-marker bigger-110 red"></i>
							<span class="menu-text"> Statistik Alat </span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="#">
									<i class="menu-icon fa fa-caret-right"></i>
									Alat Berdasarkan Lokasi
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="#">
									<i class="menu-icon fa fa-caret-right "></i>
									Alat Berdasarkan Divisi
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
					</li>
						<li class="hover">
						<a href="#">
							<i class="menu-icon fa fa-user-plus bigger-110 purple"></i>
							<span class="menu-text"> User Alat </span>
						</a>

						<b class="arrow"></b>
						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/karyawan3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									List User Alat
								</a>

								<b class="arrow"></b>
							</li>

							
							<li class="hover">
								<a href="<?php echo base_url('front/jabatan3');?>">
									<i class="menu-icon fa fa-caret-right "></i>
									Level Jabatan
								</a>
								<b class="arrow"></b>
							</li>	
						</ul>
					</li>
					<li class="hover">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-wrench bigger-110 blue"></i>
							<span class="menu-text"> Setting Sistem </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="hover">
								<a href="<?php echo base_url('front/user3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									User Sistem
								</a>

								<b class="arrow"></b>
							</li>

							<li class="hover">
								<a href="<?php echo base_url('front/log3');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Log sistem
								</a>

								<b class="arrow"></b>
							</li>

							<li class="hover">
								<a href="<?php echo base_url('front/loguser');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Log User
								</a>

								<b class="arrow"></b>
							</li>	
						</ul>
					</li>
					<li class="hover">
						<a href="<?php echo base_url().'front/dashboard/logout'?>">
							<i class="menu-icon fa fa-sign-out bigger-110 green"></i>
							<span class="menu-text"> Keluar </span>
						</a>
						<b class="arrow"></b>
						
					</li>

				
				</ul>
			</div>
