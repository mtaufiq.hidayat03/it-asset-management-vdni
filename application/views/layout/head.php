<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>VDNI-IT IMS</title>
		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap.min.css'?>" />
		<link rel="icon" href="<?php echo base_url().'assets\ace\assets\images\avatars\16.png" type="image/x-icon'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/font-awesome/4.5.0/css/font-awesome.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/fonts.googleapis.com.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ace.min.css'?>" class="ace-main-stylesheet" id="main-ace-style" />	
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ace-part2.min.css'?>" class="ace-main-stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ace-skins.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ace-rtl.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ace-ie.min.css'?>" />		
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/jquery-ui.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-datepicker3.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/ui.jqgrid.min.css'?>" />
        <link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/jquery.gritter.min.css'?>" />		
		<script src="<?php echo base_url().'assets/ace/assets/js/ace-extra.min.js'?>"></script>	
		<script src="<?php echo base_url().'assets/ace/assets/js/html5shiv.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/respond.min.js'?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/chosen.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-duallistbox.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-multiselect.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/select2.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/jquery-ui.custom.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/jquery.gritter.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/jquery-ui.custom.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/chosen.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-datepicker3.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-timepicker.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/daterangepicker.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-datetimepicker.min.css'?>" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/ace/assets/css/bootstrap-colorpicker.min.css'?>" />
</head>
	
