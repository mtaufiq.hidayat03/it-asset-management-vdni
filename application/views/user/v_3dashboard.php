<!DOCTYPE html>
<html lang="en">
							<?php $this->load->view("layout/head.php") ?>
	<body class="no-skin">
							<?php $this->load->view("layout/navbar1.php") ?>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

							<?php $this->load->view("layout/sidebar.php") ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<div class="page-header">
							<h1>
								INVENTORY SYSTEM							
							</h1>
						</div>
						<div class="row">
							<div class="col-sm-2">
										<h4 class="header smaller lighter red">DATA STATISTIK</h4>

										<div class="well well-lg">
											<center><h4 class="blue">Total User Alat</h4></center>
											<center><h3><b><?php echo $total_karyawan;?></b></h3></center>
										</div>
										
										<div class="well">
											<center><h4 class="green smaller lighter">Total Lokasi</h4></center>
											<center><h3><b><?php echo $total_lokasi;?></b></h3></center>
										</div>

										<div class="well well-lg">
											<center><h4 class="blue">Total Divisi</h4></center>
											<center><h3><b><?php echo $total_divisi;?></b></h3></center>
										</div>
										
										
										
							</div>

							
							<div class="row">
									<div class="col-sm-2">
										<h4 class="header smaller lighter red">DATA ALAT</h4>

										<div class="well well-lg">
											<center><h4 class="blue">Total Seluruh Alat</h4></center>
											<center><h3><b><?php echo $total_jumlah_alat;?></b></h3></center>
										</div>
										
										<div class="well">
											<center><h4 class="green">Total Alat Baru</h4></center>
											<center><h3><b>Data Belum Tersaji</b></h3></center>
										</div>

										<div class="well well-lg">
											<center><h4 class="yellow">Total Alat Terpakai</h4></center>
											<center><h3><b>Data Belum Tersaji</b></h3></center>
										</div>
										
										
										
							</div>
								</div>						
						</div>												
					</div>
				</div>
			</div>
										<?php $this->load->view("layout/footer1.php") ?>

			
		</div>
		
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-2.1.4.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-1.11.3.min.js'?>"></script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url().'assets/ace/assets/js/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace-elements.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace.min.js'?>"></script>
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
		</script>
	</body>
</html>
