<?php
class Hrd extends CI_Controller{
  function __construct(){
    parent::__construct();
        $this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_karyawan');
		$this->load->model('m_kategori');
		$this->load->model('m_jabatan1');
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_assign');
		$this->load->model('m_merek');
		$this->load->model('m_ukuran');
		$this->load->model('m_suplier');
		$this->load->model('m_form');
		$this->load->model('m_grafik');
		$this->load->library('upload');
  }
 
  function index(){
           
         $x['product']=$this->m_assign->get_all_data();
         $this->load->view('admin/v_hrd');
  }
  
  function hasil()
	{
		$x['cari'] = $this->m_assign->cariAlat();
		$this->load->view('admin/v_cari', $x);
	}
  
}