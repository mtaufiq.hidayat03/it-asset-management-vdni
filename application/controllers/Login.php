<?php
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('m_login');
    }
    function index(){
        $this->load->view('v_login2');
    }
    function auth(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $cek_user=$this->m_login->auth_user($username,$password);
        if($cek_user->num_rows() > 0){ 
                $data=$cek_user->row_array();
                $this->session->set_userdata('masuk',TRUE);
                 if($data['role']=='admin'){ //Akses admin
                    $this->session->set_userdata('akses','admin');
                    $this->session->set_userdata('ses_id',$data['username']);
                    $this->session->set_userdata('ses_nama',$data['nama_user']);
                    echo $this->session->set_flashdata('msg','success');
                    helper_log("login", "masuk dari sistem");
                    redirect('page');
                }else{ //akses user
                    $this->session->set_userdata('akses','user');
                    $this->session->set_userdata('ses_id',$data['username']);
                    $this->session->set_userdata('ses_nama',$data['nama_user']);
                    echo $this->session->set_flashdata('msg','success');
                    helper_log("login", "masuk dari sistem");
                    redirect('page');
                    
                 }

        }else{ 
                    $cek_front=$this->m_login->auth_front($username,$password);
                    if($cek_front->num_rows() > 0){
                            $data=$cek_front->row_array();
                    $this->session->set_userdata('masuk',TRUE);
                            $this->session->set_userdata('akses','front');
                            $this->session->set_userdata('ses_id',$data['username']);
                            $this->session->set_userdata('ses_nama',$data['nama_user']);
                            helper_log("login", "masuk dari sistem");
                            redirect('front/dashboard');
                    }
                    else{  // jika username dan password tidak ditemukan atau salah
                            $url=base_url();
                            echo $this->session->set_flashdata('msg','error');
                            redirect($url);
                    }
        }
    }
    function logout(){
        $this->session->sess_destroy();
        helper_log("logout", "keluar dari sistem");
        $url=base_url('');
        redirect($url);
    }
}