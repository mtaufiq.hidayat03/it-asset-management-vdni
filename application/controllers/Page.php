<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
        $this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_karyawan');
		$this->load->model('m_kategori');
		$this->load->model('m_jabatan1');
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_assign');
		$this->load->model('m_merek');
		$this->load->model('m_ukuran');
		$this->load->model('m_suplier');
		$this->load->model('m_form');
		$this->load->model('m_grafik');
		$this->load->library('upload');
		$this->load->model('m_upload');
		$this->load->model('m_file');
		$this->load->model('m_alat');
		$this->load->model('m_profil');
		$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
  
    //**validasi jika user belum login**//
    if($this->session->userdata('masuk') != TRUE)
        {
            $url=base_url();
            redirect($url);
        }
  }
 
  function index(){
            $x['total_karyawan']= $this->m_karyawan->total_karyawan();
			$x['total_lokasi']	= $this->m_lokasi->total_lokasi();
			$x['total_user']	= $this->m_user->total_user();
			$x['total_divisi']	= $this->m_divisi->total_divisi();
			$x['total_alat']	= $this->m_assign->total_assign();
			$x['user']			= $this->m_user->show_user();
			$x['lokasi']		= $this->m_lokasi->show_lokasi();
			$x['log_data']		= $this->m_log->tampil_log();
			$x['data_alat']     = $this->m_assign->alat_terpakai();
			$x['jml_suplier']   = $this->m_suplier->jumlah_suplier();
			$x['alatlokasi1'] 	= $this->m_assign->alatlokasi();
			$x['alatstatus1'] 	= $this->m_assign->alatstatus();
			$x['alatlokasi2'] 	= $this->m_assign->get_alat_lokasi();
            $x['alatstatus2'] 	= $this->m_assign->get_alat_status();
            $x['alatkategori1'] = $this->m_assign->get_alat_kategori();
            $x['alatmerek1'] 	= $this->m_assign->alatmerek();
            $x['alatnama1'] 	= $this->m_assign->get_alat_nama();
            $x['alatmerek2'] 	= $this->m_assign->get_alat_merek();
            $x['form1'] 	    = $this->m_form->get_form_suplier();
            $x['profil'] 	    = $this->m_profil->get_all_profil();
            $this->load->view('admin/v_3dashboard',$x);
  }
  function statistik(){
            $x['total_karyawan']= $this->m_karyawan->total_karyawan();
			$x['total_lokasi']	= $this->m_lokasi->total_lokasi();
			$x['total_user']	= $this->m_user->total_user();
			$x['total_divisi']	= $this->m_divisi->total_divisi();
			$x['total_alat']	= $this->m_assign->total_assign();
			$x['user']			= $this->m_user->show_user();
			$x['lokasi']		= $this->m_lokasi->show_lokasi();
			$x['log_data']		= $this->m_log->tampil_log();
			$x['data_alat']     = $this->m_assign->alat_terpakai();
			$x['jml_suplier']   = $this->m_suplier->jumlah_suplier();
			$x['alatlokasi1'] 	= $this->m_assign->alatlokasi();
			$x['alatstatus1'] 	= $this->m_assign->alatstatus();
			$x['alatlokasi2'] 	= $this->m_assign->get_alat_lokasi();
            $x['alatstatus2'] 	= $this->m_assign->get_alat_status();
            $x['alatkategori1'] = $this->m_assign->get_alat_kategori();
            $x['alatmerek1'] 	= $this->m_assign->alatmerek();
            $x['form1'] 	    = $this->m_form->get_form_suplier();
            $this->load->view('admin/v_statistik',$x);
  }
 
  //** HAK AKSES ADMIN**//
  
  function kategori1(){
    if($this->session->userdata('akses')=='admin'){
      $x['data']=$this->m_kategori->get_all_kategori();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
      $this->load->view('admin/v_3kategori',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alat3(){
    if($this->session->userdata('akses')=='admin'){
      $x['data']=$this->m_alat->get_all_alat();
      $x['profil'] 	    = $this->m_profil->get_all_profil();
      $this->load->view('admin/v_3alat',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function upload1(){
    if($this->session->userdata('akses')=='admin'){
      $x['uploads']=$this->m_upload->all();
      $x['data']=$this->m_upload->get_all();
      $x['profil'] 	    = $this->m_profil->get_all_profil();
      $this->load->view('admin/v_upload',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  function file1(){
    if($this->session->userdata('akses')=='admin'){
      $x['files'] = $this->m_file->getAllFiles();
      $x['profil'] 	    = $this->m_profil->get_all_profil();
      $this->load->view('admin/file_upload', $x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  function lokasi1(){
    
    if($this->session->userdata('akses')=='admin'){
      $x['data']=$this->m_lokasi->get_all_lokasi();
      $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3lokasi',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function divisi1(){
    
    if($this->session->userdata('akses')=='admin'){
      $x['data']=$this->m_divisi->get_all_divisi();
	  $x['lokasi']=$this->m_lokasi->get_all_lokasi();
	   $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3divisi',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function status1(){
    
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_status->get_all_status();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3status',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function merek1(){
    
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_merek->get_all_merek();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3merek',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function suplier1(){
    
    if($this->session->userdata('akses')=='admin'){
        $x['suplier']=$this->m_suplier->get_all_suplier();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3suplier',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function ukuran1(){
    
    if($this->session->userdata('akses')=='admin'){
      $x['ukuran']=$this->m_ukuran->get_all_ukuran();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3ukuran',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function form1(){
    
    if($this->session->userdata('akses')=='admin'){
        $x['form']=$this->m_form->tampil_form();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['suplier']=$this->m_suplier->get_all_suplier();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3form',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function baru1(){
    if($this->session->userdata('akses')=='admin'){
        $x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['alat1']= $this->m_alat ->get_all_alat();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }

  function lama1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->alat_terpakai();
        $x['merek1']=$this->m_merek->get_all_merek();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['alat1']= $this->m_alat ->get_all_alat();
		$x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_11assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  function assign1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_4assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  function transfer1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->alat_terpakai();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3transfer',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function transfer3(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->get_all_data();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_4transfer',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function dump1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['suplier1']= $this->m_suplier  ->get_all_suplier();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_5dump',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function perbaikan1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['suplier1']= $this->m_suplier  ->get_all_suplier();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_5perbaikan',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alat1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_assign->get_all_data();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_6assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatbaru1(){
    if($this->session->userdata('akses')=='admin'){
		$x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_7assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatterpakai1(){
    if($this->session->userdata('akses')=='admin'){
		$x['data']=$this->m_assign->alat_terpakai();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_5assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatrusak1(){
    if($this->session->userdata('akses')=='admin'){
		$x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_8assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatperbaikan1(){
    if($this->session->userdata('akses')=='admin'){
		$x['data']=$this->m_assign->alat_perbaikan();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_10assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatdump1(){
    if($this->session->userdata('akses')=='admin'){
		$x['data']=$this->m_assign->alat_dump();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_9assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alathilang1(){
    if($this->session->userdata('akses')=='admin'){
        $x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_hilang();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_12assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function statistik1(){
    if($this->session->userdata('akses')=='admin'){
	   $x['alatlokasi1'] 	= $this->m_assign->get_alat_lokasi();
	   $x['alatstatus1'] 	= $this->m_assign->get_alat_status();
	   $x['alatkategori1'] 	= $this->m_assign->get_alat_kategori();
	   $x['alatmerek1'] 	= $this->m_assign->alatmerek();
	    $x['profil'] 	    = $this->m_profil->get_all_profil();
	   $this->load->view('admin/v_1statistiklokasi',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function karyawan1(){
    if($this->session->userdata('akses')=='admin'){
        $x['kar']=$this->m_karyawan->get_all_karyawan();
		$x['jabatan']=$this->m_jabatan1->get_all_jabatan();
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3karyawan',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function jabatan1(){
    if($this->session->userdata('akses')=='admin'){
        $x['data']=$this->m_jabatan1->get_all_jabatan();
		$this->load->view('admin/v_3jabatan',$x);
		 $x['profil'] 	    = $this->m_profil->get_all_profil();
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function user1(){
    if($this->session->userdata('akses')=='admin'){
      $x['users']=$this->m_user->get_all_user();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3user',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
   function log1(){
    if($this->session->userdata('akses')=='admin'){
      $x['log_data']	= $this->m_log->tampil_log();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3log',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function loguser1(){
    if($this->session->userdata('akses')=='admin'){
      $x['log_data'] = $this->m_log->tampil_log();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_3loguser',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function profil(){
    if($this->session->userdata('akses')=='admin'){
      $x['profil'] = $this->m_profil->get_all_profil();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_profil',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function tambahprofil(){
    if($this->session->userdata('akses')=='admin'){
      //$x['data'] = $this->m_profil->get_all_profil();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('admin/v_tambahprofil');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function cetak($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_1cetak',$x);
    }
function cetak2($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['databaru']=$this->m_assign->alat_baru();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_2cetak',$x);
    }
function cetak3($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['dataterpakai']=$this->m_assign->alat_terpakai();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_3cetak',$x);
    }
function cetak4($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['dataperbaikan']=$this->m_assign->alat_perbaikan();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_4cetak',$x);
    }
function cetak5($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['datarusak']=$this->m_assign->alat_rusak();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_5cetak',$x);
    }
function cetak6($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['datahilang']=$this->m_assign->alat_hilang();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_6cetak',$x);
    }
function cetak7($id)
    {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['datadump']=$this->m_assign->alat_dump();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_7cetak',$x);
    }
function cetaklama1($id)
   {
        $x['data'] = $this->m_assign->get_detail($id);
        $x['dataterpakai']=$this->m_assign->alat_terpakai();
         $x['profil'] 	    = $this->m_profil->get_all_profil();
		$this->load->view('admin/v_lama1cetak',$x);
    }
//HAK AKSES USER  
  
  function kategori2(){
    if($this->session->userdata('akses')=='user'){
       $x['data']=$this->m_kategori->get_all_kategori();
        $x['profil'] 	    = $this->m_profil->get_all_profil();
       $this->load->view('user/v_3kategori',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function status2(){
    if($this->session->userdata('akses')=='user'){
      $this->load->view('user/v_3status');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function merek2(){
    if($this->session->userdata('akses')=='user'){
      $x['data']=$this->m_merek->get_all_merek();
       $x['profil'] 	    = $this->m_profil->get_all_profil();
	  $this->load->view('user/v_3merek',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function ukuran2(){
    if($this->session->userdata('akses')=='user'){
      $x['ukuran']=$this->m_ukuran->get_all_ukuran();
       $x['profil'] = $this->m_profil->get_all_profil();
	  $this->load->view('user/v_3ukuran',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function lokasi2(){
    if($this->session->userdata('akses')=='user'){
      $this->load->view('user/v_3lokasi');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function divisi2(){
    if($this->session->userdata('akses')=='user'){
      $x['data']=$this->m_divisi->get_all_divisi();
      $x['lokasi']=$this->m_lokasi->get_all_lokasi();
      $x['profil'] = $this->m_profil->get_all_profil();
      $this->load->view('user/v_3divisi',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function suplier2(){
    if($this->session->userdata('akses')=='user'){
      $x['suplier']=$this->m_suplier->get_all_suplier();
      $x['profil'] = $this->m_profil->get_all_profil();
	  $this->load->view('user/v_3suplier',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function form2(){
    if($this->session->userdata('akses')=='user'){
        $x['form']=$this->m_form->tampil_form();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['suplier']=$this->m_suplier->get_all_suplier();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_3form',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function baru2(){
    if($this->session->userdata('akses')=='user'){
        $x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['alat1']= $this->m_alat ->get_all_alat();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_3assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function addalat2(){
    if($this->session->userdata('akses')=='user'){
      $x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_addalat',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function lama2(){
    if($this->session->userdata('akses')=='user'){
        $x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_terpakai();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['alat1']= $this->m_alat ->get_all_alat();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_11assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function assign2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_4assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function transfer2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_terpakai();
	 	$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_3transfer',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function transfer4(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->get_all_data();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_4transfer',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function perbaikan5(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['suplier1']= $this->m_suplier  ->get_all_suplier();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_5perbaikan',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function dump5(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['suplier1']= $this->m_suplier  ->get_all_suplier();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_5dump',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alat2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->get_all_data();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_6assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatbaru2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_7assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatterpakai2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_terpakai();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_5assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  function alatrusak2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_rusak();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_8assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatperbaikan2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_perbaikan();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_10assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alatdump2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_dump();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_9assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function statistik2(){
    if($this->session->userdata('akses')=='user'){
	   $x['alatlokasi1'] 	= $this->m_assign->get_alat_lokasi();
	   $x['alatstatus1'] 	= $this->m_assign->get_alat_status();
	   $x['alatkategori1'] 	= $this->m_assign->get_alat_kategori();
	   $x['alatmerek1'] 	= $this->m_assign->alatmerek();
	   $x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_1statistiklokasi',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function alathilang2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_assign->alat_hilang();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_13assign',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function karyawan2(){
    if($this->session->userdata('akses')=='user'){
      $x['kar']=$this->m_karyawan->get_all_karyawan();
	  $x['jabatan']=$this->m_jabatan1->get_all_jabatan();
	  $x['profil'] = $this->m_profil->get_all_profil();
      $this->load->view('user/v_3karyawan',$x);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  
  
  
  function jabatan2(){
    if($this->session->userdata('akses')=='user'){
        $x['data']=$this->m_jabatan1->get_all_jabatan();
        $x['profil'] = $this->m_profil->get_all_profil();
		$this->load->view('user/v_3jabatan',$x);;
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
}