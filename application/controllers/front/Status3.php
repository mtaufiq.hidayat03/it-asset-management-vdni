<?php
class Status3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_status');
        $this->load->library('upload');
		
	}


	function index(){
		$x['data']=$this->m_status->get_all_status();
		$this->load->view('front/v_3status',$x);
	}

	function simpan_status(){
		$status=strip_tags($this->input->post('xstatus'));
		$this->m_status->simpan_status($status);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan status alat");
		redirect('admin/status3');
	}

	function update_status(){
		$kode=strip_tags($this->input->post('kode'));
		$status=strip_tags($this->input->post('xstatus'));
		$this->m_status->update_status($kode,$status);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "mengapdate status alat");
		redirect('admin/status3');
	}
	function hapus_status(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_status->hapus_status($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus status alat");
		redirect('admin/status3');
	}
	

}