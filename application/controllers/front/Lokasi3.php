<?php
class Lokasi3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_lokasi');
        $this->load->library('upload');
		
	}


	function index(){
		$x['data']=$this->m_lokasi->get_all_lokasi();
		$this->load->view('front/v_3lokasi',$x);
	}

	function simpan_lokasi(){
		$lokasi=strip_tags($this->input->post('xlokasi'));
		$this->m_lokasi->simpan_lokasi($lokasi);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan lokasi kerja");
		redirect('admin/lokasi3');
	}

	function update_lokasi(){
		$id_lokasi=strip_tags($this->input->post('id_lokasi'));
		$lokasi=strip_tags($this->input->post('xlokasi'));
		$this->m_lokasi->update_lokasi($id_lokasi,$lokasi);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "mengapdate lokasi kerja");
		redirect('admin/lokasi3');
	}
	function hapus_lokasi(){
		$id_lokasi=strip_tags($this->input->post('id_lokasi'));
		$this->m_lokasi->hapus_lokasi($id_lokasi);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menhapus lokasi kerja");
		redirect('admin/lokasi3');
	}
	

}