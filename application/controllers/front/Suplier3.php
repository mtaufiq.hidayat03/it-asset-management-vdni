<?php
class Suplier3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_suplier');
        $this->load->library('upload');
		
	}


	function index(){
		$x['suplier']=$this->m_suplier->get_all_suplier();
		$this->load->view('front/v_3suplier',$x);
	}
	
	function simpan(){
        $suplier_id=$this->input->post('suplier_id');
        $suplier_nama=$this->input->post('suplier_nama');
		$alamat_nama=$this->input->post('alamat_nama');
        $this->m_suplier->simpan($suplier_id,$suplier_nama,$alamat_nama);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan suplier");
        redirect('admin/suplier3');
    }
	function update(){
        $karyawan_id=$this->input->post('karyawan_id');
        $karyawan_nik=$this->input->post('karyawan_nik');
		$karyawan_nama=$this->input->post('karyawan_nama');
        $jabatan_nama=$this->input->post('jabatan_nama');
        $this->m_karyawan->update($karyawan_id,$karyawan_nik,$karyawan_nama,$jabatan_nama);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "mengapdate data suplier");
        redirect('admin/karyawan3');
    }

	function hapus(){
		$suplier_id=strip_tags($this->input->post('suplier_id'));
		$this->m_suplier->hapus($suplier_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus data suplier");
		redirect('admin/suplier3');
	}
	

}