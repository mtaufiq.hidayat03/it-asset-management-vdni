<?php
class User3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_user');
        $this->load->library('upload');
		
	}


	function index(){
		$x['users']=$this->m_user->get_all_user();
		$this->load->view('front/v_3user',$x);
	}
	function simpan(){
		$id_user=$this->input->post('id_user');
		$nama_user=$this->input->post('nama_user');
		$username=$this->input->post('username');
		$password=MD5($this->input->post('password'));
		$role=$this->input->post('role');
		$status=$this->input->post('status');
        $this->m_user->simpan($id_user,$nama_user,$username,$password,$role,$status);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan user sistem");
        redirect('admin/user3');
    }
	function edit_user(){
        $id_user=$this->input->post('id_user');
		$nama_user=$this->input->post('nama_user');
		$username=$this->input->post('username');
		$password=MD5($this->input->post('password'));
		$role=$this->input->post('role');
		$status=$this->input->post('status');
        $this->m_user->edit_user($id_user,$nama_user,$username,$password,$role,$status);
		helper_log("edit", "mengupdate user sistem");
        redirect('admin/user3');
    }
	function ganti_password(){
        $id_user=$this->input->post('id_user');
		$nama_user=$this->input->post('nama_user');
		$username=$this->input->post('username');
		$password=MD5($this->input->post('password'));
		$role=$this->input->post('role');
		$status=$this->input->post('status');
        $this->m_user->edit_user($id_user,$nama_user,$username,$password,$role,$status);
		helper_log("edit", "mengganti password user sistem");
        redirect('admin/user3');
    }

	function hapus(){
		$id_user=strip_tags($this->input->post('id_user'));
		$this->m_user->hapus_user($id_user);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus user sistem");
		redirect('admin/user3');
	}
	

}