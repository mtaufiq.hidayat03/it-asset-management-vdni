<?php
class Kategori3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_kategori');
        $this->load->library('upload');
		
	}


	function index(){
		$x['data']=$this->m_kategori->get_all_kategori();
		$this->load->view('front/v_3kategori',$x);
	}

	function simpan_kategori(){
		$kategori=strip_tags($this->input->post('xkategori'));
		$this->m_kategori->simpan_kategori($kategori);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan kategori");
		redirect('admin/kategori3');
	}

	function update_kategori(){
		$kode=strip_tags($this->input->post('kode'));
		$kategori=strip_tags($this->input->post('xkategori'));
		$this->m_kategori->update_kategori($kode,$kategori);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update kategori");
		redirect('admin/kategori3');
	}
	function hapus_kategori(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_kategori->hapus_kategori($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus kategori");
		redirect('admin/kategori3');
	}
	

}