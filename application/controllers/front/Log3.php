<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log3 extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('m_login');						
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_user');
		$this->load->model('m_kategori');
		$this->load->model('m_stok');
		$this->load->model('m_log');
		
        //cek session dan level user
        if($this->m_login->is_role() != "front"){
            redirect("login/");
        }
    }

    function index(){
			
						
			$x['log_data']	= $this->m_log->tampil_log();
			
			
			
			$this->load->view('front/v_3log',$x);
	
	}

    

}