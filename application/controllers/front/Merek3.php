<?php
class Merek3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_merek');
        $this->load->library('upload');
		
	}


	function index(){
		$x['data']=$this->m_merek->get_all_merek();
		$this->load->view('front/v_3merek',$x);
	}

	function simpan_merek(){
		$merek=strip_tags($this->input->post('xmerek'));
		$this->m_merek->simpan_merek($merek);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan merek");
		redirect('admin/merek3');
	}

	function update_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$merek=strip_tags($this->input->post('xmerek'));
		$this->m_merek->update_merek($kode,$merek);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update merek");
		redirect('admin/merek3');
	}
	function hapus_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_merek->hapus_merek($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus merek");
		redirect('admin/merek3');
	}
	

}