<?php
class Aset extends CI_Controller{
  function __construct(){
    parent::__construct();
        $this->load->model('m_assign');
		$this->load->model('m_divisi');
		$this->load->model('m_lokasi');
		$this->load->model('m_karyawan');
		$this->load->model('m_status');
		$this->load->model('m_kategori');
		$this->load->model('m_ukuran');
		$this->load->helper('url');
        $this->load->library('upload');
  }
  function index(){
        $x['data1']=$this->m_assign->get_all_data();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
         $this->load->view('admin/v_aset',$x);
  }
  
}