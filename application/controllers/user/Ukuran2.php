<?php
class Ukuran2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_ukuran');
        $this->load->library('upload');
	}


	function index(){
		redirect('page/ukuran2');	
	    
	}

	function simpan_ukuran(){
		$ukuran=strip_tags($this->input->post('xukuran'));
		$this->m_ukuran->simpan_ukuran($ukuran);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan ukuran");
		redirect('user/ukuran2');
	}

	function update_ukuran(){
		$ukuran_id=strip_tags($this->input->post('ukuran_id'));
		$ukuran=strip_tags($this->input->post('xukuran'));
		$this->m_ukuran->update_ukuran($ukuran_id,$ukuran);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update ukuran");
		redirect('user/ukuran2');
	}
	function hapus_ukuran(){
		$ukuran_id=strip_tags($this->input->post('ukuran_id'));
		$this->m_ukuran->hapus_ukuran($ukuran_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus ukuran");
		redirect('user/ukuran2');
	}
	

}