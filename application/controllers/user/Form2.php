<?php
class Form2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		
		$this->load->model('m_form');
		$this->load->model('m_divisi');
		$this->load->model('m_suplier');
        $this->load->library('upload');
	}

	function index(){
		redirect('page/form2');	
	    
	}
	function get_divisi(){
        $divisi_id=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_suplier(){
        $suplier_id=$this->input->post('suplier_id');
        $data=$this->m_suplier->get_suplier_byid($suplier_id);
        echo json_encode($data);
    }
	
	
	function simpan_form(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '100%';
	                        $config['width']= 500;
	                        $config['height']= 870;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$form_id=$this->input->post('form_id');
							$nomor_form=$this->input->post('nomor_form');
							$po_form=$this->input->post('po_form');
							$divisi_nama=$this->input->post('divisi_nama');
							$suplier_nama=$this->input->post('suplier_nama');
							$tgl_masuk=$this->input->post('tgl_masuk');
							$serial=$this->input->post('serial');
							$this->m_form->simpan_form($foto,$form_id,$nomor_form,$po_form,$divisi_nama,$suplier_nama,$tgl_masuk,$serial);
							echo $this->session->set_flashdata('msg','success');
							helper_log("add", "menambahkan tanda terima barang");
							redirect('user/form2');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
	                    	redirect('user/form2');
	               			}
	                 
	            			}else{
							redirect('user/form2');
							}
				
	}

	function hapus_form(){
		$form_id=strip_tags($this->input->post('form_id'));
		$this->m_form->hapus_form($form_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus tanda terima barang");
		redirect('user/form2');
	}
	

}