<?php
class Assign2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_assign');
		$this->load->model('m_divisi');
		$this->load->model('m_lokasi');
		$this->load->model('m_karyawan');
		$this->load->model('m_status');
		$this->load->model('m_kategori');
		$this->load->model('m_ukuran');
        $this->load->library('upload');
		
	}

	function index(){
	    redirect('page/assign2');
	}
	function get_divisi(){
        $divisi_id=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_kategori(){
        $kategori_id=$this->input->post('kategori_id');
        $data=$this->m_kategori->get_kategori_byid($kategori_id);
        echo json_encode($data);
    }
	function get_lokasi(){
        $lokasi_id=$this->input->post('lokasi_id');
        $data=$this->m_lokasi->get_lokasi_byid($lokasi_id);
        echo json_encode($data);
    }
	function get_karyawan(){
        $karyawan_id=$this->input->post('karyawan_id');
        $data=$this->m_karyawan->get_karyawan_byid($karyawan_id);
        echo json_encode($data);
    }
	function get_status(){
        $status_id=$this->input->post('status_id');
        $data=$this->m_status->get_status_byid($status_id);
        echo json_encode($data);
    }
	function get_ukuran(){
        $ukuran_id=$this->input->post('ukuran_id');
        $data=$this->m_ukuran->get_ukuran_byid($ukuran_id);
       echo json_encode($data);
    }
	

	function assign_user(){
			$assign_id		=$this->input->post('assign_id');
			$alat_nama		=$this->input->post('alat_nama');
			$kategori_nama	=$this->input->post('kategori_nama');
			$merek			=$this->input->post('merek');
			$model			=$this->input->post('model');
			$serial			=$this->input->post('serial');
			$karyawan_nama	=$this->input->post('karyawan_nama');
			$lokasi_nama	=$this->input->post('lokasi_nama');
			$divisi_nama	=$this->input->post('divisi_nama');
			$deskripsi		=$this->input->post('deskripsi');
			$jumlah			=$this->input->post('jumlah');
			$ukuran			=$this->input->post('ukuran');
			$status_nama	=$this->input->post('status_nama');
			
			
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = './assets/'; //string, the default is application/cache/
            $config['errorlog']     = './assets/'; //string, the default is application/logs/
            $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224,255,255); // array, default is array(255,255,255)
            $config['white']        = array(70,130,180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);
     
            $qr_code=$serial.'.png'; //buat name dari qr code sesuai dengan nim
     
            $params['data'] = $serial; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH.$config['imagedir'].$qr_code; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			
			
			
			
			
			
			$this->m_assign->assign_alat($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama,$qr_code);
			echo $this->session->set_flashdata('msg','info');
			helper_log("add", "assign alat ke user");
			redirect('user/assign2');
		}	



}
