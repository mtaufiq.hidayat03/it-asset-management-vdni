<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
       
		$this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_user');
		$this->load->model('m_kategori');
		$this->load->model('m_log');
		$this->load->model('m_karyawan');
		$this->load->model('m_assign');
	//	$this->auth->cek_auth();
		
        //if($this->m_login->is_role() != "user"){
           // redirect("login/");
        //}
        if($this->session->userdata('masuk') != TRUE){
            $url=base_url();
            redirect($url);
        }
    }

    function index(){
			
						
			$x['total_user']		= $this->m_user->total_user();
			$x['total_karyawan']	= $this->m_karyawan->total_karyawan();
			$x['total_assign']		= $this->m_assign->total_assign();
			$x['total_jumlah_alat']	= $this->m_assign->total_jumlah_alat();
			$x['total_lokasi']		= $this->m_lokasi->total_lokasi();
			$x['total_divisi']		= $this->m_divisi->total_divisi();
			$x['user']				= $this->m_user->show_user();
			$x['lokasi']			= $this->m_lokasi->show_lokasi();
		//	$x['alat'] 				= $this->m_alat3->tampil_alat();
			$x['log_data']			= $this->m_log->tampil_log();
			
			
			$this->load->view('user/v_3dashboard',$x);
	
	}
	
	function getdata(){
        $data  = $this->m_grafik->getdata();
        print_r(json_encode($data, true));
    }

	function get_stok(){
		$data = $this->m_grafik->tampil_alat();
		header('Content-Type: application/json');
		echo $data;
	}
	
	function get_alat(){
		$data = $this->m_grafik->tampil_alat();
		print_r(json_encode($data, true));
	}

   // function logout()
   // {
       // $this->session->sess_destroy();
	//	helper_log("logout", "keluar dari sistem");

       // redirect('login');
   // }
   
   
   function logout() 
    {
    helper_log("hapus", "keluar dari sistem");
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('role');
    session_destroy();
    redirect('login');
    }

}
