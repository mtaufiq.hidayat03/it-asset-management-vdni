<?php
class Merek2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_merek');
        $this->load->library('upload');
		
	}


	function index(){
		redirect('page/merek2');	
	    
	}

	function simpan_merek(){
		$merek=strip_tags($this->input->post('xmerek'));
		$this->m_merek->simpan_merek($merek);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan merek");
		redirect('user/merek2');
	}

	function update_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$merek_nama=strip_tags($this->input->post('xmerek'));
		$this->m_merek->update_merek($kode,$merek_nama);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update merek");
		redirect('user/merek2');
	}
	function hapus_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_merek->hapus_merek($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus merek");
		redirect('user/merek2');
	}
	

}