<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DATA extends CI_Controller {

      function __construct(){

            parent::__construct();

				$this->load->model('m_alat');
				$this->load->model('m_status');
				$this->load->model('m_kategori');
				$this->load->model('m_lokasi');
				$this->load->model('m_divisi');
				$this->load->model('m_device');
				$this->load->model('m_user');                
				$this->load->library('upload');
      }

     

      public function index(){

            $x['alat']=$this->m_alat->tampil_alat();
			$x['device']=$this->m_device->get_all_device();
			$x['kategori']  = $this->m_kategori->get_all_kategori();
			$x['device']	= $this->m_device  ->get_all_device();
			$x['lokasi']	= $this->m_lokasi  ->get_all_lokasi();
			$x['status']	= $this->m_status  ->get_all_status();
			$x['divisi']	= $this->m_divisi  ->get_all_divisi();
			$x['user']		= $this->m_user    ->get_all_user();
            $this->load->view('admin/v_data',$x);

      }
	  
		
		public function pencarian(){
							$nama_alat=$this->input->get('nama_alat');
							$kategori_nama=$this->input->get('kategori_nama');
							$nama_user=$this->input->get('nama_user');
							$divisi_nama=$this->input->get('divisi_nama');
							$lokasi_nama=$this->input->get('lokasi_nama');
							$status_alat=$this->input->get('status_alat');
							$x['hasil'] = $this->m_alat->pencarian($nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat)->result_array();
				redirect('admin/datahasil'); // ini view menampilkan hasil pencarian
		}

}