<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alatdivisi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('m_grafik');
		$this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_user');
		$this->load->model('m_alat');
		$this->load->model('m_device');
		$this->load->model('m_kategori');
		$this->load->model('m_stok');
		$this->load->model('m_log');
        //cek session dan level user
        if($this->m_login->is_role() != "admin"){
            redirect("login/");
        }
    }

    function index(){
			
						
			$x['total_user']	= $this->m_user->total_user();
			$x['total_lokasi']	= $this->m_lokasi->total_lokasi();
			$x['total_divisi']	= $this->m_divisi->total_divisi();
			$x['jml']			= $this->m_stok->get_all_jumlah();
			$x['jml_alat']		= $this->m_alat->total_alat();
			$x['user']			= $this->m_user->show_user();
			$x['lokasi']		= $this->m_lokasi->show_lokasi();
			$x['alat'] 			= $this->m_alat->tampil_alat();
			$x['log_data']		= $this->m_log->tampil_log();
			$x['datagrafik']	= $this->m_grafik->tampil_alat();
			$x['data'] 			= $this->m_alat->tampil_alatbaru();
			$x['data1'] 			= $this->m_alat->tampil_alatlokasi();
			$x['data2'] 			= $this->m_alat->tampil_alatdivisi();
			
			
			
			$this->load->view('admin/v_alatdivisi',$x);
	
	}
	
	function getdata(){
        $data  = $this->m_grafik->getdata();
        print_r(json_encode($data, true));
    }
	
	
	
	
	
	
	
	
	function get_stok(){
		$data = $this->m_grafik->tampil_alat();
		header('Content-Type: application/json');
		echo $data;
	}
	
	function get_alat(){
		$data = $this->m_grafik->tampil_alat();
		print_r(json_encode($data, true));
	}

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}