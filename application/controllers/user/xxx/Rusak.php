<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rusak extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->model('m_rusak');
			$this->load->model('m_device');
			$this->load->model('m_kategori');
			$this->load->model('m_status');
			$this->load->model('m_stok');

      }

     

      public function index(){

            $x['rusak']=$this->m_rusak->show_rusak();
			$x['data']=$this->m_device->get_all_device();
            $this->load->view('admin/v_rusak',$x);

      }

}