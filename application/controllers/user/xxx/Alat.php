<?php 

class Alat extends CI_Controller{

	function __construct(){
		parent::__construct();	
				$this->load->model('m_alat');
				$this->load->model('m_status');
				$this->load->model('m_kategori');
				$this->load->model('m_lokasi');
				$this->load->model('m_divisi');
				$this->load->model('m_device');
				$this->load->model('m_user');                
				$this->load->library('upload');
	}

	function index(){
		$x['alat'] 		= $this->m_alat->tampil_alat();
		$x['kategori']  = $this->m_kategori->get_all_kategori();
		$x['device']	= $this->m_device  ->get_all_device();
		$x['lokasi']	= $this->m_lokasi  ->get_all_lokasi();
		$x['status']	= $this->m_status  ->get_all_status();
		$x['divisi']	= $this->m_divisi  ->get_all_divisi();
		$x['user']		= $this->m_user    ->get_all_user();
				
		$this->load->view('admin/v_alat',$x);
	}
	function get_kategori(){
        $id=$this->input->post('id');
        $data=$this->m_kategori->get_kategori_byid($kategori_id);
        echo json_encode($data);
    }
	function get_status(){
        $status_id=$this->input->post('status_id');
        $data=$this->m_status->get_status_byid($status_id);
       echo json_encode($data);
    }
	function get_user(){
        $id_user=$this->input->post('id_user');
        $data=$this->m_user->get_user_byid($id);
        echo json_encode($data);
    }
	//function simpan_alat(){
				//$config['upload_path'] = './assets/images/'; //path folder
	           // $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	           // $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            //$this->upload->initialize($config);
	           // if(!empty($_FILES['filefoto']['name']))
	           // {
	               // if ($this->upload->do_upload('filefoto'))
	               // {
	                       // $gbr = $this->upload->data();
	                        //Compress Image
	                       // $config['image_library']='gd2';
	                       // $config['source_image']='./assets/images/'.$gbr['file_name'];
	                       // $config['create_thumb']= FALSE;
	                        //$config['maintain_ratio']= FALSE;
	                       // $config['quality']= '60%';
	                       // $config['width']= 840;
	                       // $config['height']= 450;
	                       // $config['new_image']= './assets/images/'.$gbr['file_name'];
	                       // $this->load->library('image_lib', $config);
	                        //$this->image_lib->resize();

	                       // $foto=$gbr['file_name'];
							//$alat_id=$this->input->post('alat_id');
							//$nama_alat=$this->input->post('nama_alat');
							//$kategori_nama=$this->input->post('kategori_nama');
							//$nama_user=$this->input->post('nama_user');
							//$divisi_nama=$this->input->post('divisi_nama');
							//$lokasi_nama=$this->input->post('lokasi_nama');
							//$status_alat=$this->input->post('status_alat');
							//$deskripsi=$this->input->post('deskripsi');
							//$this->m_alat->simpan($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,//$status_alat,$deskripsi);
							//echo $this->session->set_flashdata('msg','success');
							//helper_log("add", "tambah assign device");
							//redirect('admin/alat');
							//}else{
	                    	//echo $this->session->set_flashdata('msg','warning');
							//helper_log("add", "tambah assign device");
	                    	//redirect('admin/alat');
	               			//}
	                 
	            			//}else{
							//redirect('admin/alat');
							//}
				
	//}
	//function edit_alat(){
							//$config['upload_path'] = './assets/images/'; //path folder
							//$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
							//$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

							//$this->upload->initialize($config);
							//if(!empty($_FILES['filefoto']['name']))
							//{
								//if ($this->upload->do_upload('filefoto'))
								//{
	                       // $gbr = $this->upload->data();
	                        //Compress Image
	                       /// $config['image_library']='gd2';
	                       // $config['source_image']='./assets/images/'.$gbr['file_name'];
	                       // $config['create_thumb']= FALSE;
	                       // $config['maintain_ratio']= FALSE;
	                       // $config['quality']= '60%';
	                       // $config['width']= 300;
	                       // $config['height']= 300;
	                       // $config['new_image']= './assets/images/'.$gbr['file_name'];
	                       // $this->load->library('image_lib', $config);
	                       // $this->image_lib->resize();

	                        //$foto=$gbr['file_name'];
							//$alat_id=$this->input->post('alat_id');
							//$nama_alat=$this->input->post('nama_alat');
							//$kategori_nama=$this->input->post('kategori_nama');
							//$nama_user=$this->input->post('nama_user');
							//$divisi_nama=$this->input->post('divisi_nama');
							//$lokasi_nama=$this->input->post('lokasi_nama');
							//$status_alat=$this->input->post('status_alat');
							//$deskripsi=$this->input->post('deskripsi');
							//$this->m_alat->edit_alat($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,//$status_alat,$deskripsi);
							//echo $this->session->set_flashdata('msg','info');
							//helper_log("edit", "update transfer user  device");
							//redirect('admin/alat');
	               			//}	                    	               	                
							
							//}else{
							//$alat_id=$this->input->post('alat_id');
							//$nama_alat=$this->input->post('nama_alat');
							//$kategori_nama=$this->input->post('kategori_nama');
							//$nama_user=$this->input->post('nama_user');
							//$divisi_nama=$this->input->post('divisi_nama');
							//$lokasi_nama=$this->input->post('lokasi_nama');
							//$status_alat=$this->input->post('status_alat');
							//$deskripsi=$this->input->post('deskripsi');	            	
							//$this->m_alat->edit_alat_tanpa_gambar($alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,//$lokasi_nama,$status_alat,$deskripsi);
							//echo $this->session->set_flashdata('msg','warning');
							//helper_log("edit", "update transfer user  device");
							//redirect('admin/alat');
							//}
	//}
	//function edit_alat1(){
						//	$config['upload_path'] = './assets/images/'; //path folder
						//	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
							//$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

							//$this->upload->initialize($config);
							//if(!empty($_FILES['filefoto']['name']))
							//{
							//	if ($this->upload->do_upload('filefoto'))
							//	{
	                       // $gbr = $this->upload->data();
	                        //Compress Image
	                        //$config['image_library']='gd2';
	                        ///$config['source_image']='./assets/images/'.$gbr['file_name'];
	                        //$config['create_thumb']= FALSE;
	                        //$config['maintain_ratio']= FALSE;
	                        //$config['quality']= '60%';
	                        //$config['width']= 300;
	                       // $config['height']= 300;
	                       // $config['new_image']= './assets/images/'.$gbr['file_name'];
	                       // $this->load->library('image_lib', $config);
	                        //$this->image_lib->resize();

	                       // $foto=$gbr['file_name'];
							//$alat_id=$this->input->post('alat_id');
							///$nama_alat=$this->input->post('nama_alat');
							//$kategori_nama=$this->input->post('kategori_nama');
							//$nama_user=$this->input->post('nama_user');
							//$divisi_nama=$this->input->post('divisi_nama');
							//$lokasi_nama=$this->input->post('lokasi_nama');
							//$status_alat=$this->input->post('status_alat');
							//$deskripsi=$this->input->post('deskripsi');
							////$this->m_alat->edit_alat($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,//$status_alat,$deskripsi);
							//echo $this->session->set_flashdata('msg','info');
							//helper_log("edit", "update status alat di assign device");
							//redirect('admin/alat');
	               			//}else{
	                    	//echo $this->session->set_flashdata('msg','warning');
							//helper_log("edit", "update status alat di assign device");
	                    	//redirect('admin/alat');
	               			//}
	                 
	            			//}else{
							//redirect('admin/alat');
							//}
	//}
	
	
	function simpan_alat(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');
							$this->m_alat->simpan($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','success');
							helper_log("add", "tambah assign device");
							redirect('admin/alat');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
							helper_log("add", "tambah assign device");
	                    	redirect('admin/alat');
	               			}
	                 
	            			}else{
							redirect('admin/alat');
							}
				
	}
	function edit_alat(){
							$config['upload_path'] = './assets/images/'; //path folder
							$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
							$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

							$this->upload->initialize($config);
							if(!empty($_FILES['filefoto']['name']))
							{
								if ($this->upload->do_upload('filefoto'))
								{
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 300;
	                        $config['height']= 300;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');
							$this->m_alat->edit_alat($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','info');
							helper_log("edit", "update transfer user  device");
							redirect('admin/alat');
	               			}	                    	               	                
							
							}else{
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');	
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');							
							$this->m_alat->edit_alat_tanpa_gambar1($alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','warning');
							helper_log("edit", "update transfer user  device");
							redirect('admin/alat');
							}
	}
	function edit_alat1(){
							$config['upload_path'] = './assets/images/'; //path folder
							$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
							$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

							$this->upload->initialize($config);
							if(!empty($_FILES['filefoto']['name']))
							{
								if ($this->upload->do_upload('filefoto'))
								{
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 300;
	                        $config['height']= 300;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');
							$this->m_alat->edit_alat1($alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','info');
							helper_log("edit", "update status alat di assign device");
							redirect('admin/alat');
	               			}else{
	                    	echo $this->session->set_flashdata('msg','warning');
							helper_log("edit", "update status alat di assign device");
	                    	redirect('admin/alat');
	               			}
	                 
	            			}else{
							redirect('admin/alat');
							}
	}
	function edit_alat2(){
							$config['upload_path'] = './assets/images/'; //path folder
							$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
							$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

							$this->upload->initialize($config);
							if(!empty($_FILES['filefoto']['name']))
							{
								if ($this->upload->do_upload('filefoto'))
								{
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 300;
	                        $config['height']= 300;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');
							$this->m_alat->edit_alat2($foto,$alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','info');
							helper_log("edit", "update data alat");
							redirect('admin/alat');
	               			}	                    	               	                
							
							}else{
							$alat_id=$this->input->post('alat_id');
							$nama_alat=$this->input->post('nama_alat');
							$kategori_nama=$this->input->post('kategori_nama');
							$nama_user=$this->input->post('nama_user');
							$divisi_nama=$this->input->post('divisi_nama');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$status_alat=$this->input->post('status_alat');
							$deskripsi=$this->input->post('deskripsi');	
							$aset_nomor=$this->input->post('aset_nomor');
							$merek=$this->input->post('merek');
							$model=$this->input->post('model');
							$serial=$this->input->post('serial');							
							$this->m_alat->edit_alat_tanpa_gambar2($alat_id,$nama_alat,$kategori_nama,$nama_user,$divisi_nama,$lokasi_nama,$status_alat,$deskripsi,$aset_nomor,$merek,$model,$serial);
							echo $this->session->set_flashdata('msg','warning');
							helper_log("edit", "update data alat ");
							redirect('admin/alat');
							}
	}
	
	
	
	
	
	
	
	function hapus_alat(){
        $alat_id=$this->input->post('alat_id');
        $this->m_alat->hapus_alat($alat_id);
	    echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus assign device");
        redirect('admin/alat');
    }
	
	
}