<?php 

class User extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_user');
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_shift');
        $this->load->library('upload');
	}

	function index(){
        $x['user']=$this->m_user->show_user();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['data']=$this->m_jabatan->get_all_jabatan();
		$x['shift']=$this->m_shift->get_all_shift();
        $this->load->view('admin/v_user',$x);
    }
	function get_divisi(){
        $divisi_idid=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_jabatan(){
        $id_jabatan=$this->input->post('id_jabatan');
        $data=$this->m_jabatan->get_jabatan_byid($id_jabatan);
        echo json_encode($data);
    }
	function get_shift(){
        $shift_id=$this->input->post('shift_id');
        $data=$this->m_shift->get_shift_byid($shift_id);
        echo json_encode($data);
    }
	
	
	function simpan(){
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=MD5($this->input->post('password'));
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$work_nama=$this->input->post('work_nama');
							$role=$this->input->post('role');
							$foto=$this->input->post('foto');
							$status=$this->input->post('status');
							$this->m_user->salin($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$work_nama,$role,$foto,$status);
        redirect('admin/user');
    }
	
	function salin(){
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=$this->input->post('password');
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$work_nama=$this->input->post('work_nama');
							$role=$this->input->post('role');
							$foto=$this->input->post('foto');
							$tatus=$this->input->post('status');
							$this->m_user->simpan($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$work_nama,$role,$foto,$status);
        redirect('admin/user');
    }
	
	function update(){
        
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=MD5($this->input->post('password'));
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$work_nama=$this->input->post('work_nama');
							$role=$this->input->post('role');
							$foto=$this->input->post('foto');
							$this->m_user->edit($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$work_nama,$role,$foto);
        redirect('admin/user');
    }
	
	
	function hapus_user(){
        $id_user=$this->input->post('id_user');
        $this->m_user->hapus_user($id_user);
        redirect('admin/user');
    }

}