<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Alatlokasi extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->model('m_stoklokasi');
			$this->load->model('m_device');

      }

     

      public function index(){

            $x['alatlokasi']=$this->m_stoklokasi->show_stoklokasi();
			$x['data']=$this->m_device->get_all_device();
            $this->load->view('admin/v_stoklokasi1',$x);

      }

}