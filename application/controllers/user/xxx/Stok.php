<?php
class Stok extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_device');
		$this->load->model('m_kategori');
		$this->load->model('m_status');
		$this->load->model('m_stok');
        $this->load->library('upload');
	}


	function index(){
		$x['stok']= $this->m_stok->tampil_stok();
		$x['data']=$this->m_device->get_all_device();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['status']=$this->m_status->get_all_status();
		$x['device']= $this->m_device  ->get_all_device();
		$this->load->view('admin/v_stok',$x);
	}
	function get_kategori(){
        $id=$this->input->post('id');
        $data=$this->m_kategori->get_kategori_byid($kategori_id);
        echo json_encode($data);
    }
	function get_status(){
        $status_id=$this->input->post('status_id');
        $data=$this->m_status->get_status_byid($status_id);
       echo json_encode($data);
    }

	
	function simpan_stok(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$stok_id=$this->input->post('stok_id');
							$aset_nama=$this->input->post('aset_nama');
							$kategori_nama=$this->input->post('kategori_nama');
							$merek_nama=$this->input->post('merek_nama');
							$model_nama=$this->input->post('model_nama');
							$tipe_nama=$this->input->post('tipe_nama');
							$serial_nama=$this->input->post('serial_nama');
							$status_nama=$this->input->post('status_nama');
							$ket_nama=$this->input->post('ket_nama');
							$tgl=$this->input->post('tgl');
							$jml=$this->input->post('jml');
							$this->m_stok->simpan_stok($foto,$stok_id,$aset_nama,$kategori_nama,$merek_nama,$model_nama,$tipe_nama,$serial_nama,$status_nama,$ket_nama,$tgl,$jml);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/stok');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
	                    	redirect('admin/stok');
	               			}
	                 
	            			}else{
							redirect('admin/stok');
							}
				
	}
	
	function edit_stok(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$stok_id=$this->input->post('stok_id');
							$aset_nama=$this->input->post('aset_nama');
							$kategori_nama=$this->input->post('kategori_nama');
							$merek_nama=$this->input->post('merek_nama');
							$model_nama=$this->input->post('model_nama');
							$tipe_nama=$this->input->post('tipe_nama');
							$serial_nama=$this->input->post('serial_nama');
							$status_nama=$this->input->post('status_nama');
							$ket_nama=$this->input->post('ket_nama');
							$tgl=$this->input->post('tgl');
							$jml=$this->input->post('jml');
							$this->m_stok->edit_stok($foto,$stok_id,$aset_nama,$kategori_nama,$merek_nama,$model_nama,$tipe_nama,$serial_nama,$status_nama,$ket_nama,$tgl,$jml);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/stok');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
	                    	redirect('admin/stok');
	               			}
	                 
	            			}else{
							redirect('admin/stok');
							}
				
	}

	function update(){
        $dev_id=$this->input->post('dev_id');
        $dev_nama=$this->input->post('dev_nama');
		$aset_nomor=$this->input->post('aset_nomor');
		$kategori_nama=$this->input->post('kategori_nama');
		$dev_merek=$this->input->post('dev_merek');
		$dev_model=$this->input->post('dev_model');
		$dev_tipe=$this->input->post('dev_tipe');
		$dev_serial=$this->input->post('dev_serial');
		$dev_ket=$this->input->post('dev_ket');
        $this->m_device->update($dev_id,$aset_nomor,$dev_nama,$kategori_nama,$dev_merek,$dev_model,$dev_tipe,$dev_serial,$dev_ket);
		echo $this->session->set_flashdata('msg','info');
        redirect('admin/device');
    }
	function hapus(){
		$stok_id=strip_tags($this->input->post('stok_id'));
		$this->m_stok->hapus($stok_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/stok');
	}
	

}