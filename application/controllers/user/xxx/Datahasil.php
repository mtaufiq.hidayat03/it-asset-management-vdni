<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DATAHASIL extends CI_Controller {

      function __construct(){

            parent::__construct();

				$this->load->model('m_alat');
				$this->load->model('m_status');
				$this->load->model('m_kategori');
				$this->load->model('m_lokasi');
				$this->load->model('m_divisi');
				$this->load->model('m_device');
				$this->load->model('m_user');                
				$this->load->library('upload');
      }

     

      public function index(){

            
			$x['hasil'] 		= $this->m_alat->tampil_alat();			
            $this->load->view('admin/v_datahasil',$x);

      }
	  
	  
		
		

}