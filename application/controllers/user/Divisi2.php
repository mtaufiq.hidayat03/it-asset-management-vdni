<?php
class Divisi2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_divisi');
		$this->load->model('m_lokasi');
        $this->load->library('upload');
	}


	function index(){
		redirect('page/divisi2');	
	    
	}
	function get_lokasi(){
        $id_lokasi=$this->input->post('id_lokasi');
        $data=$this->m_lokasi->get_lokasi_byid($id_lokasi);
        echo json_encode($data);
    }

	
	function simpan(){
        $divisi_id=$this->input->post('divisi_id');
        $divisi_nama=$this->input->post('divisi_nama');
        $lokasi_nama=$this->input->post('lokasi_nama');
        $this->m_divisi->simpan($divisi_id,$divisi_nama,$lokasi_nama);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan master divisi");
        redirect('user/divisi2');
    }
	function update(){
        $divisi_id=$this->input->post('divisi_id');
        $divisi_nama=$this->input->post('divisi_nama');
        $lokasi_nama=$this->input->post('lokasi_nama');
        $this->m_divisi->update($divisi_id,$divisi_nama,$lokasi_nama);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "mengapdate master divisi");
        redirect('user/divisi2');
    }

	function hapus_divisi(){
		$divisi_id=strip_tags($this->input->post('divisi_id'));
		$this->m_divisi->hapus_divisi($divisi_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus master divisi");
		redirect('user/divisi2');
	}
	

}