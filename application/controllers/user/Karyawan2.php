<?php
class Karyawan2 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_karyawan');
		$this->load->model('m_jabatan1');
        $this->load->library('upload');
	}
	function index(){
		redirect('page/karyawan2');	
	    
	}
	function get_jabatan(){
        $jabatan_id=$this->input->post('jabatan_id');
        $data=$this->m_jabatan1->get_jabatan_byid($jabatan_id);
        echo json_encode($data);
    }	
	function simpan(){
        $karyawan_id=$this->input->post('karyawan_id');
       $karyawan_nik=$this->input->post('karyawan_nik');
        $karyawan_nama=$this->input->post('karyawan_nama');
		$jabatan_nama=$this->input->post('jabatan_nama');
        $this->m_karyawan->simpan($karyawan_id,$karyawan_nik,$karyawan_nama,$jabatan_nama);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan user alat");
        redirect('user/karyawan2');
    }
	function update(){
        $karyawan_id=$this->input->post('karyawan_id');
        $karyawan_nik=$this->input->post('karyawan_nik');
		$karyawan_nama=$this->input->post('karyawan_nama');
        $jabatan_nama=$this->input->post('jabatan_nama');
        $this->m_karyawan->update($karyawan_id,$karyawan_nik,$karyawan_nama,$jabatan_nama);
		echo $this->session->set_flashdata('msg','info');
		helper_log("add", "mengupdate user alat");
        redirect('user/karyawan2');
    }
	function hapus(){
		$karyawan_id=strip_tags($this->input->post('karyawan_id'));
		$this->m_karyawan->hapus($karyawan_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus user alat");
		redirect('user/karyawan2');
	}
	function checknik(){
        $this->load->model('m_karyawan');
        $karyawan_nik = $this->input->post("karyawan_nik");
        $ceknik = $this->m_karyawan->Ceknik($karyawan_nik);
        if ($ceknik > 0){
          echo "ok";
            }
    }
	

}