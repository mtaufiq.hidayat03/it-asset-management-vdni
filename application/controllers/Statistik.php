<?php
class Statistik extends CI_Controller{
  function __construct(){
    parent::__construct();
        $this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_karyawan');
		$this->load->model('m_kategori');
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_assign');
		$this->load->model('m_form');
		$this->load->model('m_suplier');
        $this->load->library('upload');
  }
  
    function index(){
            $x['total_karyawan']= $this->m_karyawan->total_karyawan();
			$x['total_lokasi']	= $this->m_lokasi->total_lokasi();
			$x['total_user']	= $this->m_user->total_user();
			$x['total_divisi']	= $this->m_divisi->total_divisi();
			$x['total_alat']	= $this->m_assign->total_assign();
			$x['user']			= $this->m_user->show_user();
			$x['lokasi']		= $this->m_lokasi->show_lokasi();
			$x['log_data']		= $this->m_log->tampil_log();
			$x['data_alat']     = $this->m_assign->alat_terpakai();
			$x['jml_suplier']   = $this->m_suplier->jumlah_suplier();
			$x['alatlokasi1'] 	= $this->m_assign->alatlokasi();
			$x['alatstatus1'] 	= $this->m_assign->alatstatus();
			$x['alatlokasi2'] 	= $this->m_assign->get_alat_lokasi();
            $x['alatstatus2'] 	= $this->m_assign->get_alat_status();
            $x['alatkategori1'] = $this->m_assign->get_alat_kategori();
            $x['alatmerek1'] 	= $this->m_assign->alatmerek();
            $x['alatnama1'] 	= $this->m_assign->get_alat_nama();
            $x['alatmerek2'] 	= $this->m_assign->get_alat_merek();
            $x['form1'] 	    = $this->m_form->get_form_suplier();
            $this->load->view('admin/v_statistik',$x);
  }
	function getdata(){
        $data  = $this->m_grafik->getdata();
        print_r(json_encode($data, true));
    }
	function get_stok(){
		$data = $this->m_grafik->tampil_alat();
		header('Content-Type: application/json');
		echo $data;
	}
	function get_alat(){
		$data = $this->m_grafik->tampil_alat();
		print_r(json_encode($data, true));
	}
   function logout()
   {
       $this->session->sess_destroy();
		helper_log("hapus", "keluar dari sistem");
        redirect('login');
    }
   

}
