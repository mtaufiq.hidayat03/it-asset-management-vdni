<?php
class Jabatan1 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_jabatan1');
        $this->load->library('upload');
		
	}


	function index(){
		redirect('page/jabatan1');
	}

	function simpan_jabatan(){
		$jabatan=strip_tags($this->input->post('xjabatan'));
		$this->m_jabatan1->simpan_jabatan($jabatan);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan level jabatan");
		redirect('admin/jabatan1');
	}

	function update_jabatan(){
		$kode=strip_tags($this->input->post('kode'));
		$jabatan=strip_tags($this->input->post('xjabatan'));
		$this->m_jabatan1->update_jabatan($kode,$jabatan);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update jabatan");
		redirect('admin/jabatan1');
	}
	function hapus_jabatan(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_jabatan1->hapus_jabatan($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus jabatan");
		redirect('admin/jabatan1');
	}
	

}