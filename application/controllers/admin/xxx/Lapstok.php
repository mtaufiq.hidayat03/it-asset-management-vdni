<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lapstok extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->model('m_lapstok');
			$this->load->model('m_device');
			$this->load->model('m_kategori');
			$this->load->model('m_status');
			$this->load->model('m_stok');
			$this->load->model('m_grafik');
			$this->load->model('m_login');		
			$this->load->model('m_divisi');
			$this->load->model('m_jabatan');
			$this->load->model('m_lokasi');
			
			$this->load->model('m_alat');
			
			$this->load->model('m_log');

      }

     

      public function index(){

           
			$x['total_lokasi']	= $this->m_lokasi->total_lokasi();
			$x['total_divisi']	= $this->m_divisi->total_divisi();
			$x['jml']			= $this->m_stok->get_all_jumlah();
			
			$x['lokasi']		= $this->m_lokasi->show_lokasi();
			$x['alat'] 			= $this->m_alat->tampil_alat();
			$x['log_data']		= $this->m_log->tampil_log();
			$x['datagrafik']	= $this->m_grafik->tampil_alat();
			$x['data'] 			= $this->m_alat->tampil_alatbaru();
			$x['data1'] 		= $this->m_alat->tampil_alatlokasi();
			$x['data2'] 		= $this->m_alat->tampil_alatdivisi();
			$x['data3'] 		= $this->m_alat->tampil_alatnama();
            $this->load->view('admin/v_lapstok',$x);

      }

}