<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class File1 extends CI_Controller {

      function __construct(){

            parent::__construct();

			//	$this->load->model('m_alat');
			//	$this->load->model('m_status');
			//	$this->load->model('m_kategori');
			//	$this->load->model('m_lokasi');
			//	$this->load->model('m_divisi');
			//	$this->load->model('m_device');
			//	$this->load->model('m_user');                
			//	$this->load->library('upload');
				$this->load->helper('url');
                $this->load->model('m_file');
				
      }
      function index()
      {
            redirect('page/file1');
      }
      
      function insert(){
              //load session library to use flashdata
                  $this->load->library('session');
            
               //Check if file is not empty
                  if(!empty($_FILES['upload']['name'])){
                    $config['upload_path'] = 'upload/';
                        //restrict uploads to this mime types
                    $config['allowed_types'] = 'pdf';
                    $config['file_name'] = $_FILES['upload']['name'];
                    
                        //Load upload library and initialize configuration
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if($this->upload->do_upload('upload')){
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
            
                //set file data to insert to database
                        $file['description'] = $this->input->post('description');
                        $file['filename'] = $filename;
            
                        $query = $this->m_file->insertfile($file);
                        if($query){
                           header('location:'.base_url().$this->index());
                           $this->session->set_flashdata('success','File uploaded successfully');
                       }
                       else{
                           header('location:'.base_url().$this->index());
                           $this->session->set_flashdata('error','File uploaded but not inserted to database');
                       }
            
                   }else{
                     header('location:'.base_url().$this->index());
                     $this->session->set_flashdata('error','Cannot upload file.'); 
                     redirect(base_url('page/upload1'));
                 }
                         }else{
                              header('location:'.base_url().$this->index());
                                 $this->session->set_flashdata('error','Cannot upload empty file.');
                                 redirect(base_url('page/upload1'));
                    }
                    
                    
            
        }
    
    
    function download($id){
    $this->load->helper('download');
    $fileinfo = $this->files_model->download($id);
    $file = 'upload/'.$fileinfo['filename'];
    force_download($file, NULL);
    }
    
    
	  
		
		

}