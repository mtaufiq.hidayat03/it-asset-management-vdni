<?php 

class Adduser extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_user');
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
        $this->load->library('upload');
	}

	function index(){
        $x['user']=$this->m_user->show_user();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['data']=$this->m_jabatan->get_all_jabatan();
		$x['lokasi']= $this->m_lokasi  ->get_all_lokasi();
        $this->load->view('admin/v_adduser',$x);
    }
	function get_divisi(){
        $divisi_idid=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_jabatan(){
        $id_jabatan=$this->input->post('id_jabatan');
        $data=$this->m_jabatan->get_jabatan_byid($id_jabatan);
        echo json_encode($data);
    }
	function get_lokasi(){
        $lokasi_id=$this->input->post('lokasi_id');
        $data=$this->m_lokasi->get_lokasi_byid($lokasi_id);
        echo json_encode($data);
    }
	
	
	function simpan(){
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=MD5($this->input->post('password'));
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$lokasi_nama=$this->input->post('lokasi_nama');
							$role=$this->input->post('role');
							$foto=$this->input->post('foto');
							$status=$this->input->post('status');
							$this->m_user->simpan($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$lokasi_nama,$role,$foto,$status);
        echo $this->session->set_flashdata('msg','success');
		redirect('admin/user');
    }
	function simpan_user(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 840;
	                        $config['height']= 450;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=MD5($this->input->post('password'));
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$lokasi_kerja=$this->input->post('lokasi_kerja');
							$role=$this->input->post('role');
							$status=$this->input->post('status');
							$this->m_user->simpan_user($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$lokasi_kerja,$foto,$role,$status);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/user');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
	                    	redirect('admin/user');
	               			}
	                 
	            			}else{
							redirect('admin/user');
							}
				
	}
	
	function update(){
        
							$id_user=$this->input->post('id_user');
							$nama_user=$this->input->post('nama_user');
							$username=$this->input->post('username');
							$password=MD5($this->input->post('password'));
							$nik=$this->input->post('nik');
							$divisi_nama=$this->input->post('divisi_nama');
							$nama_jabatan=$this->input->post('nama_jabatan');
							$work_nama=$this->input->post('work_nama');
							$role=$this->input->post('role');
							$foto=$this->input->post('foto');
							$status=$this->input->post('status');
							$this->m_user->edit($id_user,$nama_user,$username,$password,$nik,$divisi_nama,$nama_jabatan,$work_nama,$role,$foto,$status);
        redirect('admin/user');
    }
	
	
	function hapus_user(){
        $id_user=$this->input->post('id_user');
        $this->m_user->hapus_user($id_user);
        redirect('admin/user');
    }

}