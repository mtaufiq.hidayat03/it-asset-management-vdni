<?php
class Jabatan extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_jabatan');
		$this->load->model('m_divisi');
        $this->load->library('upload');
	}


	function index(){
		$x['data']=$this->m_jabatan->get_all_jabatan();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$this->load->view('admin/v_jabatan',$x);
	}
	function get_divisi(){
        $divisi_idid=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function simpan(){
		$id_jabatan=$this->input->post('id_jabatan');
        $nama_jabatan=$this->input->post('nama_jabatan');
        $divisi_nama=$this->input->post('divisi_nama');
		$this->m_jabatan->simpan($id_jabatan,$nama_jabatan,$divisi_nama);
		echo $this->session->set_flashdata('msg','success');
        redirect('admin/jabatan');
	}

	function simpan_jabatan(){
		$jabatan=strip_tags($this->input->post('xjabatan'));
		$this->m_jabatan->simpan_jabatan($jabatan);
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/jabatan');
	}
	function update(){
        $id_jabatan=$this->input->post('id_jabatan');
        $nama_jabatan=$this->input->post('nama_jabatan');
        $divisi_nama=$this->input->post('divisi_nama');
        $this->m_jabatan->update($id_jabatan,$nama_jabatan,$divisi_nama);
		echo $this->session->set_flashdata('msg','info');
        redirect('admin/jabatan');
    }

	function update_jabatan(){
		$id_jabatan=strip_tags($this->input->post('id_jabatan'));
		$jabatan=strip_tags($this->input->post('xjabatan'));
		$this->m_jabatan->update_jabatan($id_jabatan,$jabatan);
		echo $this->session->set_flashdata('msg','info');
		redirect('admin/jabatan');
	}
	function hapus_jabatan(){
		$id_jabatan=strip_tags($this->input->post('id_jabatan'));
		$this->m_jabatan->hapus_jabatan($id_jabatan);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/jabatan');
	}
	

}