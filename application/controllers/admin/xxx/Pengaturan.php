<?php
class Device extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_device');
		$this->load->model('m_kategori');
        $this->load->library('upload');
	}


	function index(){
		$x['data']=$this->m_device->get_all_device();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$this->load->view('admin/v_device',$x);
	}

	function simpan(){
        $dev_id=$this->input->post('dev_id');
        $dev_nama=$this->input->post('dev_nama');
		$aset_nomor=$this->input->post('aset_nomor');
		$kategori_nama=$this->input->post('kategori_nama');
		$dev_merek=$this->input->post('dev_merek');
		$dev_model=$this->input->post('dev_model');
		$dev_tipe=$this->input->post('dev_tipe');
		$dev_serial=$this->input->post('dev_serial');
		$dev_ket=$this->input->post('dev_ket');
		
        
        $this->m_device->simpan($dev_id,$aset_nomor,$dev_nama,$kategori_nama,$dev_merek,$dev_model,$dev_tipe,$dev_serial,$dev_ket);
        echo $this->session->set_flashdata('msg','success');
		redirect('admin/device');
    }

	function update(){
        $dev_id=$this->input->post('dev_id');
        $dev_nama=$this->input->post('dev_nama');
		$aset_nomor=$this->input->post('aset_nomor');
		$kategori_nama=$this->input->post('kategori_nama');
		$dev_merek=$this->input->post('dev_merek');
		$dev_model=$this->input->post('dev_model');
		$dev_tipe=$this->input->post('dev_tipe');
		$dev_serial=$this->input->post('dev_serial');
		$dev_ket=$this->input->post('dev_ket');
        $this->m_device->update($dev_id,$aset_nomor,$dev_nama,$kategori_nama,$dev_merek,$dev_model,$dev_tipe,$dev_serial,$dev_ket);
		echo $this->session->set_flashdata('msg','info');
        redirect('admin/device');
    }
	function hapus(){
		$dev_id=strip_tags($this->input->post('dev_id'));
		$this->m_device->hapus($dev_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/device');
	}
	

}