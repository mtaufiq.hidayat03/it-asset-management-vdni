<?php
class  Upload1 extends CI_Controller{
  function __construct(){
    parent::__construct();
        $this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_karyawan');
		$this->load->model('m_kategori');
		$this->load->model('m_jabatan1');
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_assign');
		$this->load->model('m_merek');
		$this->load->model('m_ukuran');
		$this->load->model('m_suplier');
		$this->load->model('m_form');
		$this->load->model('m_upload');
		//$this->load->library('upload');
		
		$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');
   
  }
 
  function index(){
           
          redirect('page/upload1');
  }
  
 
 function upload()
     {
          $config['upload_path'] = './upload';
          $config['allowed_types'] = 'pdf|doc';
          $config['max_size'] = 3000;
          $this->load->library('upload', $config);
          if($this->upload->do_upload('file')) {
               $fileData = $this->upload->data();
               $upload = [
                    'nama_file' => $fileData['file_name'],
                    'tipe_file' => $fileData['file_type'],
                    'ukuran_file' => $fileData['file_size'],
               ];
               if($this->m_upload->insert($upload)) {
                    $this->session->set_flashdata('success', '<p>File Berhasil diUnggah <strong>'. $fileData['file_name'] .'</strong></p>');
               } else {
                    $this->session->set_flashdata('error', '<p>Gagal! File '. $fileData['file_name'] .' tidak berhasil tersimpan di database,silahkan ulangi lagi</p>');
               }
               redirect(base_url('page/upload1'));
          } else {
               $this->session->set_flashdata('error', $this->upload->display_errors());
               redirect(base_url('page/upload1'));
          }
     }
}