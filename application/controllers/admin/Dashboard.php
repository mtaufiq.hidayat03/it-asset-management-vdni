<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
		$this->load->model('m_login');		
		$this->load->model('m_divisi');
		$this->load->model('m_jabatan');
		$this->load->model('m_lokasi');
		$this->load->model('m_status');		
		$this->load->model('m_karyawan');
		$this->load->model('m_kategori');
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_assign');
		$this->load->model('m_profil');
        if($this->session->userdata('masuk') != TRUE){
            $url=base_url();
            redirect($url);
        }
    }
    function index(){
			redirect('page/index');
	}
	function getdata(){
        $data  = $this->m_grafik->getdata();
        print_r(json_encode($data, true));
    }
	function get_stok(){
		$data = $this->m_grafik->tampil_alat();
		header('Content-Type: application/json');
		echo $data;
	}
	function get_alat(){
		$data = $this->m_grafik->tampil_alat();
		print_r(json_encode($data, true));
	}
   function logout()
   {
       $this->session->sess_destroy();
		helper_log("hapus", "keluar dari sistem");
        redirect('login');
    }
   

}
