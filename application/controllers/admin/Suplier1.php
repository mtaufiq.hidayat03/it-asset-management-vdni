<?php
class Suplier1 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_suplier');
        $this->load->library('upload');
		
	}


	function index(){
	redirect('page/suplier1');
	}
	
	function simpan(){
        $suplier_id=$this->input->post('suplier_id');
        $suplier_nama=$this->input->post('suplier_nama');
		$alamat_nama=$this->input->post('alamat_nama');
        $this->m_suplier->simpan($suplier_id,$suplier_nama,$alamat_nama);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan suplier");
        redirect('admin/suplier1');
    }
	function hapus(){
		$suplier_id=strip_tags($this->input->post('suplier_id'));
		$this->m_suplier->hapus($suplier_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "menghapus data suplier");
		redirect('admin/suplier1');
	}
	

}