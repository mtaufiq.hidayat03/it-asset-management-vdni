<?php
class Merek1 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_merek');
        $this->load->library('upload');
		
	}
	
	function index(){
		redirect('page/merek1');
	}

	function simpan_merek(){
		$merek=strip_tags($this->input->post('xmerek'));
		$this->m_merek->simpan_merek($merek);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan merek");
		redirect('admin/merek1');
	}

	function update_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$merek=strip_tags($this->input->post('xmerek'));
		$this->m_merek->update_merek($kode,$merek);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update merek");
		redirect('admin/merek1');
	}
	function hapus_merek(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_merek->hapus_merek($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus merek");
		redirect('admin/merek1');
	}
	

}