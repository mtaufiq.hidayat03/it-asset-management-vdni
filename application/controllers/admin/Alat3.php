<?php
class Alat3 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_alat');
        $this->load->library('upload');
		
	}


	function index(){
		redirect('page/alat3');
	}

	function simpan_alat(){
		$alat=strip_tags($this->input->post('xalat'));
		$this->m_alat->simpan_alat($alat);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan Jenis Alat");
		redirect('page/alat3');
	}

	function update_alat(){
		$kode=strip_tags($this->input->post('kode'));
		$alat=strip_tags($this->input->post('xalat'));
		$this->m_alat->update_alat($kode,$alat);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "update jenis alat");
		redirect('page/alat3');
	}
	function hapus_alat(){
		$kode=strip_tags($this->input->post('kode'));
		$this->m_alat->hapus_alat($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "hapus jenis alat");
		redirect('page/alat3');
	}
	

}