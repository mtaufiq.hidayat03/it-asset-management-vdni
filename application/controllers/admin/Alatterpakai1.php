<?php
class Alatterpakai1 extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_assign');
		$this->load->model('m_divisi');
		$this->load->model('m_lokasi');
		$this->load->model('m_karyawan');
		$this->load->model('m_status');
		$this->load->model('m_kategori');
		$this->load->model('m_ukuran');
        $this->load->library('upload');
		
	}

	function index(){
	redirect('page/alatterpakai1');
	}
	function get_divisi(){
        $divisi_id=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_kategori(){
        $kategori_id=$this->input->post('kategori_id');
        $data=$this->m_kategori->get_kategori_byid($kategori_id);
        echo json_encode($data);
    }
	function get_lokasi(){
        $lokasi_id=$this->input->post('lokasi_id');
        $data=$this->m_lokasi->get_lokasi_byid($lokasi_id);
        echo json_encode($data);
    }
	function get_karyawan(){
        $karyawan_id=$this->input->post('karyawan_id');
        $data=$this->m_karyawan->get_karyawan_byid($karyawan_id);
        echo json_encode($data);
    }
	function get_status(){
        $status_id=$this->input->post('status_id');
        $data=$this->m_status->get_status_byid($status_id);
        echo json_encode($data);
    }
	
	function get_ukuran(){
        $ukuran_id=$this->input->post('ukuran_id');
        $data=$this->m_ukuran->get_ukuran_byid($ukuran_id);
       echo json_encode($data);
    }
	function detail($id)
    {
        $data['detail'] = $this->m_assign->get_assign($id);
		$this->load->view('admin/v_5detail', $data);
    }
	function cetak($id)
    {
        $data['detail'] = $this->m_assign->get_detail($id);
		$this->load->view('admin/v_5cetak', $data);
    }
	
	
	function simpan(){
        $assign_id=$this->input->post('assign_id');
        $alat_nama=$this->input->post('alat_nama');
		$kategori_nama=$this->input->post('kategori_nama');
		$merek=$this->input->post('merek');
		$model=$this->input->post('model');
		$serial=$this->input->post('serial');
        $karyawan_nama=$this->input->post('karyawan_nama');
		$lokasi_nama=$this->input->post('lokasi_nama');
		$divisi_nama=$this->input->post('divisi_nama');
		$deskripsi=$this->input->post('deskripsi');
		$jumlah=$this->input->post('jumlah');
		$ukuran=$this->input->post('ukuran');
		$status_nama=$this->input->post('status_nama');
        $this->m_assign->simpan2($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama);
		echo $this->session->set_flashdata('msg','success');
		helper_log("add", "menambahkan data");
        redirect('admin/alatterpakai1');
    }
	
	function assign_user(){
		$assign_id=$this->input->post('assign_id');
        $alat_nama=$this->input->post('alat_nama');
		$kategori_nama=$this->input->post('kategori_nama');
		$merek=$this->input->post('merek');
		$model=$this->input->post('model');
		$serial=$this->input->post('serial');
        $karyawan_nama=$this->input->post('karyawan_nama');
		$lokasi_nama=$this->input->post('lokasi_nama');
		$divisi_nama=$this->input->post('divisi_nama');
		$deskripsi=$this->input->post('deskripsi');
		$jumlah=$this->input->post('jumlah');
		$ukuran=$this->input->post('ukuran');
		$status_nama=$this->input->post('status_nama');
        $this->m_assign->update($assign_id,$alat_nama,$kategori_nama,$merek,$model,$serial,$karyawan_nama,$lokasi_nama,$divisi_nama,$deskripsi,$jumlah,$ukuran,$status_nama);
		echo $this->session->set_flashdata('msg','info');
		helper_log("edit", "mengapdate data");
        redirect('admin/alatterpakai1');
	}
	function hapus_assign(){
		$assign_id=strip_tags($this->input->post('assign_id'));
		$this->m_assign->hapus_assign($assign_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		helper_log("hapus", "meghapus data");
		redirect('admin/alatterpakai1');
	}
	

}