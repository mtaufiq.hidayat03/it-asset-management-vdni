<?php
class Profil extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('m_profil');
        $this->load->library('upload');
	}


	function index(){
		redirect('page/profil');
	}

	function simpan_profil(){
				$config['upload_path'] = './assets/images/profil'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./assets/images/profil'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '100%';
	                        $config['width']= 500;
	                        $config['height']= 870;
	                        $config['new_image']= './assets/images/profil'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $foto=$gbr['file_name'];
							$profil_id=$this->input->post('profil_id');
							$profil_nama=$this->input->post('profil_nama');
							$aplikasi_nama=$this->input->post('aplikasi_nama');
							$alamat_nama=$this->input->post('alamat_nama');
							$nomor_telepon=$this->input->post('nomor_telepon');
							$nomor_fax=$this->input->post('nomor_fax');

							$this->m_profil->simpan_profil($foto,$profil_id,$profil_nama,$aplikasi_nama,$alamat_nama,$nomor_telepon,$nomor_fax);
							echo $this->session->set_flashdata('msg','success');
							helper_log("add", "menambahkan pengaturan sistem");
							redirect('page/profil');
							}else{
	                    	echo $this->session->set_flashdata('msg','warning');
	                    	redirect('page/profil');
	               			}
	            			}else{
							redirect('page/profil');
							}
				
	}
	function hapus(){
		$dev_id=strip_tags($this->input->post('dev_id'));
		$this->m_device->hapus($dev_id);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/device');
	}
	

}