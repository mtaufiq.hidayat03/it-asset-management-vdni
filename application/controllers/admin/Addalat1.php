<?php 

class Addalat1 extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_assign');
		$this->load->model('m_divisi');
		$this->load->model('m_lokasi');
		$this->load->model('m_karyawan');
		$this->load->model('m_status');
		$this->load->model('m_kategori');
		$this->load->model('m_ukuran');
		$this->load->model('m_merek');
        $this->load->library('upload');
	}

	function index(){
	    redirect('page/addalat1');
		$x['merek1']=$this->m_merek->get_all_merek();
		$x['data']=$this->m_assign->alat_baru();
		$x['divisi']=$this->m_divisi->get_all_divisi();
		$x['lokasi']=$this->m_lokasi->get_all_lokasi();
		$x['karyawan']=$this->m_karyawan->get_all_karyawan();
		$x['status']=$this->m_status->get_all_status();
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['ukuran1']= $this->m_ukuran  ->get_all_ukuran();
		$this->load->view('admin/v_addalat',$x);
	}
	function get_divisi(){
        $divisi_id=$this->input->post('divisi_id');
        $data=$this->m_divisi->get_divisi_byid($divisi_id);
        echo json_encode($data);
    }
	function get_kategori(){
        $kategori_id=$this->input->post('kategori_id');
        $data=$this->m_kategori->get_kategori_byid($kategori_id);
        echo json_encode($data);
    }
	function get_lokasi(){
        $lokasi_id=$this->input->post('lokasi_id');
        $data=$this->m_lokasi->get_lokasi_byid($lokasi_id);
        echo json_encode($data);
    }
	function get_karyawan(){
        $karyawan_id=$this->input->post('karyawan_id');
        $data=$this->m_karyawan->get_karyawan_byid($karyawan_id);
        echo json_encode($data);
    }
	function get_status(){
        $status_id=$this->input->post('status_id');
        $data=$this->m_status->get_status_byid($status_id);
        echo json_encode($data);
    }
	
	function get_ukuran(){
        $ukuran_id=$this->input->post('ukuran_id');
        $data=$this->m_ukuran->get_ukuran_byid($ukuran_id);
       echo json_encode($data);
    }
    function get_merek(){
        $merek_id=$this->input->post('merek_id');
        $data=$this->m_merek->get_merek_byid($merek_id);
       echo json_encode($data);
    }


}