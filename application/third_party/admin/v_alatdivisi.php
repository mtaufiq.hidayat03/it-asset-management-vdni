<!DOCTYPE html>
<html lang="en">

<head>
    <title>IMS VDNI-ALAT DIVISI </title>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
   
    <link rel="icon" href="<?php echo base_url().'assets\files\assets\images\favicon.ico" type="image/x-icon'?>">   
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet'?>">    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap\css\bootstrap.min.css'?>">      
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\jquery.mCustomScrollbar.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\themify-icons\themify-icons.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\icofont\css\icofont.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\feather\css\feather.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\pages\data-table\css\buttons.dataTables.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\lightbox2\css\lightbox.min.css'?>">
	<link rel="stylesheet" href="<?php echo base_url().'assets\files\assets\pages\chart\radial\css\radial.css" type="text/css" media="all'?>">    
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\pages\list-scroll\list.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\stroll\css\stroll.css'?>">
</head>
<!-- Menu sidebar static layout -->

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header ">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="<?php echo base_url().'admin/dashboard'?>">
						<div class="pcoded-navigatio-lavel">IMS VDNI V.2</div>
                            <!--<img class="img-fluid" src="<?php echo base_url().'assets\files\assets\images\vdni.jpeg" alt="Theme-Logo'?>">-->
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <!--<li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-pink">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-4.jpg" alt="Generic placeholder image'?>">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-3.jpg" alt="Generic placeholder image'?>">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-4.jpg" alt="Generic placeholder image'?>">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>-->
                            <!--<li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li>-->
							
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <a href="#">
                                                <i class="feather icon-settings"></i> <span class="hidden-xs"><?php echo $this->session->userdata("user_name") ?></span>
                                            </a>
                                        
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        
                                        <li>
                                            <a href="#">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <!-- Sidebar chat start -->
           <!-- <div id="sidebar" class="users p-chat-user showChat">
                <div class="had-container">
                    <div class="card card_main p-fixed users-main">
                        <div class="user-box">
                            <div class="chat-inner-header">
                                <div class="back_chatBox">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-friend-list">
                                <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image ">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Josephin Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alice</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alia</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Suzen</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            
            <!--<div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                        <i class="feather icon-chevron-left"></i> Josephin Doe
                    </a>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                    </a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media-right photo-table">
                        <a href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                        </a>
                    </div>
                </div>
                <div class="chat-reply-box p-b-20">
                    <div class="right-icon-control">
                        <input type="text" class="form-control search-text" placeholder="cari data">
                        <div class="form-icon">
                            <i class="feather icon-navigation"></i>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel"></div>
                            <ul class="pcoded-item pcoded-left-item">
                                                             
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext">MASTER </span>
                                        <span class="pcoded-badge label label-danger">M</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/kategori'?>">
                                                <span class="pcoded-mtext">KATEGORI ALAT</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/device'?>">
                                                <span class="pcoded-mtext">ALAT</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/status'?>">
                                                <span class="pcoded-mtext">STATUS ALAT</span>
                                            </a>
                                        </li>
                                        
										<li class="">
                                            <a href="<?php echo base_url().'admin/lokasi'?>">
                                                <span class="pcoded-mtext">LOKASI KERJA</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/divisi'?>">
                                                <span class="pcoded-mtext">DIVISI</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/jabatan'?>">
                                                <span class="pcoded-mtext">JABATAN</span>
                                            </a>
                                        </li>
										

                                    </ul>
                                </li>
                            </ul>
                            
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                                        <span class="pcoded-mtext">ALAT/TOOLS</span>
										<span class="pcoded-badge label label-danger">A</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alat'?>">
                                                <span class="pcoded-mtext">ASSIGN DEVICE</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/stok'?>">
                                                <span class="pcoded-mtext">STOK</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/rusak'?>">
                                                <span class="pcoded-mtext">RUSAK</span>
                                            </a>
                                        </li>
										
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                                        <span class="pcoded-mtext">KARYAWAN</span>
										<span class="pcoded-badge label label-danger">K</span>
                                    </a>
                                    <ul class="pcoded-submenu">                                        
                                        
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/adduser'?>">
                                                <span class="pcoded-mtext">ENTRY DATA</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/user'?>">
                                                <span class="pcoded-mtext">LIST USER</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">LAPORAN</span>
										<span class="pcoded-badge label label-danger">L</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatlokasi'?>">
                                                <span class="pcoded-mtext">ALAT PER LOKASI</span>
                                            </a>
                                        </li>
                                       
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatdivisi'?>">
                                                <span class="pcoded-mtext">ALAT PER DIVISI</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/lapstok'?>">
                                                <span class="pcoded-mtext">LAPORAN STOK</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/alatuser'?>">
                                                <span class="pcoded-mtext">ALAT PER USER</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
								<li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">SISTEM LOG</span>
										<span class="pcoded-badge label label-danger">S</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/aktivitas'?>">
                                                <span class="pcoded-mtext">USER LOG</span>
                                            </a>
                                        </li>
                                        
                                        
										

                                    </ul>
                                </li>
                               
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                                        <span class="pcoded-mtext">PENCARIAN DATA</span>
										<span class="pcoded-badge label label-danger">P</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/data'?>">
                                                <span class="pcoded-mtext">FILTER DATA</span>
                                            </a>
                                        </li>
                                        
                                        
                                        
                                        
                                    </ul>
                                </li>
								<li class=" ">
                                    <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                        <span class="pcoded-micon"><i class="feather icon-cpu"></i></span>
                                        <span class="pcoded-mtext">KELUAR</span>
                                       
                                    </a>
                                </li>
                            </ul>                           
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
											
											
											
											
											
											
											
											
											
											
											<?php 
												foreach($data2->result_array() as $row):
												$divisi_nama = $row['divisi_nama'];
												$total = $row['total'];
												?>
                                            
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card statustic-progress-card">
                                                    <div class="card-header">
                                                       Data Update <h5><?php 
															date_default_timezone_set('Asia/Jakarta');
															echo " ".date(' d-M-Y / H:i:s a');?></h5>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <label class="label label-success">
                                                                    
															<?php echo htmlentities($divisi_nama, ENT_QUOTES, 'UTF-8');?>
                                                                </label>
                                                            </div>
                                                            <div class="col text-right">
                                                                <h5 class=""><?php echo htmlentities($total , ENT_QUOTES, 'UTF-8');?></h5>
                                                            </div>
                                                        </div>
                                                       <div class="progress m-t-15">
                                                            <div class="progress-bar bg-c-pink" style="width:100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<?php endforeach; ?>
											
											<!--<div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-yellow update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white"><?php echo $total_user;?></h4>
                                                                <h6 class="text-white m-b-0">TOTAL USER</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-1" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>
															<?php 
															date_default_timezone_set('Asia/Jakarta');
															echo " ".date(' d-M-Y / H:i:s a');?>
														</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-green update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white"><?php echo $total_lokasi;?></h4>
                                                                <h6 class="text-white m-b-0">LOKASI KERJA</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-2" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>
															<?php 
															date_default_timezone_set('Asia/Jakarta');
															echo " ".date(' d-M-Y / H:i:s a');?>
														</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-pink update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white"><?php echo $total_divisi;?></h4>
                                                                <h6 class="text-white m-b-0">DIVISI</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-3" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>
															<?php 
															date_default_timezone_set('Asia/Jakarta');
															echo " ".date(' d-M-Y / H:i:s a');?>
														</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-lite-green update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white"><?php echo $jml;?></h4>
                                                                <h6 class="text-white m-b-0">TOTAL STOK</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-4" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>
															<?php 
															date_default_timezone_set('Asia/Jakarta');
															echo " ".date(' d-M-Y / H:i:s a');?>
														</p>
                                                    </div>
                                                </div>
                                            </div>-->
                                            
											
                                           
                                            <!--<div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <div class="card-header-left ">
                                                            <h5>Monthly View</h5>
                                                            <span class="text-muted">For more details about usage, please refer <a href="https://www.amcharts.com/online-store/" target="_blank">amCharts</a> licences.</span>
                                                        </div>
                                                    </div>
                                                    <div class="card-block-big">
                                                        <div id="monthly-graph" style="height:250px"></div>
                                                    </div>
                                                </div>
                                            </div>-->
											<!--<div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Grafik Statistik Alat</h5>
                                                        <span>Lokasi Keja & Divisi</span>

                                                    </div>
                                                    <div class="card-block">
                                                        <div id="grafik1" style="width: 100%; height: 350px;"></div>
                                                    </div>
                                                </div>
                                            </div>-->
                                            

                                            
                                            <div class="col-xl-12 col-md-18">
                                                <div class="card table-card">
                                                    <div class="card-header">
                                                        <h5>Data Alat Divisi</h5>
                                                        
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table  id="simpletable" class="table table-striped table-bordered nowrap">
                                                                <thead>
																<tr>		
																		<th style="width:50px;">Gambar</th>
																		<th style="width:50px;">Nama Alat</th>					
																		<!--<th style="width:50px;">Kategori</th>-->
																		<th style="width:50px;">Karyawan Pemegang</th>
																		<th style="width:50px;">Divisi</th>
																		<!--<th style="width:50px;">Lokasi Kerja</th>--> 
																		<th style="width:50px;">Status Alat</th> 												
																		
																</tr>
															</thead>
                                                                <tbody>
																			<?php
																			$no=0;
																			foreach ($alat->result_array() as $i) :
																			   $no++;
																			   $alat_id=$i['alat_id'];
																			   $foto=$i['foto'];
																			   $nama_alat=$i['nama_alat'];
																			   $kategori_nama=$i['kategori_nama'];
																			   $nama_user=$i['nama_user'];
																			   $divisi_nama=$i['divisi_nama'];
																			   $lokasi_nama=$i['lokasi_nama'];
																			   $status_alat=$i['status_alat'];                       
																			   $deskripsi=$i['deskripsi'];
																			?>
                                                                    <tr>
					
					
																		<td><div class="col-lg-2 col-sm-6">
																		<div class="thumbnail">
																		<div class="thumb">
																				<a href="<?php echo base_url().'assets/images/'.$foto;?>" data-lightbox="2" data-title="<?php echo $nama_alat ?> - <?php echo $kategori_nama ?>- <?php echo $divisi_nama ?>">
																					<img width="20" height="20" class="img-circle" src="<?php echo base_url().'assets/images/'.$foto;?>" alt="" class="img-fluid ">
																				</a>
																			</div>
																			</div>
																			</div></td>
																		<td><?php echo $nama_alat ?></td>
																		<!--<td><?php echo $kategori_nama ?></td>-->
																		<td><a class="btn" href="<?php echo base_url().'admin/userprofil'?>"><?php echo $nama_user ?></td>
																		<td><?php echo $divisi_nama ?></td>
																		<!--<td><?php echo $lokasi_nama ?></td>-->					
																		
																		<td>
																		<?php if($status_alat=='LOST'):?><span class="label label-danger">HILANG</span>
																		<?php else:?><span class="label label-success"><?php echo $status_alat ?></span>
																		<?php endif;?>
																			</td>
																		
																	  <?php endforeach;?>
																	</tr>
                                                                    
                                                                </tbody>
                                                            </table>
                                                            <div class="text-right m-r-20">
                                                                <a href="<?php echo base_url().'admin/lapstok'?>" class=" b-b-primary text-primary"  target="_blank">Lihat Data</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>											
											
											
                                            

                                            
                                           
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>










  
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery\js\jquery.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-ui\js\jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\popper.js\js\popper.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap\js\bootstrap.min.js'?>"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js'?>"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\modernizr.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\css-scrollbars.js'?>"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\chart.js\js\Chart.js'?>"></script>
    <!-- Google map js -->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\google-maps\gmaps.js'?>"></script>
    <!-- gauge js -->
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\gauge\gauge.min.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\amchart\amcharts.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\amchart\serial.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\amchart\gauge.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\amchart\pie.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\pages\widget\amchart\light.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\widget\custom-widget1.js'?>"></script>
	<!-- Custom js -->
    <script src="<?php echo base_url().'assets\files\assets\js\pcoded.min.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\js\vartical-layout.min.js'?>"></script>
    <script src="<?php echo base_url().'assets\files\assets\js\jquery.mCustomScrollbar.concat.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\dashboard\crm-dashboard.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets\files\assets\js\script.js'?>"></script>
	<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\data-table-custom.js'?>"></script>
			
			
			<script src="<?php echo base_url().'assets\files\bower_components\datatables.net\js\jquery.dataTables.min.js'?>"></script>

			<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next\js\i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-i18next\js\jquery-i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\lightbox2\js\lightbox.min.js'?>"></script>
			
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>			
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\chart\google\js\custom-google-chart.js'?>"></script>			

<script src="<?php echo base_url().'assets\files\bower_components\stroll\js\stroll.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\list-scroll\list-custom.js'?>"></script>


			
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net\js\jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\jszip.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\pdfmake.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\vfs_fonts.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.print.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.html5.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js'?>"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>



<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Lokasi', 'Jakarta', 'Morosi', 'Baruga', 'Jetty', 'Average'],
          ['Jakarta',  100,      9,         5,             9,                 614.6],
          ['Morosi',  100,      1,        5,             1,                682],
          ['Baruga',  100,      1,        5,             8,                 623],
          ['Jetty',  100,      1,        6,             9,                 609.4]
          
        ]);

        var options = {
          title : 'Statistik Alat -Lokasi Kerja',
          vAxis: {title: 'Jumlah'},
          hAxis: {title: 'Lokasi Keja'},
          seriesType: 'bars',
          series: {4: {type: 'line'}},
		  colors: ['#93BE52', '#69CEC6', '#FE8A7D', '#4680ff', '#FFB64D', '#FC6180']
		  };

        var chart = new google.visualization.ComboChart(document.getElementById('grafik1'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	




</body>

</html>

