<!DOCTYPE html>
<html lang="en">

<head>
    <title>IMS-TAMBAH USER </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url().'assets\files\assets\images\favicon.ico" type="image/x-icon'?>">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap\css\bootstrap.min.css'?>">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\themify-icons\themify-icons.css'?>">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\icofont\css\icofont.css'?>">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\feather\css\feather.css'?>">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\style.css'?>">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\jquery.mCustomScrollbar.css'?>">

</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">

               <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="<?php echo base_url().'admin/dashboard'?>">
						<div class="pcoded-navigatio-lavel">IMS VDNI V.2</div>
                            <!--<img class="img-fluid" src="<?php echo base_url().'assets\files\assets\images\vdni.jpeg" alt="Theme-Logo'?>">-->
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-bell"></i>
                                    <span class="badge bg-c-pink">5</span>
                                </div>
                                <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <h6>Notifications</h6>
                                        <label class="label label-danger">New</label>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-4.jpg" alt="Generic placeholder image'?>">
                                            <div class="media-body">
                                                <h5 class="notification-user">John Doe</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-3.jpg" alt="Generic placeholder image'?>">
                                            <div class="media-body">
                                                <h5 class="notification-user">Joseph William</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="<?php echo base_url().'assets\files\assets\images\avatar-4.jpg" alt="Generic placeholder image'?>">
                                            <div class="media-body">
                                                <h5 class="notification-user">Sara Soudein</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-message-square"></i>
                                    <span class="badge bg-c-green">3</span>
                                </div>
                            </div>
                        </li>
                        <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <a href="#">
                                                <i class="feather icon-settings"></i> <span class="hidden-xs"><?php echo $this->session->userdata("user_name") ?></span>
                                            </a>
                                        
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        
                                        <li>
                                            <a href="#">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Sidebar chat start -->
        <div id="sidebar" class="users p-chat-user showChat">
            <div class="had-container">
                <div class="card card_main p-fixed users-main">
                    <div class="user-box">
                        <div class="chat-inner-header">
                            <div class="back_chatBox">
                                <div class="right-icon-control">
                                    <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                    <div class="form-icon">
                                        <i class="icofont icofont-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-friend-list">
                            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image ">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Josephin Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Lary Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alice</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alia</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Suzen</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat start-->
        <div class="showChat_inner">
            <div class="media chat-inner-header">
                <a class="back_chatBox">
                    <i class="feather icon-chevron-left"></i> Josephin Doe
                </a>
            </div>
            <div class="media chat-messages">
                <a class="media-left photo-table" href="#!">
                    <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                </a>
                <div class="media-body chat-menu-content">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
            <div class="media chat-messages">
                <div class="media-body chat-menu-reply">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media-right photo-table">
                    <a href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                    </a>
                </div>
            </div>
            <div class="chat-reply-box p-b-20">
                <div class="right-icon-control">
                    <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                    <div class="form-icon">
                        <i class="feather icon-navigation"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                 <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel"></div>
                            <ul class="pcoded-item pcoded-left-item">
                                                             
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext">MASTER </span>
                                        <span class="pcoded-badge label label-danger">M</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/kategori'?>">
                                                <span class="pcoded-mtext">KATEGORI ALAT</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/device'?>">
                                                <span class="pcoded-mtext">ALAT</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/status'?>">
                                                <span class="pcoded-mtext">STATUS ALAT</span>
                                            </a>
                                        </li>
                                        
										<li class="">
                                            <a href="<?php echo base_url().'admin/lokasi'?>">
                                                <span class="pcoded-mtext">LOKASI KERJA</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/divisi'?>">
                                                <span class="pcoded-mtext">DIVISI</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/jabatan'?>">
                                                <span class="pcoded-mtext">JABATAN</span>
                                            </a>
                                        </li>
										

                                    </ul>
                                </li>
                            </ul>
                            
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                                        <span class="pcoded-mtext">ALAT/TOOLS</span>
										<span class="pcoded-badge label label-danger">A</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alat'?>">
                                                <span class="pcoded-mtext">ASSIGN DEVICE</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/stok'?>">
                                                <span class="pcoded-mtext">STOK</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/rusak'?>">
                                                <span class="pcoded-mtext">RUSAK</span>
                                            </a>
                                        </li>
										
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                                        <span class="pcoded-mtext">KARYAWAN</span>
										<span class="pcoded-badge label label-danger">K</span>
                                    </a>
                                    <ul class="pcoded-submenu">                                        
                                        
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/adduser'?>">
                                                <span class="pcoded-mtext">ENTRY DATA</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/user'?>">
                                                <span class="pcoded-mtext">LIST USER</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">LAPORAN</span>
										<span class="pcoded-badge label label-danger">L</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatlokasi'?>">
                                                <span class="pcoded-mtext">ALAT PER LOKASI</span>
                                            </a>
                                        </li>
                                       
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatdivisi'?>">
                                                <span class="pcoded-mtext">ALAT PER DIVISI</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/lapstok'?>">
                                                <span class="pcoded-mtext">LAPORAN STOK</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/alatuser'?>">
                                                <span class="pcoded-mtext">ALAT PER USER</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
								<li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">SISTEM LOG</span>
										<span class="pcoded-badge label label-danger">S</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/aktivitas'?>">
                                                <span class="pcoded-mtext">USER LOG</span>
                                            </a>
                                        </li>
                                        
                                        
										

                                    </ul>
                                </li>
                               
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                                        <span class="pcoded-mtext">PENCARIAN DATA</span>
										<span class="pcoded-badge label label-danger">P</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/data'?>">
                                                <span class="pcoded-mtext">FILTER DATA</span>
                                            </a>
                                        </li>
                                        
                                        
                                        
                                        
                                    </ul>
                                </li>
								<li class=" ">
                                    <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                        <span class="pcoded-micon"><i class="feather icon-cpu"></i></span>
                                        <span class="pcoded-mtext">KELUAR</span>
                                       
                                    </a>
                                </li>
                            </ul>                           
                        </div>
                    </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-8">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Personal Data User</h4>
                                                    <span><code></code></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-lg-4">
                                            <div class="page-header-breadcrumb">
                                                <ul class="breadcrumb-title">
                                                    <li class="breadcrumb-item">
                                                        <a href="#"> <i class="feather icon-home"></i> </a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">Form User</a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">Tambah</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                <!-- Page-header end -->

                                <!-- Page body start -->
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- Basic Form Inputs card start -->
                                            <div class="card">
                                                
                                                <div class="card-block">
                                                   
                                                     <form class="form-horizontal" action="<?php echo base_url().'admin/adduser/simpan_user'?>" method="post" enctype="multipart/form-data">
                                                                                                         
       													<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Nama Karyawan</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="nama_user" class="form-control form-control-round" id="nama_user" placeholder="nama karyawan">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Employee Number</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="nik" class="form-control form-control-round" id="nik" placeholder="nik">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Username</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="username" class="form-control form-control-round" id="username" placeholder="username">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Password</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="password" class="form-control form-control-round" id="password" placeholder="password">
                                                            </div>
                                                        </div>
														

                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Divisi</label>
                                                            <div class="col-sm-8">
                                                                <select name="divisi_nama" id="divisi_nama" class="form-control form-control-round">
                                                                   <option value="0">Divisi</option>
                                                                    <?php foreach($divisi->result() as $row):?>
																   <option value="<?php echo $row->divisi_nama;?>"><?php echo $row->divisi_nama;?>[<?php echo $row->lokasi_nama;?>]</option>
																	<?php endforeach;?>
                                                                </select>
                                                            </div>
                                                        </div>
														<div class="form-group row">
															<label class="col-sm-3 col-form-label">Level Jabatan</label>
															<div class="col-sm-8">
																<select name="nama_jabatan" id="nama_jabatan" class="form-control form-control-round">
																	<option value="0">Level Jabatan</option>
																	<?php foreach($data->result() as $row):?>
																		<option value="<?php echo $row->nama_jabatan;?>"><?php echo $row->nama_jabatan;?>[<?php echo $row->divisi_nama;?>]</option>
																	<?php endforeach;?>
																</select>
															</div>
													    </div>
														<div class="form-group row">
															<label class="col-sm-3 col-form-label">Lokasi Kerja</label>
															<div class="col-sm-8">
																<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-round">
																	<option value="0">Lokasi Kerja</option>
																	<?php foreach($lokasi->result() as $row):?>
																		<option value="<?php echo $row->lokasi_nama;?>"><?php echo $row->lokasi_nama;?></option>
																	<?php endforeach;?>
																</select>
															</div>
													    </div>
														
														
														
														
														<div class="form-group row">
														<label for="inputUserName" class="col-sm-3 control-label">Level</label>
														<div class="col-sm-8">
															<select class="form-control form-control-round" name="role" required>
																<option value="1">Admin</option>
																<option value="2">Operator</option>
															</select>
														</div>
														</div>
														
														<div class="form-group row">
														<label for="status" class="col-sm-3 control-label">Status</label>
														<div class="col-sm-8">
															<select class="form-control form-control-round" name="status" id="status" required>
																<option value="1">Aktif</option>
																<option value="2">Non Aktif</option>
															</select>
														</div>
														</div>
                                                       <div class="form-group row">
														<label for="inputUserName" class="col-sm-3 control-label">Photo * Wajib disertakan</label>
														<div class="col-sm-8">
															<input type="file" name="filefoto" required/>
														</div>
													    </div>
                                                        
                                                        
                                                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                                                    
													</form>
                                                    
                                                    
                                                </div>
                                            </div>
                                            
                                    </div>
                                </div>
                            </div>
                            <!-- Page body end -->
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="../files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery\js\jquery.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-ui\js\jquery-ui.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\popper.js\js\popper.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap\js\bootstrap.min.js'?>"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js'?>"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\modernizr.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\css-scrollbars.js'?>"></script>

<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next\js\i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-i18next\js\jquery-i18next.min.js'?>"></script>
<!-- Custom js -->

<script src="<?php echo base_url().'assets\files\assets\js\pcoded.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\vartical-layout.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\jquery.mCustomScrollbar.concat.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\js\script.js'?>"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>

</html>
