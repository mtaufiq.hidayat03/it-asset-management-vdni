<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view("layout/head.php") ?>
	<body class="no-skin">
		<?php $this->load->view("layout/navbar.php") ?>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<?php $this->load->view("layout/sidebar.php") ?>
			
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="space-6"></div>
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-large">
												<center><h3><b>DATA ALAT</b></h3></center>
											</div>
											 
											<div class="widget-body">
												<div class="widget-main padding-24">
													<div class="row">
														<div class="col-sm-8">
															<div>
																<ul class="list-unstyled spaced">
																	<li>
																		Nama Alat : <?php echo $data['alat_nama']; ?>
																	</li>
																	<li>
																		User Alat : <?php echo $data['karyawan_nama']; ?>
																	</li>
																	<li>
																		Lokasi : <?php echo $data['lokasi_nama']; ?>
																	</li>
																	<li>
																		Divisi : <?php echo $data['divisi_nama']; ?>
																	</li>
																</ul>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="row">
																
															</div>
															<div>
																<ul class="list-unstyled  spaced">
																    <?php
													$no=0;
													foreach ($databaru->result_array() as $i) :
													   $no++;
													   $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];                       
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
													   $qr_code=$i['qr_code'];
													   
													?>
																	<li>
																	<img style="width: 70px;" src="<?php echo base_url().'assets/images/'.$qr_code;?>">
																	</li>
																	<?php endforeach;?>
																																		
																</ul>
															</div>
														</div>
													</div>
													<div class="space"></div>
													<div>
																																								
														<center><h5><b>SPESIFIKASI DETAIL ALAT</b></h5></center>
														<table class="table table-striped table-bordered">
															<thead>																
																<tr>																	
																	<th class="hidden-xs"><center>Merek</center></th>
																	<th><center>Nomor Seri</center></th>
																	<th><center>Spesifikasi</center></th>
																	<th><center>Status</center></th>
																</tr>
															</thead>
															<tbody>
																<tr>																	
																	<td class="hidden-xs"><?php echo $data['merek']; ?></td>
																	<td><?php echo $data['serial']; ?></td>
																	<td><?php echo $data['deskripsi']; ?></td>
																	<td><?php echo $data['status_nama']; ?></td>
																</tr>																																																
															</tbody>
														</table>
														
													</div>
													<div class="hr hr8 hr-double hr-dotted"></div>									
													<div class="space-6"></div>	
													<!--<div class="widget-toolbar hidden-480">
																	<a href="#">
																	<span class="invoice-info-label">Print:</span>
																		<i class="ace-icon fa fa-print">
																		</i>
																	</a>
													</div>	-->
	<script>
		window.print();
	</script>													
												</div>
											</div><!-- BODY-->											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
		
		


















		

			<?php $this->load->view("layout/footer.php") ?>
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>		
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-2.1.4.min.js'?>"></script>		
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-1.11.3.min.js'?>"></script>
		<script type="text/javascript">
		if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url().'assets/ace/assets/js/bootstrap.min.js'?>"></script>	
		<script src="<?php echo base_url().'assets/ace/assets/js/excanvas.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-ui.custom.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.ui.touch-punch.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.easypiechart.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.sparkline.index.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.flot.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.flot.pie.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.flot.resize.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace-elements.min.js'?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
		<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace.min.js'?>"></script>
		<script type="text/javascript">
		
		
		
		
			jQuery(function($) {
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: ace.vars['old_ie'] ? false : 1000,
						size: size
					});
				})
			
				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html',
									 {
										tagValuesAttribute:'data-values',
										type: 'bar',
										barColor: barColor ,
										chartRangeMin:$(this).data('min') || 0
									 });
				});
			
			
			  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
			  //but sometimes it brings up errors with normal resize event handlers
			  $.resize.throttleWindow = false;
			
			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "social networks",  data: 38.7, color: "#68BC31"},
				{ label: "search engines",  data: 24.5, color: "#2091CF"},
				{ label: "ad campaigns",  data: 8.2, color: "#AF4E96"},
				{ label: "direct traffic",  data: 18.6, color: "#DA5430"},
				{ label: "other",  data: 10, color: "#FEE074"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			
				/////////////////////////////////////
				$(document).one('ajaxloadstart.page', function(e) {
					$tooltip.remove();
				});
			
			
			
			
				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}
			
				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}
			
				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}
				
			
				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});
			
			
				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			
			
				$('.dialogs,.comments').ace_scroll({
					size: 300
			    });
				
				
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if(ace.vars['touch'] && ace.vars['android']) {
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				  });
				}
			
				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {
						//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});
			
			
				//show the dropdowns on top or bottom depending on window height and menu position
				$('#task-tab .dropdown-hover').on('mouseenter', function(e) {
					var offset = $(this).offset();
			
					var $w = $(window)
					if (offset.top > $w.scrollTop() + $w.innerHeight() - 100) 
						$(this).addClass('dropup');
					else $(this).removeClass('dropup');
				});
			
			})
		</script>
		<script>
    $(document).ready(function(){
        $('#mydata').DataTable();
    });
</script>
	</body>
</html>
