<!DOCTYPE html>
<html lang="en">

<head>
    <title>IMS-USER SISTEM </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url().'assets\files\assets\images\favicon.ico" type="image/x-icon'?>">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap\css\bootstrap.min.css'?>">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\themify-icons\themify-icons.css'?>">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\icofont\css\icofont.css'?>">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\feather\css\feather.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\lightbox2\css\lightbox.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\pages\data-table\css\buttons.dataTables.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\jquery.mCustomScrollbar.css'?>">
</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">

                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="<?php echo base_url().'admin/dashboard'?>">
						<div class="pcoded-navigatio-lavel">IMS VDNI V.2</div>
                            <!--<img class="img-fluid" src="<?php echo base_url().'assets\files\assets\images\vdni.jpeg" alt="Theme-Logo'?>">-->
                        </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>

                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-bell"></i>
                                    <span class="badge bg-c-pink">5</span>
                                </div>
                                <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <h6>Notifications</h6>
                                        <label class="label label-danger">New</label>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">John Doe</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Joseph William</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Sara Soudein</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-message-square"></i>
                                    <span class="badge bg-c-green">3</span>
                                </div>
                            </div>
                        </li>
                        <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <a href="#">
                                                <i class="feather icon-settings"></i> <span class="hidden-xs"><?php echo $this->session->userdata("user_name") ?></span>
                                            </a>
                                        
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        
                                        <li>
                                            <a href="#">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Sidebar chat start -->
        <div id="sidebar" class="users p-chat-user showChat">
            <div class="had-container">
                <div class="card card_main p-fixed users-main">
                    <div class="user-box">
                        <div class="chat-inner-header">
                            <div class="back_chatBox">
                                <div class="right-icon-control">
                                    <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                    <div class="form-icon">
                                        <i class="icofont icofont-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-friend-list">
                            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image ">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Josephin Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Lary Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alice</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alia</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Suzen</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat start-->
        <div class="showChat_inner">
            <div class="media chat-inner-header">
                <a class="back_chatBox">
                    <i class="feather icon-chevron-left"></i> Josephin Doe
                </a>
            </div>
            <div class="media chat-messages">
                <a class="media-left photo-table" href="#!">
                    <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                </a>
                <div class="media-body chat-menu-content">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
            <div class="media chat-messages">
                <div class="media-body chat-menu-reply">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media-right photo-table">
                    <a href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                    </a>
                </div>
            </div>
            <div class="chat-reply-box p-b-20">
                <div class="right-icon-control">
                    <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                    <div class="form-icon">
                        <i class="feather icon-navigation"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel"></div>
                            <ul class="pcoded-item pcoded-left-item">
                                                             
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext">MASTER </span>
                                        <span class="pcoded-badge label label-danger">M</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/kategori'?>">
                                                <span class="pcoded-mtext">KATEGORI ALAT</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/device'?>">
                                                <span class="pcoded-mtext">ALAT</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/status'?>">
                                                <span class="pcoded-mtext">STATUS ALAT</span>
                                            </a>
                                        </li>
                                        
										<li class="">
                                            <a href="<?php echo base_url().'admin/lokasi'?>">
                                                <span class="pcoded-mtext">LOKASI KERJA</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/divisi'?>">
                                                <span class="pcoded-mtext">DIVISI</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/jabatan'?>">
                                                <span class="pcoded-mtext">JABATAN</span>
                                            </a>
                                        </li>
										

                                    </ul>
                                </li>
                            </ul>
                            
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                                        <span class="pcoded-mtext">ALAT/TOOLS</span>
										<span class="pcoded-badge label label-danger">A</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alat'?>">
                                                <span class="pcoded-mtext">ASSIGN DEVICE</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/stok'?>">
                                                <span class="pcoded-mtext">STOK</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/rusak'?>">
                                                <span class="pcoded-mtext">RUSAK</span>
                                            </a>
                                        </li>
										
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                                        <span class="pcoded-mtext">KARYAWAN</span>
										<span class="pcoded-badge label label-danger">K</span>
                                    </a>
                                    <ul class="pcoded-submenu">                                        
                                        
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/adduser'?>">
                                                <span class="pcoded-mtext">ENTRY DATA</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/user'?>">
                                                <span class="pcoded-mtext">LIST USER</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">LAPORAN</span>
										<span class="pcoded-badge label label-danger">L</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatlokasi'?>">
                                                <span class="pcoded-mtext">ALAT PER LOKASI</span>
                                            </a>
                                        </li>
                                       
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatdivisi'?>">
                                                <span class="pcoded-mtext">ALAT PER DIVISI</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/lapstok'?>">
                                                <span class="pcoded-mtext">LAPORAN STOK</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/alatuser'?>">
                                                <span class="pcoded-mtext">ALAT PER USER</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
								<li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">SISTEM LOG</span>
										<span class="pcoded-badge label label-danger">S</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/aktivitas'?>">
                                                <span class="pcoded-mtext">USER LOG</span>
                                            </a>
                                        </li>
                                        
                                        
										

                                    </ul>
                                </li>
                               
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                                        <span class="pcoded-mtext">PENCARIAN DATA</span>
										<span class="pcoded-badge label label-danger">P</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/data'?>">
                                                <span class="pcoded-mtext">FILTER DATA</span>
                                            </a>
                                        </li>
                                        
                                        
                                        
                                        
                                    </ul>
                                </li>
								<li class=" ">
                                    <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                        <span class="pcoded-micon"><i class="feather icon-cpu"></i></span>
                                        <span class="pcoded-mtext">KELUAR</span>
                                       
                                    </a>
                                </li>
                            </ul>                           
                        </div>
                    </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-8">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>List user </h4>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="page-header-breadcrumb">
                                                <ul class="breadcrumb-title">
                                                    <li class="breadcrumb-item">
                                                        <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">Master Karyawan</a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">List data</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-header end -->

                                <!-- Page-body start -->
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- Zero config.table start -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5><a class="btn btn-primary btn-skew" href="<?php echo base_url().'admin/adduser'?> "target="blank"><span class="fa fa-plus"></span> Add User</a></h5>

 <!--<a class="btn btn-primary btn-skew" href="<?php echo base_url().'admin/adduser'?> "target="blank"><span class="fa fa-plus"></span> Add User</a>-->                                               
												</div>
                                                <div class="card-block">
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                <thead>
							<tr>
								
								<th>NO</th>
								<th>FOTO</th>
								<th>NAMA</th>
								<!--<th>USERNAME</th>				
								<th>PASSWORD</th>-->
								<th>NIK</th>
								<th>DIVISI</th>
								<th>LEVEL</th>
								<th>LOKASI</th>
								<th>ROLE</th>
								<th>STATUS LOG</th>
								<th style="text-align:center;">Aksi</th>
							</tr>
                </thead>
                <tbody>
				<?php
                foreach($user->result_array() as $u):
							$id_user=$u['id_user'];
							$nama_user=$u['nama_user'];
							$username=$u['username'];
							$password=$u['password'];
          					$nik=$u['nik'];
          					$divisi_nama=$u['divisi_nama'];
          					$nama_jabatan=$u['nama_jabatan'];
          					$lokasi_kerja=$u['lokasi_kerja'];
							$role=$u['role'];
          					$foto=$u['foto'];
							$status=$u['status'];
				?>
				<tr>
					
					
					<td><?php echo $id_user ?></td>
					<td>
					<div class="col-lg-2 col-sm-6">
					<div class="thumbnail">
					<div class="thumb">
							<a href="<?php echo base_url().'assets/images/'.$foto;?>" data-lightbox="2" data-title="<?php echo $nama_user ?>">
								<img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/images/'.$foto;?>" alt="" class="img-fluid ">
                            </a>
                        </div>
						</div>
						</div>
					</td>
					<!--<td><img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/images/'.$foto;?>"></td>-->
					<td> <a class="btn" data-toggle="modal" data-target="#ModalEdit1<?php echo $id_user;?>"><?php echo $nama_user ?></a></td>
					<!--<td><?php echo $username ?></td>
					<td><?php echo $password ?></td>-->
					<td><?php echo $nik ?></td>
					<td><?php echo $divisi_nama ?></td>
					<td><?php echo $nama_jabatan ?></td> 
					<td><?php echo $lokasi_kerja ?></td>
					<td><?php echo $role ?></td>
										
					    <?php if($status=='1'):?>
                        <td><span class="label label-success">Aktif</span></td>
                        <?php else:?>
                        <td><span class="label label-danger">Non Aktif</span></td>
                        <?php endif;?>
					
				  <td class="text-center">
                                                                            <a class="dropdown-toggle addon-btn" data-toggle="dropdown" aria-expanded="true">
                                                                                <i class="icofont icofont-ui-settings"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $id_user;?>"><i class="icofont icofont-ui-edit"></i>Edit</a>
                                                                                
                                                                                <div role="separator" class="dropdown-divider"></div>
                                                                                <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $id_user;?>"><i class="ti-trash"></i>Hapus</a>
                                                                            </div>
                                                                    </td>
				</tr>
				<?php endforeach;?>
                </tbody>
                                                            <tfoot>
                                                            <tr>
                                <th>NO</th>
								<th>FOTO</th>
								<th>NAMA</th>
								<!--<th>USERNAME</th>				
								<th>PASSWORD</th>-->
								<th>NIK</th>
								<th>DIVISI</th>
								<th>LEVEL</th>
								<th>LOKASI</th>
								<th>ROLE</th>
								<th>STATUS LOG</th>
								<th style="text-align:center;">Aksi</th>
                                                                
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-body end -->
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

		
		
		<?php foreach ($user->result_array() as $u) :
					  
							$id_user=$u['id_user'];
							$nama_user=$u['nama_user'];
							$username=$u['username'];
							$password=$u['password'];
          					$nik=$u['nik'];
          					$divisi_nama=$u['divisi_nama'];
          					$nama_jabatan=$u['nama_jabatan'];
          					$lokasi_kerja=$u['lokasi_kerja'];
							$role=$u['role'];
          					$foto=$u['foto'];
							$status=$u['status'];
            ?>
	<!--Modal Edit Propinsi-->
        <div class="modal fade" id="ModalEdit<?php echo $id_user;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/user/edit_user'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
					
					
					
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Nama Karyawan</label>
                                    <div class="col-sm-7">
                                        <input type="hidden" name="id_user" value="<?php echo $id_user;?>"/> 
                                        <input type="text" name="nama_user" class="form-control form-control-round" id="nama_user" value="<?php echo $nama_user;?>" placeholder="nama karyawan" required>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Employee No</label>
                                    <div class="col-sm-7">
                                       <input type="text" name="nik" class="form-control form-control-round" id="nik" value="<?php echo $nik;?>" placeholder="nik" required>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Username</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="username" class="form-control form-control-round" id="username" value="<?php echo $username;?>" placeholder="username" required>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Password</label>
                                    <div class="col-sm-7">
                                       <input type="text" name="password" class="form-control form-control-round" id="password" value="***********************" placeholder="password" required>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Lokasi Kerja</label>
                                    <div class="col-sm-7">
                                       <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-round">
													<option value="0"><?php echo $lokasi_kerja;?></option>
													<?php foreach($lok->result() as $row):?>
														<option value="<?php echo $row->lokasi_nama;?>"><?php echo $row->lokasi_nama;?></option>
													<?php endforeach;?>
												</select>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Divisi</label>
                                    <div class="col-sm-7">
                                      <select name="divisi_nama" id="divisi_nama" class="form-control form-control-round">
													<option value="0"><?php echo $divisi_nama;?></option>
													<?php foreach($divisi->result() as $row):?>
														<option value="<?php echo $row->divisi_nama;?>"><?php echo $row->divisi_nama;?></option>
													<?php endforeach;?>
												</select>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Jabatan</label>
                                    <div class="col-sm-7">
                                      <select name="nama_jabatan" id="nama_jabatan" class="form-control form-control-round">
																	<option value="0"><?php echo $nama_jabatan;?></option>
																	<?php foreach($data->result() as $row):?>
																		<option value="<?php echo $row->nama_jabatan;?>"><?php echo $row->nama_jabatan;?></option>
																	<?php endforeach;?>
																</select>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Log User</label>
                                    <div class="col-sm-7">
                                      <select class="form-control form-control-round" name="role" required>
																<option value="1">Admin</option>
																<option value="2">Operator</option>
															</select>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-7">
                                      <select class="form-control form-control-round" name="status" id="status" required>
														<option value="1">Aktif</option>
														<option value="2">Non Aktif</option>
													</select>
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Foto</label>
                                    <div class="col-sm-7">
                                      <input type="file" name="filefoto" />
                                    </div>
                                </div>
					
									
                                
                                    
									
									
									
									
									
									
									
									
									
									
									
									
									

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
		<div class="modal fade" id="ModalEdit1<?php echo $id_user;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Profil User</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/user/edit_user'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                                
                                    <!--<div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Nama Karyawan</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="id_user" value="<?php echo $id_user;?>"/> 
                                            <input type="text" name="nama_user" class="form-control form-control-round" id="nama_user" value="<?php echo $nama_user;?>" placeholder="nama karyawan" required>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Employee No</label>
                                        <div class="col-sm-8">	 
                                            <input type="text" name="nik" class="form-control form-control-round" id="nik" value="<?php echo $nik;?>" placeholder="nik" required>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Username</label>
                                        <div class="col-sm-8">	 
                                            <input type="text" name="username" class="form-control form-control-round" id="username" value="<?php echo $username;?>" placeholder="username" required>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Password</label>
                                        <div class="col-sm-8">	 
                                            <input type="text" name="password" class="form-control form-control-round" id="password" value="<?php echo $password;?>" placeholder="password" required>
                                        </div>
                                    </div>
									
									
									
									<div class="form-group">
											<label for="inputUserName" class="col-sm-4 control-label">Lokasi Kerja</label>
											<div class="col-md-8">
												<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-round">
													<option value="0"><?php echo $lokasi_kerja;?></option>
													<?php foreach($lok->result() as $row):?>
														<option value="<?php echo $row->lokasi_nama;?>"><?php echo $row->lokasi_nama;?></option>
													<?php endforeach;?>
												</select>
											</div>
									</div>
									<div class="form-group">
											<label for="inputUserName" class="col-sm-4 control-label">Divisi</label>
											<div class="col-md-8">
												<select name="divisi_nama" id="divisi_nama" class="form-control form-control-round">
													<option value="0"><?php echo $divisi_nama;?></option>
													<?php foreach($divisi->result() as $row):?>
														<option value="<?php echo $row->divisi_nama;?>"><?php echo $row->divisi_nama;?></option>
													<?php endforeach;?>
												</select>
											</div>
									</div>
									<div class="form-group ">
															<label for="inputUserName" class="col-sm-4 control-label">Jabatan</label>
															<div class="col-sm-8">
																<select name="nama_jabatan" id="nama_jabatan" class="form-control form-control-round">
																	<option value="0"><?php echo $nama_jabatan;?></option>
																	<?php foreach($data->result() as $row):?>
																		<option value="<?php echo $row->nama_jabatan;?>"><?php echo $row->nama_jabatan;?></option>
																	<?php endforeach;?>
																</select>
															</div>
													    </div>
									<div class="form-group">
														<label for="inputUserName" class="col-sm-4 control-label">Log User</label>
														<div class="col-sm-8">
															<select class="form-control form-control-round" name="role" required>
																<option value="1">Admin</option>
																<option value="2">Operator</option>
															</select>
														</div>
														</div>
									<div class="form-group ">
											<label for="status" class="col-sm-2 control-label">Status</label>
												<div class="col-sm-8">
													<select class="form-control form-control-round" name="status" id="status" required>
														<option value="1">Aktif</option>
														<option value="2">Non Aktif</option>
													</select>
												</div>
									</div>
									<div class="form-group ">
														<label for="inputUserName" class="col-sm-3 control-label">Photo </label>
														<div class="col-sm-8">
															<input type="file" name="filefoto" />
														</div>
													    </div>-->
									

												<div class="col-xl-12 col-md-20">
                                                <div class="card user-card-full">
                                                    <div class="row m-l-0 m-r-0">
                                                        <div class="col-sm-4 bg-c-lite-green user-profile">
                                                            <div class="card-block text-center text-white">
                                                                <div class="m-b-25">
																	<img width="65" height="65" class="img-circle" src="<?php echo base_url().'assets/images/'.$foto;?>">
                                                                    
                                                                </div>
                                                                <h2 class="f-w-600"><?php echo $username;?></h2>
                                                               
																 
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <div class="card-block">
                                                                <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Informasi</h6>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Nama</p>
                                                                        <h6 class="text-muted f-w-400"><?php echo $nama_user;?></h6>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Nik</p>
                                                                        <h6 class="text-muted f-w-400"><?php echo $nik ?></h6>
                                                                    </div>
                                                                </div>
                                                                <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Detail</h6>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Lokasi Kerja</p>
                                                                        <h6 class="text-muted f-w-400"><?php echo $lokasi_kerja ?></h6>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Divisi</p>
                                                                        <h6 class="text-muted f-w-400"><?php echo $divisi_nama ?></h6>
                                                                    </div>
																	<div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Jabatan</p>
                                                                        <h6 class="text-muted f-w-400"><?php echo $nama_jabatan ?></h6>
                                                                    </div>
																	<div class="col-sm-6">
                                                                        <p class="m-b-10 f-w-600">Status</p>
                                                                        <h6 class="text-muted f-w-400"><?php if($status=='1'):?>
																	<td><span class="label label-success">Aktif</span></td>
																	<?php else:?>
																	<td><span class="label label-danger">Non Aktif</span></td>
																	<?php endif;?></h6>
                                                                    </div>
																	
																	
																	
                                                                </div>
                                                               <!-- <ul class="social-link list-unstyled m-t-40 m-b-10">
                                                                    <li><a href="#!" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="facebook"><i class="feather icon-facebook facebook" aria-hidden="true"></i></a></li>
                                                                    <li><a href="#!" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="twitter"><i class="feather icon-twitter twitter" aria-hidden="true"></i></a></li>
                                                                    <li><a href="#!" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="instagram"><i class="feather icon-instagram instagram" aria-hidden="true"></i></a></li>
                                                                </ul>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                    </div>
					
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>
	
	<?php foreach ($user->result_array() as $u) :
							$id_user=$u['id_user'];
							$nama_user=$u['nama_user'];
							$username=$u['username'];
							$password=$u['password'];
          					$nik=$u['nik'];
          					$divisi_nama=$u['divisi_nama'];
          					$nama_jabatan=$u['nama_jabatan'];
          					$lokasi_kerja=$u['lokasi_kerja'];
							$role=$u['role'];
          					$foto=$u['foto'];
							$status=$u['status'];
            ?>
	<!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus<?php echo $id_user;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/user/hapus_user'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
							<input type="hidden" name="id_user" value="<?php echo $id_user;?>"/> 
                            <p>Apakah Anda yakin mau menghapus User<b><?php echo $nama_user;?></b> ?</p>
                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery\js\jquery.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-ui\js\jquery-ui.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\popper.js\js\popper.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap\js\bootstrap.min.js'?>"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js'?>"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\modernizr.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\css-scrollbars.js'?>"></script>

<!-- data-table js -->
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net\js\jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\jszip.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\pdfmake.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\vfs_fonts.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.print.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.html5.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js'?>"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\lightbox2\js\lightbox.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next\js\i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-i18next\js\jquery-i18next.min.js'?>"></script>
<!-- Custom js -->
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\data-table-custom.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\pcoded.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\vartical-layout.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\jquery.mCustomScrollbar.concat.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\js\script.js'?>"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': false
        })

    </script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: " Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: " berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: " Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>

</html>
