<!DOCTYPE html>
<html lang="en">

<head>
    <title>IMS-Stok Alat </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url().'assets\files\assets\images\favicon.ico" type="image/x-icon'?>">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\lightbox2\css\lightbox.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap\css\bootstrap.min.css'?>">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\themify-icons\themify-icons.css'?>">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\icofont\css\icofont.css'?>">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\icon\feather\css\feather.css'?>">
    <!-- Data Table Css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\pages\advance-elements\css\bootstrap-datetimepicker.css'?>">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\bootstrap-daterangepicker\css\daterangepicker.css'?>">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datedropper\css\datedropper.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\spectrum\css\spectrum.css'?>">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\jquery-minicolors\css\jquery.minicolors.css'?>">
   

	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\pages\data-table\css\buttons.dataTables.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets\files\assets\css\jquery.mCustomScrollbar.css'?>">
</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">

                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="<?php echo base_url().'admin/dashboard'?>">
						<div class="pcoded-navigatio-lavel">IMS VDNI V.2</div>
                            <!--<img class="img-fluid" src="<?php echo base_url().'assets\files\assets\images\vdni.jpeg" alt="Theme-Logo'?>">-->
                        </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>

                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-bell"></i>
                                    <span class="badge bg-c-pink">5</span>
                                </div>
                                <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <h6>Notifications</h6>
                                        <label class="label label-danger">New</label>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">John Doe</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Joseph William</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Sara Soudein</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-message-square"></i>
                                    <span class="badge bg-c-green">3</span>
                                </div>
                            </div>
                        </li>
                       <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <a href="#">
                                                <i class="feather icon-settings"></i> <span class="hidden-xs"><?php echo $this->session->userdata("user_name") ?></span>
                                            </a>
                                        
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        
                                        <li>
                                            <a href="#">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Sidebar chat start -->
        <div id="sidebar" class="users p-chat-user showChat">
            <div class="had-container">
                <div class="card card_main p-fixed users-main">
                    <div class="user-box">
                        <div class="chat-inner-header">
                            <div class="back_chatBox">
                                <div class="right-icon-control">
                                    <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                    <div class="form-icon">
                                        <i class="icofont icofont-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-friend-list">
                            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image ">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Josephin Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Lary Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alice</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alia</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="..\files\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Suzen</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat start-->
        <div class="showChat_inner">
            <div class="media chat-inner-header">
                <a class="back_chatBox">
                    <i class="feather icon-chevron-left"></i> Josephin Doe
                </a>
            </div>
            <div class="media chat-messages">
                <a class="media-left photo-table" href="#!">
                    <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                </a>
                <div class="media-body chat-menu-content">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
            <div class="media chat-messages">
                <div class="media-body chat-menu-reply">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media-right photo-table">
                    <a href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="..\files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                    </a>
                </div>
            </div>
            <div class="chat-reply-box p-b-20">
                <div class="right-icon-control">
                    <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                    <div class="form-icon">
                        <i class="feather icon-navigation"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                 <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel"></div>
                            <ul class="pcoded-item pcoded-left-item">
                                                             
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext">MASTER </span>
                                        <span class="pcoded-badge label label-danger">M</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/kategori'?>">
                                                <span class="pcoded-mtext">KATEGORI ALAT</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/device'?>">
                                                <span class="pcoded-mtext">ALAT</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/status'?>">
                                                <span class="pcoded-mtext">STATUS ALAT</span>
                                            </a>
                                        </li>
                                        
										<li class="">
                                            <a href="<?php echo base_url().'admin/lokasi'?>">
                                                <span class="pcoded-mtext">LOKASI KERJA</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/divisi'?>">
                                                <span class="pcoded-mtext">DIVISI</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?php echo base_url().'admin/jabatan'?>">
                                                <span class="pcoded-mtext">JABATAN</span>
                                            </a>
                                        </li>
										

                                    </ul>
                                </li>
                            </ul>
                            
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                                        <span class="pcoded-mtext">ALAT/TOOLS</span>
										<span class="pcoded-badge label label-danger">A</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alat'?>">
                                                <span class="pcoded-mtext">ASSIGN DEVICE</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/stok'?>">
                                                <span class="pcoded-mtext">STOK</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/rusak'?>">
                                                <span class="pcoded-mtext">RUSAK</span>
                                            </a>
                                        </li>
										
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                                        <span class="pcoded-mtext">KARYAWAN</span>
										<span class="pcoded-badge label label-danger">K</span>
                                    </a>
                                    <ul class="pcoded-submenu">                                        
                                        
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/adduser'?>">
                                                <span class="pcoded-mtext">ENTRY DATA</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/user'?>">
                                                <span class="pcoded-mtext">LIST USER</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">LAPORAN</span>
										<span class="pcoded-badge label label-danger">L</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatlokasi'?>">
                                                <span class="pcoded-mtext">ALAT PER LOKASI</span>
                                            </a>
                                        </li>
                                       
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/alatdivisi'?>">
                                                <span class="pcoded-mtext">ALAT PER DIVISI</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/lapstok'?>">
                                                <span class="pcoded-mtext">LAPORAN STOK</span>
                                            </a>
                                        </li>
										<li class=" ">
                                            <a href="<?php echo base_url().'admin/alatuser'?>">
                                                <span class="pcoded-mtext">ALAT PER USER</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
								<li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">SISTEM LOG</span>
										<span class="pcoded-badge label label-danger">S</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/aktivitas'?>">
                                                <span class="pcoded-mtext">USER LOG</span>
                                            </a>
                                        </li>
                                        
                                        
										

                                    </ul>
                                </li>
                               
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                                        <span class="pcoded-mtext">PENCARIAN DATA</span>
										<span class="pcoded-badge label label-danger">P</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url().'admin/data'?>">
                                                <span class="pcoded-mtext">FILTER DATA</span>
                                            </a>
                                        </li>
                                        
                                        
                                        
                                        
                                    </ul>
                                </li>
								<li class=" ">
                                    <a href="<?php echo base_url().'admin/dashboard/logout'?>">
                                        <span class="pcoded-micon"><i class="feather icon-cpu"></i></span>
                                        <span class="pcoded-mtext">KELUAR</span>
                                       
                                    </a>
                                </li>
                            </ul>                           
                        </div>
                    </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-8">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Stok Alat</h4>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="page-header-breadcrumb">
                                                <ul class="breadcrumb-title">
                                                    <li class="breadcrumb-item">
                                                        <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">Master Alat</a>
                                                    </li>
                                                    <li class="breadcrumb-item"><a href="#!">Stok</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-header end -->

                                <!-- Page-body start -->
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- Zero config.table start -->
                                            <div class="card">
                                                <div class="card-header">
                                                    
													<button type="button" class="btn btn-primary btn-skew" data-toggle="modal" data-target="#large-Modal">Add Stok</button>
                                                    <span></span>

                                                </div>
                                                <div class="card-block">
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                                                            <thead>
                <tr>		
							
          					<th style="width:40px;">Nama Alat</th>							
							<th style="width:40px;">Kategori</th>
          					<th style="width:40px;">Merek</th>
          					<th style="width:40px;">Model</th>
          					<th style="width:40px;">Tipe</th> 
							<th style="width:40px;">Serial</th>
							<th style="width:40px;">Status</th>
							<th style="width:40px;">Jumlah</th>
							<th style="width:40px;">Ket</th>
							<th style="width:40px;">Capitalization Date</th>
														
							
                </tr>
                </thead>
                <tbody>
				<?php
					$no=0;
				foreach ($stok->result_array() as $i) :
                       $no++;
					   $stok_id=$i['stok_id'];
					   $foto=$i['foto'];
					   $aset_nama=$i['aset_nama'];
                       $kategori_nama=$i['kategori_nama'];
                       $merek_nama=$i['merek_nama'];
                       $model_nama=$i['model_nama'];
                       $tipe_nama=$i['tipe_nama'];
					   $serial_nama=$i['serial_nama'];
                       $status_nama=$i['status_nama'];                       
                       $ket_nama=$i['ket_nama'];
					   $tgl=$i['tgl'];
					   $jml=$i['jml'];
                    ?>
				<tr>
					
					
					
					<td><div class="col-lg-2 col-sm-6">
					<div class="thumbnail">
					<div class="thumb">
							<a href="<?php echo base_url().'assets/images/'.$foto;?>" data-lightbox="2" data-title="<?php echo $aset_nama ?>-<?php echo $kategori_nama ?>-<?php echo $merek_nama ?>-<?php echo $model_nama ?>-<?php echo $tipe_nama ?>-<?php echo $serial_nama ?>-<?php echo $tgl ?>">
								<img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/images/'.$foto;?>" alt="" class="img-fluid ">
                            </a>
                        </div>
						</div>
						<a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $aset_nama ?></a>
						</div>
					</td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $kategori_nama ?></a></td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $merek_nama ?></a></td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $model_nama ?></a></td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $tipe_nama ?></a></td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $serial_nama ?></a></td>
					<td>
					<?php if($status_nama=='BARU'):?><span class="label label-success">BARU</span>
					<?php else:?><span class="label label-danger"><?php echo $status_nama ?></span>
					<?php endif;?>
						</td>
										
					  
					
					
                        
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $jml ?></a></td>
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $ket_nama ?></a></td>					
					<td><a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><?php echo $tgl ?></a></td>
									
					
				  <!--<td class="text-center">
                                                                            <a class="dropdown-toggle addon-btn" data-toggle="dropdown" aria-expanded="true">
                                                                                <i class="icofont icofont-ui-settings"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $stok_id;?>"><i class="icofont icofont-ui-edit"></i>Edit</a>
                                                                                
                                                                                <div role="separator" class="dropdown-divider"></div>
                                                                                <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $stok_id;?>"><i class="ti-trash"></i>Hapus</a>
                                                                            </div>
                                                                    </td>-->
				</tr>
				<?php endforeach;?>
                </tbody>
                                                            <tfoot>
                                                            <tr>
                                                               
							
          					<th style="width:40px;">Nama Alat</th>							
							<th style="width:40px;">Kategori</th>
          					<th style="width:40px;">Merek</th>
          					<th style="width:40px;">Model</th>
          					<th style="width:40px;">Tipe</th> 
							<th style="width:40px;">Serial</th>
							<th style="width:40px;">Status</th>
							<th style="width:40px;">Jml</th>
							<th style="width:40px;">Ket</th>
							<th style="width:40px;">Capitalization Date</th>							
							
                                                                
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-body end -->
                            </div>
                        </div>
                        <!-- Main-body end -->
                        <div id="styleSelector">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="large-Modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Add Stok</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
									</button>
								</div>
					
					<form class="form-horizontal" action="<?php echo base_url().'admin/stok/simpan_stok'?>" method="post" enctype="multipart/form-data">
                             <div class="modal-body">
                                 <div class="row">
                                        <div class="col-sm-12">
                                           
                                            <div class="card">
                                                
                                                <div class="card-block">
                                                    
                                                    
                                                                                                         
       													<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Device</label>
                                                            <div class="col-sm-8">
                                                                <select name="aset_nama" id="aset_nama" class="form-control form-control-round">
                                                                   <option value="0"></option>
																   <?php foreach($device->result() as $row):?>
																	<option value="<?php echo $row->dev_nama;?>"><?php echo $row->dev_nama;?>(<?php echo $row->dev_merek;?>-<?php echo $row->dev_serial;?>)</option>
																   <?php endforeach;?>
																	
                                                                </select>
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Kategori</label>
                                                            <div class="col-sm-8">
                                                                <select name="kategori_nama" id="kategori_nama" class="form-control form-control-round">
                                                                   <option value="0"></option>
																   <?php foreach($kategori->result() as $row):?>
																		<option value="<?php echo $row->kategori_nama;?>"><?php echo $row->kategori_nama;?></option>
																   <?php endforeach;?>
																	
                                                                </select>
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Merek</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="merek_nama" class="form-control form-control-round" id="merek_nama" placeholder="merek">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Model</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="model_nama" class="form-control form-control-round" id="model_nama" placeholder="model">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Tipe</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="tipe_nama" class="form-control form-control-round" id="tipe_nama" placeholder="tipe">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Serial</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="serial_nama" class="form-control form-control-round" id="serial_nama" placeholder="serial">
                                                            </div>
                                                        </div>
														<div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Jumlah</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="jml" class="form-control form-control-round" id="jml" placeholder="jumlah">
                                                            </div>
                                                        </div>
														

                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Keadaan Alat</label>
                                                            <div class="col-sm-8">
                                                                <select name="status_nama" id="status_nama" class="form-control form-control-round">
																	<option value="0"></option>
																	<?php foreach($status->result() as $row):?>
																		<option value="<?php echo $row->status_nama;?>"><?php echo $row->status_nama;?></option>
																	<?php endforeach;?>
																</select>
                                                            </div>
                                                        </div>
														<div class="form-group row">
															<label for="inputUserName" class="col-sm-3 col-form-label">Capitalization Date</label>
															<div class="col-md-8">
																	<input type="date" name="tgl" id="#dropper-border" class="form-control form-control-round" />
															
															</div>
														</div>
															
														<div class="form-group row">
																<label for="inputUserName" class="col-sm-3 control-label">Keterangan</label>
															   <div class="col-md-8">
																	<textarea name="ket_nama" class="form-control form-control-round" id="ket_nama" rows="3"></textarea>
															   </div>
														</div>
														<div class="form-group row">
															<label for="inputUserName" class="col-sm-3 control-label">Photo </label>
															<div class="col-sm-7">
																<input type="file" name="filefoto" required/>
															</div>
														</div> 
																												
														<div class="modal-footer">
															<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Tutup</button>
															<button type="submit" class="btn btn-primary waves-effect waves-light ">Simpan</button>
														</div>                                                    									                                                                                                        
                                                </div>
												
                                            </div>                                           
                                    </div>
                                </div>
                            </div>
						</form>                                                                            
                    </div>
                </div>
</div>
		
		<?php foreach ($stok->result_array() as $i) :
					   $stok_id=$i['stok_id'];
					   $foto=$i['foto'];
					   $aset_nama=$i['aset_nama'];
                       $kategori_nama=$i['kategori_nama'];
                       $merek_nama=$i['merek_nama'];
                       $model_nama=$i['model_nama'];
                       $tipe_nama=$i['tipe_nama'];
					   $serial_nama=$i['serial_nama'];
                       $status_nama=$i['status_nama'];                       
                       $ket_nama=$i['ket_nama'];
					   $tgl=$i['tgl'];
					   $jml=$i['jml'];
            ?>
	<!--Modal Edit Pengguna-->
        <div class="modal fade" id="ModalEdit<?php echo $stok_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Stok</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/stok/edit_stok'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
					
					
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Device</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="aset_nama" value="<?php echo $aset_nama;?>"/> 
                                            <input type="text" name="aset_nama" class="form-control form-control-round" id="inputUserName" value="<?php echo $aset_nama;?>" placeholder="aset nama" >
                                        </div>
                                    </div>
                                
                                    <div class="form-group row">
											<label for="inputUserName" class="col-sm-3 control-label">Kategori Alat</label>
											<div class="col-md-8">
												
												
												<select name="kategori_nama" id="kategori_nama" class="form-control form-control-round">
													<option value="0"><?php echo $kategori_nama;?></option>
													<?php foreach($kategori->result() as $row):?>
														<option value="<?php echo $row->kategori_nama;?>"><?php echo $row->kategori_nama;?></option>
													<?php endforeach;?>
												</select>
											</div>
									</div>
									
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 col-form-label">Merek</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="merek_nama" value="<?php echo $merek_nama;?>"/> 
                                            <input type="text" name="merek_nama" class="form-control form-control-round" id="merek_nama" value="<?php echo $merek_nama;?>" placeholder="merek" >
                                        </div>
                                    </div>
									
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Model</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="model_nama" value="<?php echo $model_nama;?>"/> 
                                            <input type="text" name="model_nama" class="form-control form-control-round" id="inputUserName" value="<?php echo $model_nama;?>" placeholder="model" >
                                        </div>
                                    </div>
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Tipe</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="tipe_nama" value="<?php echo $tipe_nama;?>"/> 
                                            <input type="text" name="tipe_nama" class="form-control form-control-round" id="inputUserName" value="<?php echo $tipe_nama;?>" placeholder="tipe" >
                                        </div>
                                    </div>
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Nomor Serial</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="serial_nama" value="<?php echo $serial_nama;?>"/> 
                                            <input type="text" name="serial_nama" class="form-control form-control-round" id="serial_nama" value="<?php echo $serial_nama;?>" placeholder="serial nomor" >
                                        </div>
                                    </div>
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Jumlah</label>
                                        <div class="col-sm-8">
											<input type="hidden" name="jumlah" value="<?php echo $jml;?>"/> 
                                            <input type="text" name="jml" class="form-control form-control-round" id="jml" value="<?php echo $jml;?>" placeholder="jumlah" >
                                        </div>
                                    </div>
									<div class="form-group row">
											<label for="inputUserName" class="col-sm-3 control-label">Keadaan Alat</label>
											<div class="col-md-8">
												<select name="status_nama" id="status_nama" class="form-control form-control-round">
													<option value="0"><?php echo $status_nama;?></option>
													<?php foreach($status->result() as $row):?>
														<option value="<?php echo $row->status_nama;?>"><?php echo $row->status_nama;?></option>
													<?php endforeach;?>
												</select>
											</div>
									</div>
									<div class="form-group row">
											<label for="inputUserName" class="col-sm-3 control-label">Keterangan</label>
										   <div class="col-md-8">
				
												<textarea name="ket_nama" class="form-control form-control-round" id="ket_nama" rows="4"></textarea>
									       </div>
									</div> 
									<div class="form-group row">
											<label for="inputUserName" class="col-sm-3 control-label">Capital. Date</label>
											<div class="col-md-8">
													<input type="hidden" name="tgl" value="<?php echo $tgl;?>"/>
													<input type="date" name="tgl" id="tgl" class="form-control form-control-round " />
											</div>
									</div>
									
									<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-3 control-label">Photo</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="filefoto" required/>
                                        </div>
                                    </div>
									
									

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
						<a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $stok_id;?>"><i class="ti-trash"></i>Hapus</a>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>
	
	<?php foreach ($stok->result_array() as $i) :
					   $stok_id=$i['stok_id'];
					   $foto=$i['foto'];
					   $aset_nama=$i['aset_nama'];
                       $kategori_nama=$i['kategori_nama'];
                       $merek_nama=$i['merek_nama'];
                       $model_nama=$i['model_nama'];
                       $tipe_nama=$i['tipe_nama'];
					   $serial_nama=$i['serial_nama'];
                       $status_nama=$i['status_nama'];                       
                       $ket_nama=$i['ket_nama'];
					   $tgl=$i['tgl'];
					   $jml=$i['jml'];
            ?>
	<!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus<?php echo $stok_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Stok</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/stok/hapus'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">       
							<input type="hidden" name="stok_id" value="<?php echo $stok_id;?>"/> 
                            <p>Apakah Anda yakin mau menghapus Stok <b><?php echo $aset_nama;?></b> ?</p>
                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery\js\jquery.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-ui\js\jquery-ui.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\popper.js\js\popper.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap\js\bootstrap.min.js'?>"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js'?>"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\modernizr.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\modernizr\js\css-scrollbars.js'?>"></script>

<!-- data-table js -->
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net\js\jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\jszip.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\pdfmake.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\vfs_fonts.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.print.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-buttons\js\buttons.html5.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js'?>"></script>

<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\advance-elements\moment-with-locales.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap-datepicker\js\bootstrap-datepicker.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\advance-elements\bootstrap-datetimepicker.min.js'?>"></script>
    <!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\bootstrap-daterangepicker\js\daterangepicker.js'?>"></script>
    <!-- Date-dropper js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\datedropper\js\datedropper.min.js'?>"></script>


<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\pages\advance-elements\custom-picker.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\spectrum\js\spectrum.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jscolor\js\jscolor.js'?>"></script>
    <!-- Mini-color js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-minicolors\js\jquery.minicolors.min.js'?>"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next\js\i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\jquery-i18next\js\jquery-i18next.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\bower_components\lightbox2\js\lightbox.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\pages\data-table\js\data-table-custom.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\pcoded.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\vartical-layout.min.js'?>"></script>
<script src="<?php echo base_url().'assets\files\assets\js\jquery.mCustomScrollbar.concat.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets\files\assets\js\script.js'?>"></script>
<script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': false
        })

    </script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: " Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: " berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: " Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>

</html>
