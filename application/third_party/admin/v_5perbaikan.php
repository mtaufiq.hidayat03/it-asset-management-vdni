<!DOCTYPE html>
<html lang="en">
<?php $this->load->view("layout/head1.php") ?>
	<body class="no-skin">
		<?php $this->load->view("layout/navbar1.php") ?>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
				<?php $this->load->view("layout/sidebar.php") ?>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Transfer Alat</a>
							</li>

							<li class="active">Perbaikan Alat</li>
						</ul>

						
					</div>

					<div class="page-content">
						
						

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
								
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header">
											
										</div>
										<div>
										<table id="dynamic-table" class="table table-striped table-bordered table-hover">
											
												<thead>
													<tr>
														<th>No</th>
														<th>Nama Alat</th>
														<th>Merek</th>
														<th>Nomor Seri</th>
														<th>Spesifikasi</th>
														<th>Status Alat</th>
														<th><center> Proses</center></th>
													</tr>
												</thead>

												<tbody>
													
													<?php
													$no=0;
													foreach ($data->result_array() as $i) :
													   $no++;
													   $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];                       
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
													   
													?>

													<tr>
														<td><?php echo $no ?></td>
														<td><?php echo $alat_nama ?></td>
														<td><?php echo $merek ?></td>
														<td><?php echo $serial ?></td>
														<td><?php echo $deskripsi ?></td>		
														
														<?php if($status_nama=='BARU'):?>
														<td><span class="label label-success">BARU</span></td>
														<?php elseif($status_nama=='TERPAKAI'):?>
														<td><span class="label label-default">TERPAKAI</span></td>
														<?php elseif($status_nama=='PERBAIKAN'):?>
														<td><span class="label label-warning">PERBAIKAN</span></td>
														<?php elseif($status_nama=='HILANG'):?>
														<td><span class="label label-danger">HILANG</span></td>
														<?php elseif($status_nama=='RUSAK'):?>
														<td><span class="label label-danger">RUSAK </span></td>
														<?php else:?>	
														<td><span class="label label-info">DUMP</span></td>
														<?php endif;?>
														<td>
															<center>
															<div class="hidden-sm hidden-xs action-buttons">																				
																																					
																<a class="red" data-toggle="modal" data-target="#modal_edit<?php echo $assign_id;?>">
																	<i class="ace-icon fa fa-wrench bigger-105"></i>
																</a>										
															</div>
															</center>
														</td>	
													</tr>
													<?php endforeach;?>
												</tbody>
											</table>
										</div>
									</div>
								</div>	
						</div>
					</div>
				</div>
			</div>

			<?php $this->load->view("layout/footer.php") ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>
		
		<div class="modal fade" id="modal_assign<?php echo $assign_id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Data</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'user/transfer4/simpan'?>">
                <div class="modal-body"> 
                    
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Nama Alat</label>
                        <div class="col-xs-6">
                            <input name="alat_nama" class="form-control" id="alat_nama"  placeholder="" required>
                        </div>
                    </div>
					<div class="form-group">
					<label class="col-sm-3 col-form-label">Kategori</label>
						<div class="col-xs-6">
							<select name="kategori_nama" id="kategori_nama" class="form-control">
							<option value="0"></option>
								<?php foreach($kategori->result() as $row):?>
									<option value="<?php echo $row->kategori_nama;?>"> <?php echo $row->kategori_nama;?> </option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Merek</label>
                        <div class="col-xs-6">
                            <input name="merek" class="form-control" id="merek"  placeholder="" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Model</label>
                        <div class="col-xs-6">
                            <input name="model" class="form-control" id="model"  placeholder="" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Nomor Seri</label>
                        <div class="col-xs-6">
                            <input name="serial" class="form-control" id="serial"  placeholder="" required>
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Spesifikasi</label>
                        <div class="col-xs-6">
							<textarea name="deskripsi" class="form-control" id="deskripsi" rows="3"></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 col-form-label" >Jumlah</label>
                        <div class="col-xs-6">
                            <input name="jumlah" class="form-control" id="jumlah" placeholder="" required>
                        </div>
                    </div>
					<div class="form-group">
					<label class="col-sm-3 col-form-label">ukuran</label>
						<div class="col-xs-6">
							<select name="ukuran" id="ukuran" class="form-control">
							<option value="0"></option>
								<?php foreach($ukuran1->result() as $row):?>
									<option value="<?php echo $row->ukuran_nama;?>"><?php echo $row->ukuran_nama;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
					<label class="col-sm-3 col-form-label">Status</label>
						<div class="col-xs-6">
							<select name="status_nama" id="status_nama" class="form-control">
							<option value="0"></option>
								<?php foreach($status->result() as $row):?>
									<option value="<?php echo $row->status_nama;?>"><?php echo $row->status_nama;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
                </div>			
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
		
		
		<?php foreach ($data->result_array() as $i) :
													   $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];                       
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
													   $suplier_nama=$i['suplier_nama'];
            ?>
			
			
			
			
			<div class="modal fade" id="modal_edit<?php echo $assign_id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">FORM PERBAIKAN ALAT</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/perbaikan1/perbaikan_alat'?>">
                <div class="modal-body">
 
                   
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Nama Alat</label>
                        <div class="col-xs-6">
							<input type="hidden" name="assign_id" value="<?php echo $assign_id;?>">
                            <input type="text" name="alat_nama" class="form-control " id="alat_nama" value="<?php echo $alat_nama;?>"  readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Kategori</label>
                        <div class="col-xs-6">
                            <input type="text" name="kategori_nama" class="form-control " id="kategori_nama" value="<?php echo $kategori_nama;?>" readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Merek/Manufaktur</label>
                        <div class="col-xs-6">
                            <input type="text" name="merek" class="form-control " id="merek" value="<?php echo $merek;?>"  readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Model</label>
                        <div class="col-xs-6">
                            <input type="text" name="model" class="form-control " id="model" value="<?php echo $model;?>"  readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Serial</label>
                        <div class="col-xs-6">
                            <input type="text" name="serial" class="form-control " id="serial" value="<?php echo $serial;?>"  readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Spesifikasi</label>
                        <div class="col-xs-6">
                            <input type="text" name="deskripsi" class="form-control " id="deskripsi" value="<?php echo $deskripsi;?>"  readonly>
                            <input type="hidden" name="jumlah" value="<?php echo $jumlah;?>">
                            <input type="hidden" name="ukuran" value="<?php echo $ukuran;?>">
						</div>
                    </div>
					<!--<div class="form-group">
                        <label class="control-label col-xs-4" >Jumlah</label>
                        <div class="col-xs-6">
                            <input type="text" name="jumlah" class="form-control " id="jumlah" value="<?php echo $jumlah;?>"  readonly>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Ukuran</label>
                        <div class="col-xs-6">
                            <input type="text" name="ukuran" class="form-control " id="ukuran" value="<?php echo $ukuran;?>"  readonly>
						</div>
                    </div>-->
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Nama User</label>
                        <div class="col-xs-6">
                            <input type="text" name="karyawan_nama" class="form-control " id="karyawan_nama" value="<?php echo $karyawan_nama;?>"  readonly>
						</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Lokasi</label>
                        <div class="col-xs-6">
                            <input type="text" name="lokasi_nama" class="form-control " id="lokasi_nama" value="<?php echo $lokasi_nama;?>"  readonly>
						</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Divisi</label>
                        <div class="col-xs-6">
                            <input type="text" name="divisi_nama" class="form-control " id="divisi_nama" value="<?php echo $divisi_nama;?>"  readonly>
						</div>
                    </div>
                    <div class="form-group row">
					<label for="inputUserName" class="col-sm-4 control-label">Vendor/Suplier</label>
						<div class="col-xs-6">
							<select multiple="" name="suplier_nama" id="suplier_nama" class="select2" required>
								<?php foreach($suplier1->result() as $row):?>
									<option value="<?php echo $row->suplier_nama;?>"><?php echo $row->suplier_nama;?> (<?php echo $row->alamat_nama;?>)</option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="form-group row">
                                        <label for="inputUserName" class="col-sm-4 control-label">Status</label>
                                        <div class="col-xs-6">
												<select multiple="" name="status_nama" id="status_nama" class="select2" required>
													
													<option value="PERBAIKAN">PERBAIKAN</option>	
												</select>
											</div>
                    </div>
                </div>
 
                <div class="modal-footer">
                    
                    <button class="btn btn-success">Kirim</button>
                </div>
            </form>
            </div>
            </div>
        </div>
			
			 <?php endforeach;?>
			<?php foreach ($data->result_array() as $i) :
													   $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];                       
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
            ?>
			
			<div class="modal fade" id="modal_detail<?php echo $assign_id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">DETAIL ALAT </h3>
            </div>
            <form class="form-horizontal" method="post" action="#">
                <div class="modal-body">
 
                    
 
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Nama Alat :</label>
                        <div class="col-xs-6">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $alat_nama;?></span></label>
						</div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label col-xs-4" >Kategori :</label>
                        <div class="col-xs-6">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $kategori_nama;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Merek :</label>
                        <div class="col-xs-6">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $merek;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Model :</label>
                        <div class="col-xs-6">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $model;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Serial :</label>
                        <div class="col-xs-6">
                           <label class="control-label col-xs-5" ><span align="justify"><?php echo $serial;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Spesifikasi :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $deskripsi;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Jumlah :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $jumlah;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Ukuran :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $ukuran;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >User Assign :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify" class="label label-warning"><b><?php echo $karyawan_nama;?></b></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Lokasi :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $lokasi_nama;?></span></label>
						</div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-4" >Divisi :</label>
                        <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $divisi_nama;?></span></label>
						</div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label col-xs-4" >Status Alat :</label>
                       <div class="col-xs-8">
                            <label class="control-label col-xs-5" ><span align="justify"><?php echo $status_nama;?></span></label>
						</div>
                    </div>
					
				
                </div>
 
                <div class="modal-footer">
                    
                    <button class="btn btn-success"><i class="ace-icon fa fa-print bigger-105"></i>Cetak</button>
                </div>
            </form>
            </div>
            </div>
        </div>
 
    <?php endforeach;?>
			
		<?php foreach ($data->result_array() as $i) :
                                                       $assign_id=$i['assign_id'];
													   $alat_nama=$i['alat_nama'];
													   $kategori_nama=$i['kategori_nama'];
													   $merek=$i['merek'];
													   $model=$i['model'];
													   $serial=$i['serial'];
													   $karyawan_nama=$i['karyawan_nama'];
													   $lokasi_nama=$i['lokasi_nama'];
													   $divisi_nama=$i['divisi_nama'];
													   $deskripsi=$i['deskripsi'];                       
													   $jumlah=$i['jumlah'];
													   $ukuran=$i['ukuran'];
													   $status_nama=$i['status_nama'];
            ?>
		<!-- ============ MODAL HAPUS =============== -->
        <div class="modal fade" id="modal_hapus<?php echo $assign_id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Hapus Data</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'user/transfer4/hapus_assign'?>">
                <div class="modal-body">
				<input type="hidden" name="assign_id" value="<?php echo $assign_id;?>"/>
                    <p>Anda yakin mau menghapus Alat<b> <?php echo $alat_nama;?> </b> ? </p>
                </div>
                <div class="modal-footer">
                   
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    
		<?php endforeach;?>
		

		
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-2.1.4.min.js'?>"></script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url().'assets/ace/assets/js/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.dataTables.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.dataTables.bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/dataTables.buttons.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/buttons.flash.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/buttons.html5.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/buttons.print.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/buttons.colVis.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/dataTables.select.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace-elements.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/ace.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
		
		
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.bootstrap-duallistbox.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery.raty.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/bootstrap-multiselect.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/select2.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/ace/assets/js/jquery-typeahead.js'?>"></script>
		
		
		
		<script type="text/javascript">
			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null,
					  { "bSortable": false }
					],
					"aaSorting": [],
					
					
					//"bProcessing": true,
			        //"bServerSide": true,
			        //"sAjaxSource": "http://127.0.0.1/table.php"	,
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			
			
					select: {
						style: 'multi'
					}
			    } );
			
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					  //{
						///"extend": "colvis",
						//"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Tampil/Sembunyi Kolom</span>",
						//"className": "btn btn-white btn-primary btn-bold"
						//columns: ':not(:first):not(:last)'
					 // },
					 // {
						//"extend": "copy",
						//"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						//"className": "btn btn-white btn-primary btn-bold"
					  //},
					  {
						"extend": "csv",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					 // {
						//"extend": "excel",
						//"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						//"className": "btn btn-white btn-primary btn-bold"
					 // },
					 // {
						//"extend": "pdf",
						//"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						//"className": "btn btn-white btn-primary btn-bold"
					  //},
					  {
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 blue'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'This print was produced using the Print button for DataTables'
					  }		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				//style the message box
				var defaultCopyAction = myTable.button(1).action();
				myTable.button(1).action(function (e, dt, button, config) {
					defaultCopyAction(e, dt, button, config);
					$('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
				});
				
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});
			
				////
			
				setTimeout(function() {
					$($('.tableTools-container')).find('a.dt-button').each(function() {
						var div = $(this).find(' > div').first();
						if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
						else $(this).tooltip({container: 'body', title: $(this).text()});
					});
				}, 500);
				
				
				
				
				
				myTable.on( 'select', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
				myTable.on( 'deselect', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
			
			
			
			
				/////////////////////////////////
				//table checkboxes
				$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
				
				//select/deselect all rows according to table header checkbox
				$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$('#dynamic-table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) myTable.row(row).select();
						else  myTable.row(row).deselect();
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var row = $(this).closest('tr').get(0);
					if(this.checked) myTable.row(row).deselect();
					else myTable.row(row).select();
				});
			
			
			
				$(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
					e.stopImmediatePropagation();
					e.stopPropagation();
					e.preventDefault();
				});
				
				
				
				//And for the first simple table, which doesn't have TableTools or dataTables
				//select/deselect all rows according to table header checkbox
				var active_class = 'active';
				$('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$(this).closest('table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
						else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#simple-table').on('click', 'td input[type=checkbox]' , function(){
					var $row = $(this).closest('tr');
					if($row.is('.detail-row ')) return;
					if(this.checked) $row.addClass(active_class);
					else $row.removeClass(active_class);
				});
			
				
			
				/********************************/
				//add tooltip for small view action buttons in dropdown menu
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				
				//tooltip placement on right or left
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
				
				
				
				
				/***************/
				$('.show-details-btn').on('click', function(e) {
					e.preventDefault();
					$(this).closest('tr').next().toggleClass('open');
					$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
				});
				/***************/
				
				
				
				
				
				/**
				//add horizontal scrollbars to a simple table
				$('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
				  {
					horizontal: true,
					styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
					size: 2000,
					mouseWheelLock: true
				  }
				).css('padding-top', '12px');
				*/
			
			
			})
		</script>
		
		
				<script type="text/javascript">
			jQuery(function($){
			    var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
				var container1 = demo1.bootstrapDualListbox('getContainer');
				container1.find('.btn').addClass('btn-white btn-info btn-bold');
			
				/**var setRatingColors = function() {
					$(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
					$(this).find('.star-off-png').removeClass('orange2').addClass('grey');
				}*/
				$('.rating').raty({
					'cancel' : true,
					'half': true,
					'starType' : 'i'
					/**,
					
					'click': function() {
						setRatingColors.call(this);
					},
					'mouseover': function() {
						setRatingColors.call(this);
					},
					'mouseout': function() {
						setRatingColors.call(this);
					}*/
				})//.find('i:not(.star-raty)').addClass('grey');
				
				
				
				//////////////////
				//select2
				$('.select2').css('width','270px').select2({allowClear:true})
				$('#select2-multiple-style .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('.select2').addClass('tag-input-style');
					 else $('.select2').removeClass('tag-input-style');
				});
				
				//////////////////
				$('.multiselect').multiselect({
				 enableFiltering: true,
				 enableHTML: true,
				 buttonClass: 'btn btn-white btn-primary',
				 templates: {
					button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
					ul: '<ul class="multiselect-container dropdown-menu"></ul>',
					filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
					filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
					li: '<li><a tabindex="0"><label></label></a></li>',
			        divider: '<li class="multiselect-item divider"></li>',
			        liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
				 }
				});
			
				
				///////////////////
					
				//typeahead.js
				//example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
				var substringMatcher = function(strs) {
					return function findMatches(q, cb) {
						var matches, substringRegex;
					 
						// an array that will be populated with substring matches
						matches = [];
					 
						// regex used to determine if a string contains the substring `q`
						substrRegex = new RegExp(q, 'i');
					 
						// iterate through the pool of strings and for any string that
						// contains the substring `q`, add it to the `matches` array
						$.each(strs, function(i, str) {
							if (substrRegex.test(str)) {
								// the typeahead jQuery plugin expects suggestions to a
								// JavaScript object, refer to typeahead docs for more info
								matches.push({ value: str });
							}
						});
			
						cb(matches);
					}
				 }
			
				 $('input.typeahead').typeahead({
					hint: true,
					highlight: true,
					minLength: 1
				 }, {
					name: 'states',
					displayKey: 'value',
					source: substringMatcher(ace.vars['US_STATES']),
					limit: 10
				 });
					
					
				///////////////
				
				
				//in ajax mode, remove remaining elements before leaving page
				$(document).one('ajaxloadstart.page', function(e) {
					$('[class*=select2]').remove();
					$('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
					$('.rating').raty('destroy');
					$('.multiselect').multiselect('destroy');
				});
			
			});
		</script>
		
		
		
		
		
		
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>
    
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Data Berhasil diAssign Ke User Terpilih.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Berhasil Transfer Ke User Terpilih",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Data Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#DC143C'
                });
        </script>
    <?php else:?>

    <?php endif;?>
	</body>
</html>
