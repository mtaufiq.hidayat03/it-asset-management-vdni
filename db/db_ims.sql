-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2021 at 08:57 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ims`
--

-- --------------------------------------------------------

--
-- Table structure for table `rat`
--

CREATE TABLE `rat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rat`
--

INSERT INTO `rat` (`id`, `user_id`, `date_time`, `code`, `message`) VALUES
(1, 1, '2020-02-24 06:42:02', 0, 'This is a message that will be logged'),
(2, 1, '2020-02-24 06:42:58', 5, 'This is a message that will be logged');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alat`
--

CREATE TABLE `tbl_alat` (
  `alat_id` int(11) NOT NULL,
  `nama_alat` varchar(100) NOT NULL,
  `kategori_nama` varchar(20) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `divisi_nama` varchar(100) NOT NULL,
  `lokasi_nama` varchar(100) NOT NULL,
  `status_alat` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `stok_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_alat`
--

INSERT INTO `tbl_alat` (`alat_id`, `nama_alat`, `kategori_nama`, `nama_user`, `divisi_nama`, `lokasi_nama`, `status_alat`, `foto`, `deskripsi`, `stok_id`) VALUES
(1, 'VOIP Phone', 'Elektronik', 'Ardhi Fahrizal', 'IT', 'JAKARTA', 'NEW', '', '', 1),
(2, 'Monitor LCD / LED', 'Elektronik', 'Ardhi Fahrizal', 'IT', 'JAKARTA', 'NEW', '', '', 0),
(3, 'Notebook\r\n', 'Elektronik', 'Ardhi Fahrizal', 'IT', 'JAKARTA', 'NEW', '79800229be89f5c9b4d6c168850dd7ed.jpg', 'Intel core i3 & AMD Radeon', 0),
(4, 'Notebook\r\n', 'Elektronik', 'Rachmat Akbar', 'EXIM', 'JAKARTA', 'NEW', 'd4a6ebb53ad8e4dd28f0fd424c7559cd.png', 'RAM 8Gb', 0),
(5, 'VOIP Phone\r\n', 'Elektronik', 'Rachmat Akbar', 'EXIM', 'JAKARTA', 'NEW', '876c4cdc2d32670524bfaa4071688556.png', 'Telepon VOIP, 132 x 64-pixel graphical', 0),
(6, 'Router Wireless\r\n', 'Elektronik', 'FAHMI SAFAAT', 'IT', 'MOROSI', 'NEW', 'b2025ed8741781e7313f3001fe05fc60.png', 'Stok jakarta pindah ke morosi office', 0),
(7, 'TP Link Wireless N Router\r\n', 'Elektronik', 'Gudang IT Morosi', 'IT', 'MOROSI', 'NEW', 'ac6bae1187782d5b65bad840a2112963.png', 'Wireless ada di gudang IT', 0),
(8, 'Notebook\r\n', 'Elektronik', 'FAHMI SAFAAT', 'IT', 'MOROSI', 'USED', '253d943c8700c594e6e4e525b41a7f9d.png', 'Laptop, Ram 4gb, HD 500gb', 0),
(9, 'Printer\r\n', 'Elektronik', 'Gudang IT Morosi', 'IT', 'MOROSI', 'DAMAGE', '0a95b718172e94c6a6a3e15a5d970ae8.png', '\"Printer & Copy A3  Kerusakan : Katrik hitam\"', 0),
(10, 'Printer\r\n', 'Elektronik', 'Ryan Subhan Pratama', 'EXIM', 'MOROSI', 'USED', '4d9a7bc6d834d1058d5a518e57a77890.png', 'Printer, Copy &amp; Scan A4', 0),
(11, 'Printer\r\n', 'Elektronik', 'Gudang IT Morosi', 'IT', 'MOROSI', 'REPAIR', '06d3c1ce587dd0e0e52b70dc93ee4a44.png', '\"Printer Canon IP2770 - F4&nbsp;  Kerusakan : Katrik Hitam &amp; Warna\"', 0),
(12, 'Printer\r\n', 'Elektronik', '	Lely Afzadia Alfarisa', 'PURCHASING', 'MOROSI', 'USED', '87a672cf67d83e4a706cbad996ae8346.png', 'Printer Epson Print, Copy &amp; Scan A4', 0),
(13, 'Notebook\r\n', 'Elektronik', '	Lely Afzadia Alfarisa', 'PURCHASING', 'MOROSI', 'USED', 'f5154da13e4b482cf837d3e55738db15.png', 'Lapto Asus, HD 500GB, RAM 2GB', 0),
(14, 'Notebook\r\n', 'Elektronik', 'Rizal Hidayat', 'EXIM', 'OSS', 'USED', '7adbe16dd853eb5518cc2935cc1f12fb.png', '\"Laptop Asus HD 500GB, RAM 2GB  Dipakai oleh Staff Exim OSS\"', 0),
(15, 'Printer\r\n', 'Elektronik', 'Indriyanti Sabudin', 'LOGISTIK', 'MOROSI', 'USED', '4d8bf1ce6d3cf37f7c03cb0185035bad.png', 'Printer Epson Print, Copy &amp; Scan A4', 0),
(16, 'Printer\r\n', 'Elektronik', 'Aztrimaisem', 'LOGISTIK', 'MOROSI', 'USED', 'be4b8a04e82a80d07e707902f6f74e91.png', '\"Printer Epson Print, Copy &amp; Scan A4  Dipakai oleh Admin DBM OSS\"', 0),
(17, 'Printer\r\n', 'Elektronik', 'Hasmayanti', 'HRD', 'MOROSI', 'DAMAGE', 'a136818378f4e224f19b5b701a79b799.png', 'Printer HP BW - F4, kerusakan fuser film', 0),
(18, 'Printer\r\n', 'Elektronik', 'Mardhan Nashar', 'HRD', 'MOROSI', 'USED', 'df6b727d8490e15d8d4ad8a30d3986bc.png', 'Printer Canon All In One - F4', 0),
(19, 'Printer\r\n', 'Elektronik', 'Reski Astuti', 'HRD', 'MOROSI', 'DAMAGE', '602ffe10d72786df4ca0bff4fe4ecd17.png', 'Printer Epson Copy, Print &amp; Scan A4', 0),
(20, 'Printer\r\n', 'Elektronik', 'Reski Astuti', 'HRD', 'MOROSI', 'USED', '52d3bf003194413ef1b7944bba7465f4.png', 'Printer Epson Dot Mariks', 0),
(21, 'Printer\r\n', 'Elektronik', 'Muhamad Awan', 'HRD', 'MOROSI', 'DAMAGE', 'a4a6f15d2ef7739745e5497535af8c5c.png', 'Printer HP Lasejet B/W - F4 (Kerusakan Mekanik - Karet Roller Hangus)', 0),
(22, 'Notebook\r\n', 'Elektronik', 'Reski Astuti', 'HRD', 'MOROSI', 'USED', 'b623cb688b13da809aeb606619d13a72.png', 'Laptop Asus HD 1TB, RAM 4GB', 0),
(23, 'Printer\r\n', 'Elektronik', 'Reski Astuti', 'HRD', 'MOROSI', 'USED', '0bf0b4e7e7ec08afd32f060ff5403515.png', 'Printer Epson Dot Matriks', 0),
(24, 'Notebook\r\n', 'Elektronik', 'Fitriani Rahman', 'HRD', 'MOROSI', 'USED', 'f0000557e0ca20b339cf379fa74f1d7d.png', 'Laptop Acer HD 500GB, RAM 4GB', 0),
(25, 'Notebook\r\n', 'Elektronik', 'Gudang IT Morosi', 'IT', 'MOROSI', 'USED', 'b93028d01fb7d10a0e4333e3d7d4064c.png', '\"Laptop Asus HD 500GB, RAM 4GB  Dipakai oleh Admin DBM OSS\"', 0),
(26, 'Notebook\r\n', 'Elektronik', 'Herianto', 'HRD', 'MOROSI', 'USED', '6b52f3d4f55e04b3e2b2e8e9330e96cd.png', 'Laptop Asus HD 500GB, RAM 2GB', 0),
(27, 'Notebook\r\n', 'Elektronik', 'Muhamad Awan', 'HRD', 'MOROSI', 'USED', 'fa7f949e0c2ef6a4a7bc6676661b94f9.png', 'Laptop Acer HD 500GB, RAM 2GB', 0),
(28, 'Notebook\r\n', 'Elektronik', 'Indriyanti Sabudin', 'LOGISTIK', 'MOROSI', 'USED', 'fb9240f3027d3fd83c4aad5ea2125c4c.png', 'Laptop Asus HD 500GB, RAM 4GB', 0),
(29, 'Notebook\r\n', 'Elektronik', 'Ryan Subhan Pratama', 'EXIM', 'MOROSI', 'USED', '3689fca3df7ccfad2d786c1d321c54c3.png', 'Laptop Axioo HD 500GB, RAM 2GB', 0),
(30, 'Komputer\r\n', 'Elektronik', 'Gudang IT Morosi', 'GA', 'MOROSI', 'USED', '604994b53a8c9618a6ba7ee5ddece5db.png', 'Komputer PC Axioo', 0),
(31, 'Komputer\r\n', 'Elektronik', 'Muhamad Awan', 'HRD', 'MOROSI', 'USED', 'f1c7e2d33b0c5ec8d362dbd203f8db75.png', '\"Komputer PC DELL (Built Up)  Keyboard + Mouse  HDD 1 TB, RAM 4 GB\"', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alat2`
--

CREATE TABLE `tbl_alat2` (
  `alat_id` int(11) NOT NULL,
  `alat_nama` varchar(250) NOT NULL,
  `kategori_nama` varchar(250) NOT NULL,
  `merek` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `serial` varchar(200) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `ukuran` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_alat2`
--

INSERT INTO `tbl_alat2` (`alat_id`, `alat_nama`, `kategori_nama`, `merek`, `model`, `serial`, `deskripsi`, `jumlah`, `ukuran`, `status`, `foto`) VALUES
(1, 'PRINTER EPSON', 'HARDWARE', 'EPSON', 'L3110', 'CVFNMKOMPN', 'SCANER DAN FOTOCOPY', '1', 'UNIT', 'BARU', ''),
(2, 'CPU LENOVO', 'HARDWARE', 'LENOVO', 'LNV123', 'HKNMVBK67KHY', 'LAN & WIFI ADAPTOR,RAM 8 Gb,HDD 1 T', '1', 'UNIT', 'BARU', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assign`
--

CREATE TABLE `tbl_assign` (
  `assign_id` int(11) NOT NULL,
  `alat_nama` varchar(250) NOT NULL,
  `kategori_nama` varchar(250) NOT NULL,
  `merek` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `serial` varchar(200) NOT NULL,
  `karyawan_nama` varchar(200) NOT NULL,
  `lokasi_nama` varchar(200) NOT NULL,
  `divisi_nama` varchar(200) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `jumlah` varchar(200) NOT NULL,
  `ukuran` varchar(200) NOT NULL,
  `status_nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_assign`
--

INSERT INTO `tbl_assign` (`assign_id`, `alat_nama`, `kategori_nama`, `merek`, `model`, `serial`, `karyawan_nama`, `lokasi_nama`, `divisi_nama`, `deskripsi`, `jumlah`, `ukuran`, `status_nama`) VALUES
(7, 'PC LENOVO', 'HARDWARE', 'LENOVO', 'LNV320', 'DF234CVG', '', '', '', 'RAM 8 Gb,HDD 1 T', '1', 'Unit', 'BARU'),
(9, 'PRINTER EPSON', 'HARDWARE', 'epson', 'l360', 'ggggg34RTER', '', '', '', 'wifi scanner 3 dimensi', '1', 'Unit', 'BARU'),
(11, 'KABEL CAT6 LAN', 'HARDWARE', 'BELDEN', 'CAT6', 'CFTR67YUH', '', '', '', 'CAT6 PANJANG 305 M', '1', 'Rol', 'BARU'),
(13, 'LAYAR MONITOR', 'HARDWARE', 'NOC', 'NOCVGHJ', 'nocvg6t567yhb', '', '', '', 'layar 14 Inchi', '1', 'Unit', 'BARU'),
(15, 'KAMERA WEB', 'HARDWARE', 'LOGITAC', 'LGT320', 'LGTNG756GJ', '', '', '', 'WEB CAM WIFI ADAPTOR', '1', 'Unit', 'Baru'),
(17, 'PC HP ALL IN ONE', 'HARDWARE', 'HP', 'C21', 'CVGH7YBHY89M', '', '', '', 'RAM 4Gb HDD 1T', '1', 'Unit', 'Baru'),
(18, 'KEYBOARD WIRELESS', 'PERIPHERAL', 'LOGITEC', 'LGTC5647', 'LGTVHF56TYG', '', '', '', 'WIRELESS ', '1', 'Unit', 'Baru');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assign2`
--

CREATE TABLE `tbl_assign2` (
  `assign_id` int(11) NOT NULL,
  `alat_nama` varchar(200) NOT NULL,
  `kategori_nama` varchar(200) NOT NULL,
  `merek` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `serial` varchar(200) NOT NULL,
  `karyawan_nama` varchar(200) NOT NULL,
  `lokasi_nama` varchar(200) NOT NULL,
  `divisi_nama` varchar(200) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `jumlah` varchar(200) NOT NULL,
  `ukuran` varchar(200) NOT NULL,
  `status_nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_assign2`
--

INSERT INTO `tbl_assign2` (`assign_id`, `alat_nama`, `kategori_nama`, `merek`, `model`, `serial`, `karyawan_nama`, `lokasi_nama`, `divisi_nama`, `deskripsi`, `jumlah`, `ukuran`, `status_nama`) VALUES
(1, 'PC LENOVO', 'HARDWARE', 'LENOVO', 'LNV320', 'DF234CVG', 'Ardhi Fahrizal', 'JAKARTA', 'SECURITY', 'RAM 8 Gb,HDD 1 T', '1', 'Unit', 'TERPAKAI'),
(2, 'KABEL CAT6 LAN', 'HARDWARE', 'BELDEN', 'CAT6', 'CFTR67YUH', 'JOKO MALIK', 'MOROSI', 'IT', 'CAT6 PANJANG 305 M', '1', 'Rol', 'TERPAKAI'),
(7, 'KAMERA WEB', 'HARDWARE', 'LOGITAC', 'LGT320', 'LGTNG756GJ', 'Julius Puang', 'MOROSI', 'GA', 'WEB CAM WIFI ADAPTOR', '1', 'Unit', 'TERPAKAI'),
(8, 'KEYBOARD WIRELESS', 'PERIPHERAL', 'LOGITEC', 'LGTC5647', 'LGTVHF56TYG', 'Iswanto Hamdah', 'JAKARTA', 'IT', 'WIRELESS ', '1', 'Unit', 'TERPAKAI'),
(9, 'PC HP ALL IN ONE', 'HARDWARE', 'HP', 'C21', 'CVGH7YBHY89M', 'Julius Puang', 'MOROSI', 'PURCHASING', 'RAM 4Gb HDD 1T', '1', 'Unit', 'TERPAKAI'),
(10, 'LAYAR MONITOR', 'HARDWARE', 'NOC', 'NOCVGHJ', 'nocvg6t567yhb', 'Iswanto Hamdah', 'JAKARTA', 'FINANCE', 'layar 14 Inchi', '1', 'Unit', 'TERPAKAI'),
(11, 'PRINTER EPSON', 'HARDWARE', 'epson', 'l360', 'ggggg34RTER', 'Ardhi Fahrizal', 'KENDARI', 'LOGISTIK', 'wifi scanner 3 dimensi', '1', 'Unit', 'TERPAKAI');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_device`
--

CREATE TABLE `tbl_device` (
  `dev_id` int(11) NOT NULL,
  `dev_nama` varchar(100) NOT NULL,
  `kategori_nama` varchar(100) NOT NULL,
  `dev_ket` varchar(100) NOT NULL,
  `dev_serial` varchar(100) NOT NULL,
  `dev_merek` varchar(100) NOT NULL,
  `dev_model` varchar(100) NOT NULL,
  `dev_tipe` varchar(100) NOT NULL,
  `aset_nomor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_device`
--

INSERT INTO `tbl_device` (`dev_id`, `dev_nama`, `kategori_nama`, `dev_ket`, `dev_serial`, `dev_merek`, `dev_model`, `dev_tipe`, `aset_nomor`) VALUES
(1, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '4121115060012848', 'Yealink', 'SIP-T21P E2', '', ''),
(2, 'Monitor LCD / LED', 'Elektronik', 'Monitor LG 20', '604NTPC45288', 'LG', '20MP48A', '', ''),
(3, 'Notebook\r\n', 'Elektronik\r\n', 'Intel core i3  AMD Radeon\r\n', '5CG70311hr\r\n', 'HP\r\n', 'HP Notebook\r\n', '', ''),
(4, 'Notebook\r\n', 'Elektronik\r\n', 'RAM 8Gb\r\n', '5CG6523LY6\r\n', 'HP\r\n', 'HP Notebok\r\n', '', ''),
(5, 'VOIP Phone\r\n', 'Elektronik\r\n', 'Telepon VOIP, 132 x 64-pixel graphical\r\n', '4121115060012839\r\n', 'Yealink\r\n', 'SIP-T21P E2\r\n', '\r\n', ''),
(6, 'Router Wireless\r\n', 'Elektronik\r\n', 'Stok jakarta pindah ke morosi office\r\n', '2167507004179\r\n', 'TP Link\r\n', '300 mbps wireless router\r\n', '', ''),
(7, 'TP Link Wireless N Router\r\n', 'Elektronik\r\n', 'Wireless ada di gudang IT\r\n', '2167696002287\r\n', 'TP link\r\n', 'Wireless Router\r\n', '', ''),
(8, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop, Ram 4gb, HD 500gb\r\n', 'GAN0CV10471041A\r\n', 'Asus\r\n', 'A455L\r\n', '', ''),
(9, 'Router Mikrotik\r\n', 'Elektronik\r\n', 'Kondisi : Rusak Kena Petir\r\n', '250416-AINOS-Q0084\r\n', 'Mikrobit Ainos\r\n', 'Mikrotik\r\n', '', ''),
(10, 'Printer\r\n', 'Elektronik\r\n', '\"Printer & Copy A3\r\n\r\nKerusakan : Katrik hitam\"\r\n', 'E76998E7F145504\r\n', 'Brother\r\n', 'MFC-J3530DW\r\n', '', ''),
(11, 'Printer\r\n', 'Elektronik\r\n', 'Printer Copy  Scan A4\r\n', 'X3GW505649\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(12, 'Printer\r\n', 'Elektronik\r\n', '\"Printer Canon IP2770 - F4&nbsp;\r\n\r\nKerusakan : Katrik Hitam & Warna\r\n', 'QC3-5062\r\n', 'Canon\r\n', 'Canon IP2770\r\n', '', ''),
(13, 'Printer\r\n', 'Elektronik\r\n', '\"Printer Canon IP2770 - F4&nbsp;\r\n\r\nKerusakan : Katrik Hitam &amp; Warna\"\r\n', 'X3GW302695\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(14, 'Notebook\r\n', 'Elektronik', 'Lapto Asus, HD 500GB, RAM 2GB\r\n', 'H3N0WU185811136\r\n', 'Laptop Asus\r\n', 'Asus X441N\r\n', '', ''),
(15, 'Notebook\r\n', 'Elektronik', '\"Laptop Asus HD 500GB, RAM 2GB\r\n\r\nDipakai oleh Staff Exim OSS\"\r\n', 'H2N0WU113712088\r\n', 'Asus\r\n', 'Asus X441S\r\n', '', ''),
(16, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Print, Copy &amp; Scan A4\r\n', 'X3GW471413\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(17, 'Printer\r\n', 'Elektronik\r\n', '\"Printer Epson Print, Copy &amp; Scan A4\r\n\r\nDipakai oleh Admin DBM OSS\"\r\n', 'X3GW476203\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(18, 'Printer\r\n', 'Elektronik\r\n', 'Printer HP BW - F4, kerusakan fuser film\r\n', 'VNF3R61361\r\n', 'HP\r\n', 'HP Laserjet P1102\r\n', '', ''),
(19, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon All In One - F4\r\n', '34907\r\n', 'Canon\r\n', 'MP287\r\n', '', ''),
(20, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Copy, Print &amp; Scan A4\r\n', 'X3GW518926\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(21, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Dot Mariks\r\n', 'Q7FY342781\r\n', 'Epson\r\n', 'Epson LX-310\r\n', '', ''),
(22, 'Printer\r\n', 'Elektronik\r\n', 'Printer HP Lasejet B/W - F4 (Kerusakan Mekanik - Karet Roller Hangus)\r\n', 'VNF8Q20596\r\n', 'HP\r\n', 'HP Laserjet P1102\r\n', '', ''),
(23, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus HD 1TB, RAM 4GB\r\n', 'HBN0CV036435456\r\n', 'Asus\r\n', 'Asus X441U\r\n', '', ''),
(24, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Dot Matriks\r\n', 'Q7FY342508\r\n', 'Epson\r\n', 'Epson LX-310\r\n', '', ''),
(25, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Acer HD 500GB, RAM 4GB\r\n', 'NXG1F5N0076110412A3400\r\n', 'Acer\r\n', 'Acer Aspire ES-14\r\n', '', ''),
(26, 'Notebook\r\n', 'Elektronik\r\n', '\"Laptop Asus HD 500GB, RAM 4GB\r\n\r\nDipakai oleh Admin DBM OSS\"\r\n', 'HAN0CV212168444\r\n', 'Asus\r\n', 'X441U\r\n', '', ''),
(27, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus HD 500GB, RAM 2GB\r\n', 'F2N0WU13381207D\r\n', 'Asus\r\n', 'Asus X454W\r\n', '', ''),
(28, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Acer HD 500GB, RAM 2GB\r\n', 'NXMZCSN00254604D4B7600\r\n', 'Acer\r\n', 'Acer Aspire ES-14\r\n', '', ''),
(29, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus HD 500GB, RAM 4GB\r\n', 'HAN0CV12U091427\r\n', 'Asus\r\n', 'Asus X441U\r\n', '', ''),
(30, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Axioo HD 500GB, RAM 2GB\r\n', '00217030041485041\r\n', 'Axioo\r\n', 'Axioo Neon\r\n', '', ''),
(31, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC Axioo\r\n', '00216510041295014\r\n', 'Axioo\r\n', 'Axioo MyPC\r\n', '', ''),
(32, 'Printer\r\n', 'Elektronik\r\n', 'Printer Wireless Epson Copy, Print &amp; Scan A4\r\n', '\r\n', 'Epson\r\n', 'Epson L365\r\n', '', ''),
(33, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop HP, HD 1,5TB, RAM 8GB\r\n', '5CG82252QP\r\n', 'HP\r\n', 'HP 145-CF0XXX\r\n', '', ''),
(34, 'Komputer\r\n', 'Elektronik\r\n', '\"Komputer PC DELL (Built Up)\r\n\r\nKeyboard + Mouse\r\n\r\nHDD 1 TB, RAM 4 GB\"\r\n', '59ZP6Q2\r\n', 'DELL\r\n', 'Dell Vostro 3670\r\n', '', ''),
(35, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon Scan, Copy &amp; Print&nbsp;\r\n', 'QC4-29792\r\n', 'Canon\r\n', 'Canon MP237\r\n', '', ''),
(36, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Scan, Copy &amp; Print\r\n', 'X3GW518946\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(37, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon, Scan, Copy &amp; Print\r\n', 'QC3-X018\r\n\r\n', 'Canon\r\n', 'Canon MP287\r\n', '', ''),
(38, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 1 TB, RAM 4 GB\r\n', 'J3N0CV04M386111\r\n', 'Asus\r\n', 'Asus X441U\r\n', '', ''),
(39, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Dell, HDD 500 GB, RAM 2 GB\r\n', '9NST4Z1\r\n', 'DELL\r\n', 'Dell 3442\r\n', '', ''),
(40, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'Monitor DELL 19.5&quot;, Resolusi (1600*900)&nbsp;\r\n', 'CN-078390-73431-8310405-A04\r\n', 'DELL\r\n', 'DELL E2016HV\r\n', '', ''),
(41, 'Komputer\r\n', 'Elektronik\r\n', '\"Komputer PC Dell (Built Up)\r\n\r\nKeyboar + Mouse&nbsp;\r\n\r\nHDD 1 TB, RAM 4 GB\"\r\n', '549G6Q2\r\n', 'Dell\r\n', 'Dell Vostro 3670\r\n', '', ''),
(42, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', '\"Monitor Dell 19.5 Inci\r\n\r\n100x900 Res\"\r\n', 'CN-0MNPH2-FCC00-840-AUUU\r\n', 'Dell\r\n', 'Dell E2016HV\r\n', '', ''),
(43, 'Komputer\r\n', 'Elektronik\r\n', '\"Komputer PC Dell (Built Up)\r\n\r\nKeyboard + Mouse&nbsp;\r\n\r\nHDD 1 TB, RAM 4 GB\"\r\n', '59NG6Q2\r\n', 'Dell\r\n', 'Dell Vostro 3670\r\n', '', ''),
(44, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', '\"Monitor Dell 19.5 Inci&nbsp;\r\n\r\n1600x900 Res\"\r\n', 'CN-0MNPH2-FCC00-840-CG6U\r\n', 'Dell\r\n', 'Dell E2016HV\r\n', '', ''),
(45, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC All in One Acer Aspire C20-220, RAM 4GB, HDD 500GB\r\n', '71102205130\r\n', 'Acer\r\n', 'Aspire C20-220\r\n', '', ''),
(46, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC All in One Acer Aspire C20-220, RAM 4GB, HDD 500GB\r\n', '71102207030\r\n', 'Acer\r\n', 'Aspire C20-220\r\n', '', ''),
(47, 'Printer\r\n', 'Elektronik\r\n', 'Print, Copy &amp;&nbsp;Scan\r\n', 'X3GW458108\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(48, 'Notebook\r\n', 'Elektronik\r\n', '\"Laptop Asus\r\n\r\nHDD 1 TB, RAM 4 GB\"\r\n', 'J3N0CV045717118\r\n', 'Asus\r\n', 'Asus X441U\r\n', '', ''),
(49, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop HP HDD 500, RAM 4 GB\r\n', '5CG6034TPK\r\n', 'HP\r\n', 'Laptop HP\r\n', '', ''),
(50, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 500 GB, RAM 4 GB\r\n', 'H1N0WU16123903D\r\n', 'Asus\r\n', 'Asus X441U\r\n', '', ''),
(51, 'Komputer\r\n', 'Elektronik\r\n', 'PC All in One Acer Aspire C20-220, RAM 4GB, HDD 500GB\r\n', '-\r\n', 'Acer\r\n', 'Acer PC C20-220\r\n', '', ''),
(52, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon Scan, Copy &amp; Print\r\n', '', 'Canon\r\n', 'Canon MP287\r\n', '', ''),
(53, 'Access Point\r\n', 'Elektronik\r\n', 'Akses Point Unifi, Range Up to 122 Meter 5 GHz Band/Radio Rate 1300 Mbps 2 GHz/Radio Rate 450 Mbps\r\n', '788A20296DAA\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(54, 'Access Point', 'Elektronik', 'Akses Point Unifi, Range Up to 122 Meter 5 GHz Band/Radio Rate 1300 Mbps 2 GHz/Radio Rate 450 Mbps\r\n', '788A202CAC3D\r\n', 'UBIQUITI', 'UNifi AP AC PRO', '', ''),
(55, 'Access Point', 'Elektronik', 'Akses Point Unifi, Range Up to 122 Meter 5 GHz Band/Radio Rate 1300 Mbps 2 GHz/Radio Rate 450 Mbps\r\n', '(K) FCECDA167977\r\n', 'UBIQUITI', 'UNifi AP AC PRO', '', ''),
(56, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V\r\n', 'U-001\r\n', 'Ubiquiti\r\n', '', '', ''),
(57, 'PoE Adapter\r\n', 'Elektronik\r\n\r\n', 'Adaptor PoE Ubiquiti 48V', 'U-002', 'Ubiquiti\r\n', '', '', ''),
(58, 'Switch\r\n', 'Elektronik\r\n', 'Switch Edge Core ECS4100-52T\r\n', 'EC1733002253\r\n', 'Edge Core\r\n', 'ECS4100-52T\r\n', '', ''),
(59, 'Switch', 'Elektronik', 'Switch Edge Core ECS4100-52T', 'EC1733002269\r\n', 'Edge Core', 'ECS4100-52T', '', ''),
(60, 'VOIP Phone\r\n', 'Elektronik\r\n', 'Telepon VOIP, 132 x 64-pixel graphical LCD, 2 VoIP accounts, Two-port 10/100 Ethernet Switch\r\n', '2121117080D47525\r\n', 'Yealink\r\n', 'SIP-T21P E2\r\n', '', ''),
(61, 'VOIP Phone\r\n', 'Elektronik\r\n', 'Telepon VOIP, 132 x 64-pixel graphical LCD, 2 VoIP accounts, Two-port 10/100 Ethernet Switch\r\n', '2121117080D45920\r\n', 'Yealink\r\n', 'SIP-T21P E2\r\n', '', ''),
(62, 'VOIP Phone', 'Elektronik\r\n', 'Telepon VOIP, 132 x 64-pixel graphical LCD, 2 VoIP accounts, Two-port 10/100 Ethernet Switch\r\n', '2121117080D45921\r\n', 'Yealink', 'SIP-T21P E2\r\n', '', ''),
(63, 'VOIP Phone\r\n', 'Elektronik\r\n', 'Telepon VOIP, 132 x 64-pixel graphical\r\n', '2121117080D46069\r\n', 'Yealink\r\n', 'SIP-T21P E2\r\n', '', ''),
(64, 'VOIP Phone\r\n', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D46069\r\n', 'Yealink', 'SIP-T21P E2\r\n', '', ''),
(65, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D46070\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(66, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D46071\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(67, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47520\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(68, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47522\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(69, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47523\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(70, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47524\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(71, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47526\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(72, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47527\r\n\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(73, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47528\r\n\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(74, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47529\r\n\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(75, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '2121117080D47531\r\n\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(76, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '4121115070001217M\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(77, 'VOIP Phone', 'Elektronik', 'Telepon VOIP, 132 x 64-pixel graphical', '4121115090011780D\r\n\r\n\r\n\r\n', 'Yealink', 'SIP-T21P E2', '', ''),
(78, 'Router Mikrotik\r\n', 'Elektronik\r\n', 'Network Router, 8 Port, (korslet karena petir)\r\n', '191017-AINOS-AA031\r\n', 'MikroBits Ainos\r\n', 'MBITSAI2-L6\r\n', '', ''),
(79, 'Router Mikrotik\r\n', 'Elektronik', 'Network Router, 8 Port, (rusak karena\r\n', '250146-AINOS-Q0085\r\n', 'MikroBits Ainos', 'MBITSAI2-L6', '', ''),
(80, 'Digital Video Recorder\r\n', 'Elektronik\r\n', 'Digital Video Recorder HIKVISION, HDD 2 TB\r\n', '131690971\r\n', 'HIKVISION\r\n', 'DS-7216HQHI-K2\r\n', '', ''),
(81, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari\r\n', '544166314\r\n', 'Suprema Bioentry W2\r\n', 'BEW2-0DP\r\n', '', ''),
(82, 'Mesin Absen\r\n', 'Elektronik\r\n', 'AC Adaptor Mesin SIdik Jari\r\n', '000000020181\r\n', 'RoHS\r\n', 'DZ036DL120250F\r\n', '', ''),
(83, 'Mesin Absen', 'Elektronik', 'AC Adaptor Mesin SIdik Jari\r\n', '000000020204\r\n', 'RoHS', 'DZ036DL120250F', '', ''),
(84, 'Mesin Absen', 'Elektronik', 'AC Adaptor Mesin SIdik Jari', '000000020212\r\n', 'RoHS', 'DZ036DL120250F', '', ''),
(85, 'Mesin Absen', 'Elektronik', 'AC Adaptor Mesin SIdik Jari', '000000021201\r\n', 'RoHS', 'DZ036DL120250F', '', ''),
(86, 'Mesin Absen', 'Elektronik', 'AC Adaptor Mesin SIdik Jari', '000000022021\r\n', 'RoHS', 'DZ036DL120250F', '', ''),
(87, 'Switch\r\n', 'Elektronik\r\n', 'Switch D-Link 16 Port\r\n', 'QS7Q163000936\r\n', 'D-Link\r\n', 'DGS-1024C\r\n', '', ''),
(88, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'Monitor LG 20&quot;\r\n', 'MEZ64321306\r\n', 'LG\r\n', '20MP48A\r\n', '', ''),
(89, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'Monitor Lenovo&nbsp;\r\n', '1S65BAACC1USU38ABD39\r\n', 'Lenovo\r\n', 'LI2054A\r\n', '', ''),
(90, 'Modem GSM\r\n', 'Elektronik\r\n', 'Modem GSM Huawei 21 Mbps\r\n', '41982/SDPPI/2015\r\n', 'Huawei\r\n', 'E353\r\n', '', ''),
(91, 'UPS/Stavolt\r\n', 'Elektronik\r\n', 'UPS APC 650VA\r\n', '9B1527A02059\r\n', 'APC\r\n', 'BACK UPS 650\r\n', '', ''),
(92, 'UPS/Stavolt\r\n', 'Elektronik\r\n', 'UPS APC 6000 VA\r\n', '42C2E95463\r\n', 'APC\r\n', 'APC 6000\r\n', '', ''),
(93, 'Router Mikrotik\r\n', 'Elektronik\r\n', 'Router&nbsp;Fortiner 16 Port\r\n', 'C1AE25-04AA-0000\r\n', 'Fortinet\r\n', 'Fortigate 100E\r\n', '', ''),
(94, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V\r\n', 'U-003\r\n', 'Ubiquiti\r\n', '', '', ''),
(95, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus HDD 1 TB, RAM 4 TB\r\n', 'GBN0CX166778463\r\n', 'Asus\r\n', 'A456U\r\n', '', ''),
(96, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Lenovo\r\n', 'PF0ZA39Q\r\n', 'Lenovo\r\n', 'Ideapad 110\r\n', '', ''),
(97, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 1 TB, RAM 4 GB\r\n', 'GCN0CX17587151A\r\n', 'Asus\r\n', 'A456U\r\n', '', ''),
(98, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Lenovo, HDD 1 TB, RAM 8 GB\r\n', 'PF11YK87\r\n', 'Lenovo\r\n', 'Ideapad 320\r\n', '', ''),
(99, 'Notebook\r\n', 'Elektronik', 'Laptop Lenovo, HDD 1 TB, RAM 8 GB', 'PF11XVAJ\r\n', 'Lenovo', 'Ideapad 320', '', ''),
(100, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus Strix, HDD 1 TB, RAM 16 GB\r\n', 'HCN0CX07170059F\r\n', 'Asus\r\n', 'GL503V\r\n', '', ''),
(101, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus Strix, HDD 1 TB, RAM 16 GB\r\n', 'H6N0CV10K2625F\r\n', 'Asus\r\n', 'GL553V\r\n', '', ''),
(102, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari\r\n', '538626066\r\n', 'Supreme\r\n', 'Biolite Net\r\n', '', ''),
(103, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Dell Inspiron 14 3000 Series, HDD 500 GB, RAM 8 GB\r\n', '73YRB12\r\n', 'Dell\r\n', 'Inspiron 14 \r\n', '', ''),
(104, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Dell, HDD 500 GB, RAM 8 GB\r\n', '5CG6391QGN\r\n', 'HP\r\n', 'TPN-119 RMN\r\n', '', ''),
(105, 'Printer\r\n', 'Elektronik\r\n', 'Printer Brother A3\r\n', 'E72405CSF233631\r\n', 'Brother\r\n', 'MFC-J3520\r\n', '', ''),
(106, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 500 GB, RAM 2 GB\r\n', 'EBN0CX497097472\r\n', 'Asus\r\n', 'X453M\r\n', '', ''),
(107, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Dell Inspiron 14 3000 Serias, HDD 500 GB, RAM 2 GB\r\n', '4FRT4Z1\r\n', 'Dell\r\n', 'Inspiron 14\r\n', '', ''),
(108, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon Printer, Scan &amp; Copy\r\n', 'QC3-0018\r\n', 'Canon\r\n', 'Canon MP287\r\n', '', ''),
(109, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson A3\r\n', 'UBGY021456\r\n', 'Epson\r\n', 'L1300\r\n', '', ''),
(110, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 1 TB, RAM 4 GB\r\n', 'GCN0CX175843512\r\n', 'Asus\r\n', 'A456U\r\n', '', ''),
(111, 'TV\r\n', 'Elektronik\r\n', 'TV Sharp Aquos 40 Inchi\r\n', '9651016L01042\r\n', 'SHARP\r\n', 'Sharp Aquos LC-40LE1851\r\n', '', ''),
(112, 'TV\r\n', 'Elektronik\r\n', 'TV Sharp Aquos 32&quot;\r\n', '409746326\r\n', 'SHARP', 'Sharp Aquos LC-32LE155M\r\n', '', ''),
(113, 'Router Mikrotik\r\n', 'Elektronik\r\n', 'Router TP Link\r\n', '2148850000031\r\n', 'TP Link\r\n', 'TL-ER5120\r\n', '', ''),
(114, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Scan, Copy &amp; Print\r\n', 'WNSP174279\r\n', 'Epson\r\n', 'L220\r\n', '', ''),
(115, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson A3\r\n', 'MK4Y133748\r\n', 'Epson\r\n', 'LQ-2190\r\n', '', ''),
(116, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'Monitor/TV LED Samsung 40&quot; Dipakai Ruang Meeting Office Morosi\r\n', '00MT3NNHA00059D\r\n', 'Samsung\r\n', 'UA60J6200AK\r\n', '', ''),
(117, 'Projector\r\n', 'Elektronik\r\n', 'Proyektor BenQ Di Ruang Meeting Office Morosi\r\n', 'PDAAH5208300\r\n', 'BenQ\r\n', 'MS506\r\n', '', ''),
(118, 'Switch\r\n', 'Elektronik\r\n', 'Mini Switch DLink 8 Port\r\n', 'RZZI1HB000053\r\n', 'D Link\r\n', 'DGS-1008A\r\n', '', ''),
(119, 'Speaker / Pengeras Suara\r\n', 'Elektronik\r\n', 'Pengeras Suara Huper = 2 Buah di Ruang Meeting Office Morosi\r\n', '12HA350\r\n', 'Huper\r\n', '', '', ''),
(120, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari\r\n', '544166317\r\n', 'Suprema Bioentry W2\r\n', 'BEW2-0DP\r\n', '', ''),
(121, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'TV/Monitor Sharp 40&quot; untuk CCTV di Ruang Staff IT (korslet karena petir)\r\n', '802716440\r\n', 'Sharp\r\n', 'Aquos LC-40LE380X\r\n', '', ''),
(122, 'Komputer\r\n', 'Komputer\r\n', 'Komputer PC Lenovo, HDD 1TB, RAM 4GB, Server Absen Office Morosi\r\n', 'R3031VB6\r\n', 'Lenovo\r\n', '90DA\r\n', '', ''),
(123, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus\r\n', 'H5N0WU16830321B\r\n', 'Asus\r\n', 'Asus X441N\r\n', '', ''),
(124, 'Printer\r\n', 'Elektronik\r\n', 'Printer Copy &amp; Scan\r\n', 'E76998A8F112356\r\n', 'Brother\r\n', 'MFC-J3530DW\r\n', '', ''),
(125, 'Printer\r\n', 'Elektronik\r\n', 'Dipakai di Divisi Keamanan, Pos Secutiry\r\n', 'X3GW113726\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(126, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus HDD 500GB, RAM 2GB\r\n', 'F2N0WU133637060\r\n', 'Asus\r\n', 'X454W\r\n', '', ''),
(127, 'Printer\r\n', 'Elektronik\r\n', 'Printer Canon Scan, Copy &amp; Print\r\n', 'KKEW06911\r\n', 'Canon\r\n', 'Pixma G1000\r\n', '', ''),
(128, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Acer HDD 500GB, RAM 2 GB\r\n', 'NXMT1SN00652302C114P00\r\n', 'Acer\r\n', 'Acer Z1401-C810\r\n', '', ''),
(129, 'Notebook\r\n', 'Elektronik\r\n', '\"intel core i5\r\n	RAM 8 Gb\r\n	HDD 128 Gb\r\n	OS Win 10\"\r\n', '8CG7492QYR\r\n', 'HP\r\n', 'HP Pavillion X360 \r\n', '', ''),
(130, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari di Pos Security\r\n', '0003\r\n', 'Suprema Bioentry W2\r\n', 'BEW2-0DP\r\n', '', ''),
(131, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari di Pos Security\r\n', '0002\r\n', 'Suprema Bioentry W2\r\n', 'BEW2-0DP\r\n', '', ''),
(132, 'Mesin Absen\r\n', 'Elektronik\r\n', 'Mesin Absen Sidik Jari di Pos Security\r\n', '0001\r\n', 'Suprema Bioentry W2\r\n', 'BEW2-0DP\r\n', '', ''),
(133, 'Notebook\r\n', 'Elektronik\r\n', '\"RAM 6 Gb\r\n	HDD 500 Gb\r\n	Core i3\"\r\n', '5CG5530QZZ\r\n', 'HP\r\n', 'HP RTL 8273\r\n', '', ''),
(134, 'Notebook\r\n', 'Elektronik\r\n', '\"HDD 1 Tb\r\n	i7\r\n	14&quot;&quot;\"\r\n', 'PF161NK3\r\n', 'Lenovo\r\n', 'Lenovo Thinkpad E480\r\n', '', ''),
(135, 'Komputer\r\n', 'Elektronik\r\n', '\"Komputer PC HP All in One + Keyboar + Mouse\r\n\r\nHDD 1 TB, RAM 4 GB\"\r\n', '8CC8160610\r\n', 'HP\r\n', 'HP All in One PC 22\r\n', '', ''),
(136, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus, HDD 1TB, RAM 4GB\r\n', 'J4N0CV01044914H\r\n', 'Asus\r\n', 'Asus A442U\r\n', '', ''),
(137, 'Notebook\r\n', '\r\n', '', '', '', '', '', ''),
(138, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC Acer HDD 1TB, RAM 8GB\r\n', 'DQBABSNOO2746004EB3000\r\n', 'Acer\r\n', 'Acer Aspire C24-860\r\n', '', ''),
(139, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC Lenovo Core i3, HDD 1 TB, RAM 8 GB\r\n', 'YJOOD1Q7\r\n', 'Lenovo\r\n', 'Lenovo Z410z\r\n', '', ''),
(140, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC Lenovo Core i3, HDD 1 TB, RAM 8 GB\r\n', 'YJOOD1Q7\r\n', 'Lenovo\r\n', 'Lenovo Z410z\r\n', '', ''),
(141, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson, Scan &amp; Copy\r\n', 'X3GW030710\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(142, 'Printer\r\n', 'Elektronik\r\n', 'Printer Wireless Multi Fungsi\r\n', '-\r\n', 'Epson\r\n', 'WF-7621\r\n', '', ''),
(143, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC Lenovo, HDD 2TB, RAM 4GB + Monitor Lenovo\r\n', 'R3054X7W\r\n', 'Lenovo\r\n', '-\r\n', '', ''),
(144, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson L360 Copy, Print &amp; Scan\r\n', 'X3GW030710\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(145, 'Elektronik\r\n', '\r\nElektronik\r\n', 'Komputer PC Lenovo, HDD 2TB, RAM 4GB\r\n', 'R3054X7W\r\n', 'Lenovo\r\n', '-\r\n', '', ''),
(146, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Scan, Copy &amp; Print\r\n', 'X3GW030710\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(147, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Asus Intel Core i5, HDD 1 TB, RAM 4 GB\r\n', 'GAN0CX096329414\r\n', 'Asus\r\n', 'Asus A456U\r\n', '', ''),
(148, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson Single Function\r\n', 'TP2K314349\r\n', 'Epson\r\n', 'Epson L120\r\n', '', ''),
(149, 'Mouse Wireless\r\n', 'Elektronik\r\n', 'Mouse Wireless\r\n', 'ITTL-MRS0003\r\n', 'Baitnet\r\n', '-\r\n', '', ''),
(150, 'Polycom\r\n', 'Elektronik\r\n', 'Camera Polycom\r\n', '132429369\r\n', 'Polycom\r\n', 'MPTZ-10\r\n', '', ''),
(151, 'Polycom\r\n', 'Elektronik\r\n', 'Microphone Polycom\r\n\r\n', '321731045409D2\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(152, 'Polycom\r\n', 'Elektronik\r\n', 'Alat Video Conference Polycom\r\n\r\n\r\n', '82173247BEF8CV\r\n\r\n', 'Polycom\r\n', 'Polycom Group 500\r\n\r\n', '', ''),
(153, 'Polycom\r\n', 'Elektronik\r\n', 'Remote Control Polycom\r\n\r\n\r\n', '2201-52757-001\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(154, 'Polycom\r\n', 'Elektronik\r\n', 'Kabel HDMI Polycom = 2 Pcs\r\n\r\n\r\n\r\n', '2457-28808-004\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(155, 'Polycom\r\n', 'Elektronik\r\n', 'Adaptor Polycom\r\n\r\n\r\n\r\n\r\n', 'H7261004167\r\n\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(156, 'Polycom\r\n', 'Elektronik\r\n', 'Kabel Power Polycom\r\n\r\n\r\n\r\n\r\n\r\n', '2215-00286-002\r\n\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(157, 'Polycom\r\n', 'Elektronik\r\n', 'Kabel UTP Polycom\r\n\r\n\r\n\r\n\r\n\r\n\r\n', '2457-23537-001\r\n\r\n\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(158, 'Polycom\r\n', 'Elektronik\r\n', 'Kabel Microphone Polycom\r\n\r\n\r\n\r\n\r\n\r\n', '2457-23216-002\r\n\r\n\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(159, 'Polycom\r\n', 'Elektronik\r\n', 'Kabel Camera Polycom\r\n\r\n\r\n\r\n\r\n\r\n', '2457-64356-001\r\n\r\n\r\n\r\n\r\n', 'Polycom\r\n', '\r\n', '', ''),
(160, 'Switch\r\n', 'Elektronik\r\n', 'Mini Switch D-Link 8 Port\r\n', 'RZZI1HB012267\r\n', 'D-Link\r\n', 'DGS-1008A\r\n', '', ''),
(161, 'Access Point\r\n', 'Elektronik\r\n', 'Akses Point Unifi, Range Up to 122 Met\r\n', '(G)7884202C2DA0\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(162, 'Access Point', 'Elektronik', 'Akses Point Unifi, Range Up to 122 Met\r\n', '(G)788A202C2A02\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(163, 'Access Point\r\n', 'Elektronik\r\n', 'Akses Point Unifi [Rusak Kena Air Huja\r\n', '(G)788A202C29B4\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(164, 'Access Point\r\n', 'Elektronik\r\n', 'Akses Point Unifi, Range Up to 122 Met\r\n', '(G)7884202C2A7D\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(165, 'Access Point\r\n', 'Elektronik\r\n', 'Akses Point Unifi, Range Up to 122 Met\r\n', '(G)7884202C27E1\r\n', 'UBIQUITI\r\n', 'UNifi AP AC PRO\r\n', '', ''),
(166, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V (0,5A)\r\n', '8-1035402308-8A\r\n', 'UBIQUITI\r\n', '-\r\n', '', ''),
(167, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V (0,5A)\r\n', '8-1035402308-8B\r\n', 'UBIQUITI\r\n', '-\r\n', '', ''),
(168, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V (0,5A)\r\n', '8-1035402308-8C\r\n', 'UBIQUITI\r\n', '-\r\n', '', ''),
(169, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V (0,5A)\r\n', '8-1035402308-8D\r\n', 'UBIQUITI\r\n', '-\r\n', '', ''),
(170, 'PoE Adapter\r\n', 'Elektronik\r\n', 'Adaptor PoE Ubiquiti 48V (0,5A)\r\n', '8-1035402308-8E\r\n', 'UBIQUITI\r\n', '-\r\n', '', ''),
(171, 'Switch\r\n', 'Elektronik\r\n', 'Switch D-Link 16 Port\r\n', 'QS7P1HB002255\r\n', 'D-Link\r\n', 'DGS-1016C\r\n', '', ''),
(172, 'Switch\r\n', 'Elektronik\r\n', 'Switch D-Link 16 Port\r\n', 'QS7P1HB002252\r\n', 'D-Link\r\n', 'DGS-1016C\r\n', '', ''),
(173, 'Drone\r\n', 'Elektronik\r\n', '\"Kelengkapan :\r\n\r\nBaterai 3 bh, Kabel USB, Charger, Mikro USB Cable (2 bh), Adaptor\"\r\n', '0K1DF4L2BD5P02\r\n', 'DJI\r\n', 'MAVIC AIR\r\n', '', ''),
(174, 'Notebook\r\n', 'Elektronik\r\n', 'Laptop Acer Intel Celeron N3350, HDD 500GB, DDR3L 2GB\r\n', 'NXGJ3SN005736126D57600\r\n', 'Acer\r\n', 'Aspire ES1-432-C44V\r\n', '', ''),
(175, 'Printer\r\n', 'Elektronik\r\n', 'Printer Epson All in One\r\n', 'X3GW514078\r\n', 'Epson\r\n', 'Epson L360\r\n', '', ''),
(176, 'Notebook\r\n', 'Elektronik\r\n', '\"Core i7 1.8 Ghz\r\n	RAM 8 Gb\r\n	OS 64 bit\"\r\n', 'PF161BR4\r\n', 'IBM Thinkpad\r\n', 'Thinkpad\r\n', '', ''),
(177, 'Access Point\r\n', 'Elektronik\r\n', 'Acces Point D-Link 2.4 GHz\r\n', 'QBSZ1F1000460\r\n', 'D-Link\r\n', 'DWL-2600AP\r\n', '', ''),
(178, 'Komputer\r\n', 'Elektronik\r\n', 'Komputer PC All in One HP Core I5 Gen 7 HDD 1 TB RAM 4GB\r\n\r\n+ Wireless Keyboard &amp; Wireless Mouse', '8CC8050GVR\r\n', 'HP\r\n', 'HP All in One PC\r\n', '', ''),
(179, 'Notebook\r\n', 'Elektronik\r\n', '\"Intel core i-5 2.4 Ghz\r\n	RAM 4 Gb\"\r\n', '064747754153\r\n', 'Microsoft Surface Pro\r\n', 'Surface Pro\r\n', '', ''),
(180, 'Projector Screen\r\n', 'Elektronik\r\n', 'Layar Proyektor di Ruang Meeting\r\n', 'PS-DS01\r\n', 'Digital Screen\r\n', 'Digital Screen\r\n', '', ''),
(181, 'Sound Mixing Console\r\n', 'Elektronik\r\n', 'Soung Mixing Console Yamaha 16 Channel diruang Meeting\r\n', 'EGXY01174\r\n', 'Yamaha\r\n', 'MG16XU\r\n', '', ''),
(182, 'Wireless Microphone\r\n', 'Elektronik\r\n', '1 Pcs Digital Receiver, 2 Buah Mic, Freq UHF di Ruang Meeting\r\n', 'AFRM0046\r\n', 'BMB\r\n', 'WT-6600\r\n', '', ''),
(183, 'Wireless Microphone\r\n', 'Elektronik\r\n', '2 Buah Receiver, 2 Buah Mic Freq UHF di Ruang Meeting\r\n', '-\r\n', 'Aiwa\r\n', 'WM-588\r\n', '', ''),
(184, 'Speaker / Pengeras Suara\r\n', 'Elektronik\r\n', 'Stand Microphone Meja di Ruang Meeting\r\n', '-\r\n', 'PMX\r\n', 'BS-201A\r\n', '', ''),
(185, 'Wireless Microphone\r\n', 'Elektronik\r\n', '2 Buah Receiver, 2 Buah Mic, Freq UHF di Ruang Meeting\r\n', '-\r\n', 'Sony\r\n', 'BT-220\r\n', '', ''),
(186, 'Speaker / Pengeras Suara\r\n', 'Elektronik\r\n', '2 Speaker/Ampli Bassic di Ruang Meeting\r\n', '-\r\n', 'Bassic\r\n', 'KB Series 400\r\n', '', ''),
(187, 'Komputer\r\n', 'Elektronik\r\n', '\"RAM 2 Gb\r\n	HDD 512 Mb\"\r\n', 'VS81993222\r\n', 'Lenovo\r\n', 'Lenovo All in One PC\r\n', '', ''),
(188, 'VOIP Phone\r\n', 'Elektronik\r\n', 'Ext 9120\r\n', '4121115090011778\r\n', 'Yealink\r\n', 'T21P\r\n', '', ''),
(189, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', 'Monitor 21&quot;\r\n', '504NDQA39428\r\n', 'LG\r\n', '20M37A\r\n', '', ''),
(190, 'Monitor LCD / LED\r\n', 'Elektronik\r\n', '24&quot; gaming monitor\r\n', '808NTXR39018\r\n', 'LG\r\n', '24GM79G\r\n', '', ''),
(191, 'Switch\r\n', 'Elektronik\r\n', 'Switch gigabit 8 port\r\n', 'RZZI1H8002220\r\n', 'Dlink\r\n', 'GS-10008A\r\n', '', ''),
(192, 'Komputer\r\n', 'Elektronik\r\n', '\"Memori 8 Gb\r\n	HDD 1 Tb\"\r\n', '1000\r\n', 'PC rakitan Azzura GT5\r\n', 'PC rakitan Azzura GT5\r\n', '', ''),
(193, 'Microsoft Surface Pro\r\n', 'Elektronik\r\n', '\"windows 10 pro\r\n	Intel core i7\r\n	HDD 1Tb\r\n	16 Gb RAM\"\r\n', '005533581453\r\n', 'Microsoft Surface Pro\r\n', 'Model 1796\r\n', '', ''),
(194, 'Microsoft Surface Pro\r\n', 'Elektronik\r\n', '', '069067781954\r\n', 'Microsoft Surface Type Cover\r\n', 'Type Cover (keyboard surface pro)\r\n', '', ''),
(195, 'Notebook\r\n', 'Elektronik\r\n', '\"8 Gb RAM\r\n	HDD 1 TB\r\n	Laptop 15&quot;\"\r\n', 'CND74445LJ\r\n', 'HP\r\n', 'HP 15-bw0xx\r\n', '', ''),
(196, 'Notebook\r\n', 'Elektronik\r\n', '\"8 Gb RAM\r\n	Core i7\r\n	HDD 1 TB\"\r\n', 'R90LNDSX\r\n', 'IBM\r\n', '20BES09000\r\n', '', ''),
(197, 'Mouse Wireless\r\n', 'Elektronik\r\n', 'Mouse logitech\r\n', '1790LZX4CU48\r\n', 'Logitech\r\n', 'M185\r\n', '', ''),
(198, 'Notebook\r\n', 'Elektronik\r\n', '\"4 Gb RAM\r\n	HDD 500 Mb\r\n	Intel core i3\"\r\n', '5CG6034TFF\r\n', 'HP\r\n', 'HP Notebook\r\n', '', ''),
(199, 'Mouse Wireless\r\n', 'Elektronik\r\n', '', '1743LZX7X3Z8\r\n', 'logitech\r\n', 'B170\r\n', '', ''),
(200, 'Komputer\r\n', 'Elektronik\r\n', '\"RAM 2 Gb\r\n	HDD 500 Mb\r\n	Mouse\r\n	Keyboard\r\n	Intel i3\"\r\n', 'VS81993288\r\n', 'lenovo\r\n', 'Lenovo all in one PC\r\n', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_divisi`
--

CREATE TABLE `tbl_divisi` (
  `divisi_id` int(11) NOT NULL,
  `divisi_nama` varchar(250) NOT NULL,
  `lokasi_nama` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_divisi`
--

INSERT INTO `tbl_divisi` (`divisi_id`, `divisi_nama`, `lokasi_nama`) VALUES
(1, 'FINANCE', 'MOROSI'),
(2, 'IT', 'MOROSI'),
(4, 'HRD', 'MOROSI'),
(5, 'SAFETY', 'MOROSI'),
(6, 'GA', 'MOROSI'),
(7, 'PURCHASING', 'MOROSI'),
(8, 'LOGISTIK', 'MOROSI'),
(9, 'EXPORT-IMPOR', 'MOROSI'),
(10, 'ORE SUPPLY', 'MOROSI'),
(11, 'LAB ORE', 'MOROSI'),
(12, 'STOKPILE', 'MOROSI'),
(13, 'DL-TRANSPORTASI', 'MOROSI'),
(14, 'DLL-TRANSPORTASI', 'MOROSI'),
(15, 'DLA-TRANSPORTASI', 'MOROSI'),
(16, 'WORKSHOP', 'MOROSI'),
(17, 'PLTU', 'MOROSI'),
(18, 'HUMAS', 'MOROSI'),
(19, 'SECURITY', 'MOROSI');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_files`
--

CREATE TABLE `tbl_files` (
  `file_id` int(11) NOT NULL,
  `file_judul` varchar(120) DEFAULT NULL,
  `file_deskripsi` text DEFAULT NULL,
  `file_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `file_oleh` varchar(60) DEFAULT NULL,
  `file_download` int(11) DEFAULT 0,
  `file_data` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_files`
--

INSERT INTO `tbl_files` (`file_id`, `file_judul`, `file_deskripsi`, `file_tanggal`, `file_oleh`, `file_download`, `file_data`) VALUES
(2, 'Dasar-dasar CSS', 'Modul dasar-dasar CSS 3. Modul ini membantu anda untuk memahami struktur dasar CSS', '2017-01-22 20:30:01', 'Drs. Joko', 0, 'ab9a183ff240deadbedaff78e639af2f.pdf'),
(3, '14 Teknik Komunikasi Yang Paling Efektif', 'Ebook 14 teknik komunikasi paling efektif membantu anda untuk berkomunikasi dengan baik dan benar', '2017-01-23 07:26:06', 'Drs. Joko', 0, 'ab2cb34682bd94f30f2347523112ffb9.pdf'),
(4, 'Bagaimana Membentuk Pola Pikir yang Baru', 'Ebook ini membantu anda membentuk pola pikir baru.', '2017-01-23 07:27:07', 'Drs. Joko', 0, '30f588eb5c55324f8d18213f11651855.pdf'),
(5, '7 Tips Penting mengatasi Kritik', '7 Tips Penting mengatasi Kritik', '2017-01-23 07:27:44', 'Drs. Joko', 0, '329a62b25ad475a148e1546aa3db41de.docx'),
(6, '8 Racun dalam kehidupan kita', '8 Racun dalam kehidupan kita', '2017-01-23 07:28:17', 'Drs. Joko', 0, '8e38ad4948ba13758683dea443fbe6be.docx'),
(7, 'Jurnal Teknolgi Informasi', 'Jurnal Teknolgi Informasi', '2017-01-24 19:18:53', 'Gunawan, S.Pd', 0, '87ae0f009714ddfdd79e2977b2a64632.pdf'),
(8, 'Jurnal Teknolgi Informasi 2', 'Jurnal Teknolgi Informasi', '2017-01-24 19:19:22', 'Gunawan, S.Pd', 0, 'c4e966ba2c6e142155082854dc5b3602.pdf'),
(9, 'Naskah Publikasi IT', 'Naskah Teknolgi Informasi', '2017-01-24 19:21:04', 'Gunawan, S.Pd', 0, '71380b3cf16a17a02382098c028ece9c.pdf'),
(10, 'Modul Teknologi Informasi', 'Modul Teknologi Informasi', '2017-01-24 19:22:08', 'Gunawan, S.Pd', 0, '029143a3980232ab2900d94df36dbb0c.pdf'),
(11, 'Modul Teknologi Informasi Part II', 'Modul Teknologi Informasi', '2017-01-24 19:22:54', 'Gunawan, S.Pd', 0, 'ea8f3f732576083156e509657614f551.pdf'),
(12, 'Modul Teknologi Informasi Part III', 'Modul Teknologi Informasi', '2017-01-24 19:23:21', 'Gunawan, S.Pd', 0, 'c5e5e7d16e4cd6c3d22c11f64b0db2af.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jabatan1`
--

CREATE TABLE `tbl_jabatan1` (
  `jabatan_id` int(11) NOT NULL,
  `jabatan_nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jabatan1`
--

INSERT INTO `tbl_jabatan1` (`jabatan_id`, `jabatan_nama`) VALUES
(1, 'PENGAWAS'),
(2, 'KEPALA DIVISI'),
(3, 'KOORDINATOR'),
(4, 'STAFF'),
(5, 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE `tbl_karyawan` (
  `karyawan_id` int(11) NOT NULL,
  `karyawan_nik` varchar(200) NOT NULL,
  `karyawan_nama` varchar(250) NOT NULL,
  `jabatan_nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`karyawan_id`, `karyawan_nik`, `karyawan_nama`, `jabatan_nama`) VALUES
(4, '12312335', 'Julius Puang', 'STAFF'),
(7, '234567', 'Ardhi Fahrizal', 'KEPALA DIVISI'),
(8, '1231234', 'Iswanto Hamdah', 'STAFF');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'HARDWARE'),
(2, 'SOFTWARE'),
(3, 'PERIPHERAL'),
(4, 'TOOL KIT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log`
--

CREATE TABLE `tbl_log` (
  `log_id` int(11) NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_log`
--

INSERT INTO `tbl_log` (`log_id`, `log_time`, `log_user`, `log_tipe`, `log_desc`) VALUES
(200, '2021-11-13 09:32:49', 'Julius', 0, 'masuk ke sistem'),
(201, '2021-11-13 09:34:12', 'Julius', 0, 'masuk ke sistem'),
(202, '2021-11-13 09:37:28', 'Julius', 0, 'masuk ke sistem'),
(203, '2021-11-13 09:39:23', 'Ardhi Fahrizal', 3, 'update kategori'),
(204, '2021-11-13 09:44:04', 'Ardhi Fahrizal', 3, 'update kategori'),
(205, '2021-11-13 09:44:23', 'Ardhi Fahrizal', 3, 'update ukuran'),
(206, '2021-11-13 09:45:23', 'Ardhi Fahrizal', 2, 'menambahkan ukuran'),
(207, '2021-11-13 09:46:48', 'Julius', 0, 'masuk ke sistem'),
(208, '2021-11-13 09:47:17', 'Julius', 0, 'masuk ke sistem'),
(209, '2021-11-13 10:22:38', 'Ardhi Fahrizal', 0, 'masuk ke sistem'),
(210, '2021-11-13 10:23:27', 'Julius', 0, 'masuk ke sistem'),
(211, '2021-11-13 12:09:54', 'Ardhi Fahrizal', 2, 'menambahkan level jabatan'),
(212, '2021-11-13 12:10:13', 'Ardhi Fahrizal', 2, 'menambahkan level jabatan'),
(213, '2021-11-13 12:10:49', 'Ardhi Fahrizal', 2, 'menambahkan level jabatan'),
(214, '2021-11-13 12:11:26', 'Ardhi Fahrizal', 2, 'menambahkan level jabatan'),
(215, '2021-11-13 12:11:34', 'Ardhi Fahrizal', 2, 'menambahkan level jabatan'),
(216, '2021-11-13 13:15:01', 'Julius', 0, 'masuk ke sistem'),
(217, '2021-11-15 01:52:24', 'Ardhi Fahrizal', 0, 'masuk ke sistem'),
(218, '2021-11-15 01:55:21', 'Julius', 0, 'masuk ke sistem');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lokasi`
--

CREATE TABLE `tbl_lokasi` (
  `lokasi_id` int(11) NOT NULL,
  `lokasi_nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_lokasi`
--

INSERT INTO `tbl_lokasi` (`lokasi_id`, `lokasi_nama`) VALUES
(1, 'JAKARTA'),
(2, 'MOROSI'),
(3, 'BARUGA'),
(4, 'JETTY'),
(6, 'Merlot Office'),
(7, 'KENDARI');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_alat`
--

CREATE TABLE `tbl_status_alat` (
  `status_id` int(11) NOT NULL,
  `status_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status_alat`
--

INSERT INTO `tbl_status_alat` (`status_id`, `status_nama`) VALUES
(1, 'BARU'),
(2, 'TERPAKAI'),
(3, 'PERBAIKAN'),
(4, 'DUMP'),
(5, 'HILANG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stok`
--

CREATE TABLE `tbl_stok` (
  `stok_id` int(11) NOT NULL,
  `aset_nama` varchar(100) NOT NULL,
  `kategori_nama` varchar(100) NOT NULL,
  `merek_nama` varchar(100) NOT NULL,
  `model_nama` varchar(100) NOT NULL,
  `tipe_nama` varchar(100) NOT NULL,
  `serial_nama` varchar(100) NOT NULL,
  `status_nama` varchar(100) NOT NULL,
  `ket_nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tgl` date NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_stok`
--

INSERT INTO `tbl_stok` (`stok_id`, `aset_nama`, `kategori_nama`, `merek_nama`, `model_nama`, `tipe_nama`, `serial_nama`, `status_nama`, `ket_nama`, `foto`, `tgl`, `jml`) VALUES
(1, 'VOIP Phone', 'Elektronik', 'Yealink', 'SIP-T21P E2', '', '4121115060012848', 'NEW', 'Telepon VOIP, 132 x 64-pixel graphical', '', '2018-07-27', '1'),
(2, 'Monitor LCD / LED', 'Elektronik', 'LG', '20MP48A', '', '604NTPC45288', 'NEW', 'Monitor LG 20', '', '2018-07-27', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ukuran`
--

CREATE TABLE `tbl_ukuran` (
  `ukuran_id` int(11) NOT NULL,
  `ukuran_nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ukuran`
--

INSERT INTO `tbl_ukuran` (`ukuran_id`, `ukuran_nama`) VALUES
(1, 'Unit'),
(2, 'Keping'),
(4, 'Set/Paket'),
(6, 'Roll');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `status` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id_user`, `nama_user`, `username`, `password`, `role`, `status`) VALUES
(1, 'Ardhi Fahrizal', 'admin', '0192023a7bbd73250516f069df18b500', 'admin', '1'),
(106, 'Julius', 'jul', 'cfc84e6c0207938a274c17016e1270e9', 'user', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rat`
--
ALTER TABLE `rat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_alat`
--
ALTER TABLE `tbl_alat`
  ADD PRIMARY KEY (`alat_id`);

--
-- Indexes for table `tbl_alat2`
--
ALTER TABLE `tbl_alat2`
  ADD PRIMARY KEY (`alat_id`);

--
-- Indexes for table `tbl_assign`
--
ALTER TABLE `tbl_assign`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `tbl_assign2`
--
ALTER TABLE `tbl_assign2`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `tbl_device`
--
ALTER TABLE `tbl_device`
  ADD PRIMARY KEY (`dev_id`);

--
-- Indexes for table `tbl_divisi`
--
ALTER TABLE `tbl_divisi`
  ADD PRIMARY KEY (`divisi_id`);

--
-- Indexes for table `tbl_files`
--
ALTER TABLE `tbl_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `tbl_jabatan1`
--
ALTER TABLE `tbl_jabatan1`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  ADD PRIMARY KEY (`karyawan_id`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tbl_lokasi`
--
ALTER TABLE `tbl_lokasi`
  ADD PRIMARY KEY (`lokasi_id`);

--
-- Indexes for table `tbl_status_alat`
--
ALTER TABLE `tbl_status_alat`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  ADD PRIMARY KEY (`stok_id`);

--
-- Indexes for table `tbl_ukuran`
--
ALTER TABLE `tbl_ukuran`
  ADD PRIMARY KEY (`ukuran_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rat`
--
ALTER TABLE `rat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_alat`
--
ALTER TABLE `tbl_alat`
  MODIFY `alat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_alat2`
--
ALTER TABLE `tbl_alat2`
  MODIFY `alat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_assign`
--
ALTER TABLE `tbl_assign`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_assign2`
--
ALTER TABLE `tbl_assign2`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_device`
--
ALTER TABLE `tbl_device`
  MODIFY `dev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `tbl_divisi`
--
ALTER TABLE `tbl_divisi`
  MODIFY `divisi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_files`
--
ALTER TABLE `tbl_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_jabatan1`
--
ALTER TABLE `tbl_jabatan1`
  MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  MODIFY `karyawan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_log`
--
ALTER TABLE `tbl_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `tbl_lokasi`
--
ALTER TABLE `tbl_lokasi`
  MODIFY `lokasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_status_alat`
--
ALTER TABLE `tbl_status_alat`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  MODIFY `stok_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_ukuran`
--
ALTER TABLE `tbl_ukuran`
  MODIFY `ukuran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
